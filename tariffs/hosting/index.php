<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Облачный веб-хостинг для предприятий малого и среднего бизнеса, ИП, стартапов, физических лиц. Размещение корпоративных сайтов, форумов, блогов, личных страниц, сайтов-визиток.");
$APPLICATION->SetPageProperty("title", "Облачный веб-хостинг | Hostfly.by");
$APPLICATION->SetTitle("Страница");
?>

<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BANNER_CLASS" => "banner-top",
		"IMG" => "/local/assets/images/bg/bg_host.png",
		"TITLE" => "Быстрый и надежный хостинг сайтов в облаке",
		"DESCRIPTION" => "Мы единственные в Беларуси переосмыслили идею виртуального хостинга и реализовали возможность его использования непосредственно в облачной инфраструктуре. Виртуальный хостинг у других провайдеров представляет собой небольшую часть пространства физического сервера, выделенного клиенту для размещения сайта. Виртуальный хостинг Hostfly – это повышенная отказоустойчивость, высокая производительность и отсутствие большинства лимитов вместе с уже привычными вам панелью управления Cpanel, ценами от 4,99 рублей в мес. и поддержкой CMS Wordpress, Joomla, Bitrix, OpenCart, MODx, Drupal и 150+ других.",
		"TEXT_BEFORE_BUTTON" => "",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => "",
		"BUTTON_TYPE" => ""
	),
	false
);?>

<a name="hosting-plans"></a>
<?$APPLICATION->IncludeComponent(
	"tariff:HostingList",
	".default",
	array(),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"content:SpecialOffer", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Тестовый период. Оцените любой тариф в течение 14 дней совершенно бесплатно",
		"BUTTON_TEXT" => "Подробнее",
		"BUTTON_LINK" => "/about/news/novost-11/"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"faq:commonList",
	".default",
	array(
		'SECTION_ID' => '4'
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"content:Recommendations", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Когда нужен хостинг сайтов в облаке",
		"BUTTON_TEXT" => "Выбрать тариф",
		"BUTTON_LINK" => "#hosting-plans",
		"BENEFITS_LIST" => array(
			0 => "Нужно разместить простой сайт, блог, сайт-портфолио или сайт-визитку",
			1 => "Необходимо автоматически развернуть в 1 клик готовую систему управления сайтом Wordpress, Joomla, Bitrix, OpenCart, MODx, Drupal, или 150+ других",
			2 => "Нет возможности самостоятельно настроить сервер и нет навыков системного администратора",
			3 => "Нужна высокая скорость и производительность, обеспечиваемая наличием SSD-дисков",
			4 => "Требуется надежность и постоянная доступность сайта за разумные деньги",
			5 => "Необходима гарантированная сохранность данных, хранение и возможность восстановления резервных копий сайта",
			6 => "",
		),
		"IMG" => "/local/assets/images/bg/man-and-tetris.png"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"tariff:SectionBlocks",
	".default",
	array(
		'SECTION_ID' => '1'
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>