<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Hostfly.by предоставляет выделенные сервера для малого и среднего бизнеса по доступной цене.");
$APPLICATION->SetPageProperty("title", "Выделенный сервер с полными правами администрирования | Hostfly.by");
$APPLICATION->SetTitle("Страница");
?>

<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BANNER_CLASS" => "banner-top",
		"IMG" => "/local/assets/images/bg/bg_server.png",
		"TITLE" => "Выделенный физический сервер",
		"DESCRIPTION" => "Dedicated сервер с возможностью самостоятельного администрирования. Вы сможете установить и настроить любое программное обеспечение, а также свободно распоряжаться всеми ресурсами. Наши сервера с одним и двумя процессорами Intel Xeon E3/E5 размещены в одном из лучших Центров обработки данных (ЦОД) в Беларуси, сертифицированном по международному стандарту как Tier 3 (отказоустойчивость - 99,982%), имеют 1-Gbit’ный канал подключения к сети Интернет, а также до 6 Tb бесплатного суммарного трафика в месяц.",
		"TEXT_BEFORE_BUTTON" => "",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => "",
		"BUTTON_TYPE" => ""
	),
	false
);?>

<a name="dedicated-server-plans"></a>
<?$APPLICATION->IncludeComponent(
	"tariff:DedicatedServerList",
	".default",
	array(),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"content:SpecialOffer", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Скидка 20% на все физические сервера до 31.12.2018",
		"BUTTON_TEXT" => "Подробнее",
		"BUTTON_LINK" => "/about/news/novost-11/"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"faq:commonList", 
	".default", 
	array(
		"SECTION_ID" => "22",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"content:Recommendations", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Вместе с арендой физического сервера вы получаете:",
		"BUTTON_TEXT" => "Выбрать сервер",
		"BUTTON_LINK" => "#dedicated-server-plans",
		"BENEFITS_LIST" => array(
			0 => "1 Gbit/sec. канал подключения к сети Интернет",
			1 => "Размещение сервера в Дата-центре уровня Tier 3 (отказоустойчивость - 99,982%)",
			2 => "Полные права root на администрирование сервера",
			3 => "Остановка, запуск, перезагрузка работы сервера из личного кабинета клиента",
			4 => "Детальный просмотр статистики потребления трафика",
			5 => "6 Tb бесплатного суммарного интернет-трафика в месяц",
			6 => "2 выделенных IP-адреса",
			7 => "Интерфейс IPMI, KVM консоль (удаленное управление сервером)",
			8 => "Возможность обращений в круглосуточную поддержку 24/7",
		),
		"IMG" => "/local/assets/images/bg/man-and-tetris.png"
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"tariff:SectionBlocks",
	".default",
	array(
		'SECTION_ID' => '3'
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>