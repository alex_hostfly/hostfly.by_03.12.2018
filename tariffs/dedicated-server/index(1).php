<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Hostfly.by предоставляет выделенные сервера для малого и среднего бизнеса по доступной цене.");
$APPLICATION->SetPageProperty("title", "Выделенный сервер с полными правами администрирования | Hostfly.by");
$APPLICATION->SetTitle("Страница");
?>

<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BANNER_CLASS" => "banner-top",
		"IMG" => "/local/assets/images/bg/bg_server.png",
		"TITLE" => "Выделенный физический сервер",
		"DESCRIPTION" => "Dedicated сервер с возможностью самостоятельного администрирования. Вы сможете установить и настроить любое программное обеспечение, а также свободно распоряжаться всеми ресурсами. Наши сервера с одним и двумя процессорами Intel Xeon E3/E5 размещены в одном из лучших Центров обработки данных (ЦОД) в Беларуси, сертифицированном по международному стандарту как Tier 3 (отказоустойчивость - 99,982%), имеют 1-Gbit’ный канал подключения к сети Интернет, а также до 6 Tb бесплатного суммарного трафика в месяц.",
		"TEXT_BEFORE_BUTTON" => "",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => "",
		"BUTTON_TYPE" => ""
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"tariff:DedicatedServerList",
	".default",
	array(),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"content:SpecialOffer", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Скидка 20% на все физические сервера до 31.12.2018",
		"BUTTON_TEXT" => "Подробнее",
		"BUTTON_LINK" => "#"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"faq:commonList",
	".default",
	array(
		'SECTION_ID' => '4'
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"content:Recommendations", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Тарифные планы имеют следующие особенности:",
		"BUTTON_TEXT" => "Список тарифов",
		"BUTTON_LINK" => "#",
		"BENEFITS_LIST" => array(
			0 => "постоянная работа команды поддержки 24/7",
			1 => "веб-интерфейс CentOS Web Panel",
			2 => "до 3GB резервной памяти для DDoS Protection",
			3 => "ежедневное резервное копирование",
			4 => "5 выделенных IP-адресов",
			5 => "полные права на администрирование сервера",
			6 => "интерфейс IPMI, KVM консоль и возможность перезагрузки сервера",
			7 => "неограниченное количество доменов",
			8 => "доступ SSH",
			9 => "установка любой операционной системы",
			10 => "интеграция Google apps и GSuite",
			11 => "оптимально сконфигурированная серверная среда",
			12 => "",
		),
		"IMG" => "/local/assets/images/bg/man-and-tetris.png"
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"tariff:SectionBlocks",
	".default",
	array(
		'SECTION_ID' => '3'
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>