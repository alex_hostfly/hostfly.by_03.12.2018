<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница");
?>

<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BANNER_CLASS" => "banner-top",
		"IMG" => "/local/assets/images/bg/bg_vps.png",
		"TITLE" => "VPS хостинг в облаке с бесплатным администрированием*",
		"DESCRIPTION" => "Мощные виртуальные сервера с возможностью самостоятельного управления и кастомизации установленного ПО. VPS-хостинг не имеет ограничений по количеству сайтов, баз данных и доменных зон. Вы сможете в любой момент изменять конфигурацию своего сервера в зависимости от текущих потребностей. В стоимость тарифов включена защита от DDOoS-атак, системное администрирование вашего сервера нашими специалистами*, а также резервное копирование данных.",
		"TEXT_BEFORE_BUTTON" => "",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => "",
		"BUTTON_TYPE" => ""
	),
	false
);?>

<a name="cloud-server-plans"></a>
<?$APPLICATION->IncludeComponent(
	"tariff:VirtualServerListTabs",
	".default",
	array(),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"content:SpecialOffer", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Скидка 20% на все тарифы до 31.12.2018",
		"BUTTON_TEXT" => "Подробнее",
		"BUTTON_LINK" => "/about/news/novost-11/"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"faq:commonList", 
	".default", 
	array(
		"SECTION_ID" => "21",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"content:Recommendations", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Когда нужен виртуальный сервер в облаке",
		"BUTTON_TEXT" => "Выбрать тариф",
		"BUTTON_LINK" => "#cloud-server-plans",
		"BENEFITS_LIST" => array(
			0 => "Нужно разместить требовательный и высоконагруженный сайт: интернет-проект, интернет-магазин, корпоративный портал",
			1 => "Текущий хостинг сайтов в облаке перестал справляться с нагрузками",
			2 => "Требуются выделенные ресурсы для максимальной производительности",
			3 => "Нужен полный root доступ и возможность контроля, кастомизации и автономности работы собственных веб-приложений и утилит",
			4 => "Требуется повышенный уровень защиты данных",
			5 => "Необходимо резервное копирование с возможностью полного восстановления данных в случае выхода из строя оборудования",
			6 => "",
		),
		"IMG" => "/local/assets/images/bg/man-and-tetris.png"
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"tariff:SectionBlocks",
	".default",
	array(
		'SECTION_ID' => '2'
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>