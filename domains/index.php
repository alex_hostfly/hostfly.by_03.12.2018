<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница");
?>

<? $sliderBanner = [
    "img" => "/local/assets/images/bg/bg_domain.png",
    "slides" => [
        [
            "type" => "СПЕЦИАЛЬНОЕ ПРЕДЛОЖЕНИЕ",
            "name" => "Домены на акции",
            "desc" => "Попробовать сюда перенести все акции",
            "link" => "#",
            "link_text" => "Подробнее",
        ],
        [
            "type" => "СПЕЦИАЛЬНОЕ ПРЕДЛОЖЕНИЕ 2",
            "name" => "Домены на акции 2",
            "desc" => "Попробовать сюда перенести все акции",
            "link" => "#",
            "link_text" => "Подробнее",
        ],
        [
            "type" => "СПЕЦИАЛЬНОЕ ПРЕДЛОЖЕНИЕ 3",
            "name" => "Домены на акции 3",
            "desc" => "Попробовать сюда перенести все акции",
            "link" => "#",
            "link_text" => "Подробнее",
        ]
    ]
]; ?>
<div class="vue-component" data-component="SliderBanner" data-initial='<?= json_encode($sliderBanner, true); ?>'></div>
<!-- /.vue-component -->

<?$APPLICATION->IncludeComponent(
	"content:SearchAndTransferDomain", 
	".default", 
	array(
		"ROOT_CLASS" => "domain-page",
		"DOMAIN_COUNT" => "10",
		"DOMAIN_1_CODE" => "by",
		"DOMAIN_1_TEXT" => ".by",
		"COMPONENT_TEMPLATE" => ".default",
		"DOMAIN_2_CODE" => "com",
		"DOMAIN_2_TEXT" => ".com",
		"DOMAIN_3_CODE" => "ua",
		"DOMAIN_3_TEXT" => ".ua",
		"DOMAIN_4_CODE" => "com",
		"DOMAIN_4_TEXT" => ".com",
		"DOMAIN_5_CODE" => "com",
		"DOMAIN_5_TEXT" => ".com",
		"DOMAIN_6_CODE" => "ru",
		"DOMAIN_6_TEXT" => ".ru",
		"DOMAIN_7_CODE" => "бел",
		"DOMAIN_7_TEXT" => ".бел",
		"DOMAIN_8_CODE" => "ru",
		"DOMAIN_8_TEXT" => ".ru",
		"DOMAIN_9_CODE" => "by",
		"DOMAIN_9_TEXT" => ".by",
		"DOMAIN_10_CODE" => "бел",
		"DOMAIN_10_TEXT" => ".бел"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
    "content:Benefits",
    ".default",
    array(
        "BENEFITS_1_DESCRIPTION" => "Наша компания имеет статус аккредитованного регистратора национальных доменов .BY и .БЕЛ",
        "BENEFITS_1_IMG" => "/local/assets/images/icons/www.svg",
        "BENEFITS_1_TITLE" => "Официальный регистратор доменов .BY и .БЕЛ",
        "BENEFITS_2_DESCRIPTION" => "Возможность парковать, перенаправлять, управлять расширенным редактором DNS зон совершенно бесплатно",
        "BENEFITS_2_IMG" => "/local/assets/images/icons/dns.svg",
        "BENEFITS_2_TITLE" => "DNS-хостинг с каждым доменом",
        "BENEFITS_3_DESCRIPTION" => "Управляй Яндекс почтой на своем домене прямо из личного кабинета my.hostfly.by",
        "BENEFITS_3_IMG" => "/local/assets/images/icons/mail.svg",
        "BENEFITS_3_TITLE" => "Бесплатная почта с каждым доменом",
        "BENEFITS_4_DESCRIPTION" => "Регистрируй домены в режиме реального времени, без задержек, вызванных способом оплаты.",
        "BENEFITS_4_IMG" => "/local/assets/images/icons/reg.svg",
        "BENEFITS_4_TITLE" => "Мгновенная регистрация",
        "BENEFITS_COUNT" => "4",
        "BUTTON_LINK" => "",
        "BUTTON_TEXT" => "",
        "COMPONENT_TEMPLATE" => ".default",
        "TITLE" => "Выбирая домен, вы получаете следующие преимущества:"
    ),
    false
);?>

<?$APPLICATION->IncludeComponent(
    "content:ImageText",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "TITLE" => "У вас уже есть домен? Перенесите его к нам на обслуживание",
        "DESCRIPTION" => "Мы предоставляем бесплатный тестовый период пользования веб-хостингом на первые 14 дней. Ведь никогда не узнаешь, хороша ли услуга, пока не посмотришь на нее изнутри. Мы не будем задавать каких-либо уточняющих вопросов",
        "BUTTON_TEXT" => "Начать перенос",
        "BUTTON_LINK" => "/about/news/novost-11/",
        "ROOT_CLASS" => "special-offer_white",
        "IMAGE" => "/local/assets/images/markup_image/slide4.png"
    ),
    false
);?>

<?$APPLICATION->IncludeComponent(
    "content:SpecialOffer",
    "white",
    array(
        "COMPONENT_TEMPLATE" => "white",
        "TITLE" => "Тестовый период пользования веб-хостингом на первые 14 дней",
        "BUTTON_TEXT" => "Подробнее",
        "BUTTON_LINK" => "/about/news/novost-11/",
        "ROOT_CLASS" => "special-offer_white"
    ),
    false
);?>

<?$APPLICATION->IncludeComponent(
    "faq:commonList",
    ".default",
    array(
        "SECTION_ID" => "25",
        "COMPONENT_TEMPLATE" => ".default",
        "SHOW_TOP_BACKGROUND" => false,
        "WHITE_SECTION_LAST" => true
    ),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>