<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Конструктор сайтов");
?>

<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"BANNER_CLASS" => "banner-top banner-site-constructor",
		"IMG" => "/local/assets/images/bg/bg_site_constructor.png",
		"TITLE" => "Конструктор сайтов",
		"DESCRIPTION" => "Создайте свой собственный профессиональный сайт за 5 минут без программистов, дизайнеров и верстальщиков. За конструктор не нужно платить дополнительно — он встроен в панель управления хостингом. При этом на сайте не будет посторонней рекламы.",
		"BUTTON_TEXT" => "Попробовать бесплатно",
		"BUTTON_LINK" => "#",
		"BUTTON_TYPE" => "default",
		"COMPONENT_TEMPLATE" => ".default",
		"TEXT_BEFORE_BUTTON" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"content:Benefits", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Преимущества конструктора сайтов Hostfly.by",
		"FITS_CLASS" => "site-constructor",
		"BENEFITS_COUNT" => "4",
		"BENEFITS_1_TITLE" => "Простое использование",
		"BENEFITS_1_DESCRIPTION" => "Интуитивно понятный интерфейс позволит легко создать сайт, даже если вы взялись за это впервые.",
		"BENEFITS_1_IMG" => "/local/assets/images/icons/site_constructor/icon-1.png",
		"BENEFITS_2_TITLE" => "Готовый дизайн",
		"BENEFITS_2_DESCRIPTION" => "Выберите свой дизайн из более чем 190 шаблонов различной тематики - и сайт готов. Вам останется только наполнить его контентом.",
		"BENEFITS_2_IMG" => "/local/assets/images/icons/site_constructor/icon-2.png",
		"BENEFITS_3_TITLE" => "Адаптивность сайта",
		"BENEFITS_3_DESCRIPTION" => "Благодаря адаптивной верстке ваш сайт замечательно смотрится на всех типах устройств, от персонального компьютера до планшета и смартфона.",
		"BENEFITS_3_IMG" => "/local/assets/images/icons/site_constructor/icon-3.png",
		"BENEFITS_4_TITLE" => "Поисковая оптимизация",
		"BENEFITS_4_DESCRIPTION" => "Встроенные инструменты поисковой оптимизации позволяют вам улучшить позицию своего сайта в результатах поиска Google, Yandex, Tut.by",
		"BENEFITS_4_IMG" => "/local/assets/images/icons/site_constructor/icon-4.png",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"constructor:HundredTemplates", 
	".default", 
	array(
		"TITLE" => "Более 1000 шаблонов",
		"IMG" => "/local/assets/images/bg/bg_100_tamplates.png",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
    "faq:commonList",
    ".default",
    array(
        'SHOW_TOP_BACKGROUND' => false,
        'SECTION_ID' => '14'
    ),
    false
);?>

<?
$APPLICATION->IncludeComponent(
    'faq:FaqList',
    '.default',
    array(
        'IBLOCK_CODE' => 'faq',
		'SECTION_ID' => 15
    ),
    false
)
?>

<?$APPLICATION->IncludeComponent(
	"constructor:SpecialOfferConstructor", 
	".default", 
	array(
		"TITLE" => "Если вы не нашли ответ на свой вопрос в руководстве, напишите нам на <a href=>info@hostfly.by</a> — мы обязательно поможем.",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>