<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty('title', 'Страница не найдена');
$APPLICATION->SetTitle('Страница не найдена');
?>

<?
$bannerData = [
    'img' => '/local/assets/images/bg/bg_404.png',
    'title' => '404',
    'description' => '<h2>Ошибка!</h2><p>Страница не найдена или не существует. <br>Но вы всегда можете вернуться на <a href="/">главную</a>.</p>',
];
?>

<div class="vue-component" data-component="Banner404" data-initial='<?= json_encode($bannerData); ?>'></div>
<!-- /.vue-component -->

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>