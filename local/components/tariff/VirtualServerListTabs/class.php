<?php

use Local\HostFly\Products\ServerProduct;

class VirtualServerListTabsBaseComponent extends \Local\HostFly\TariffBaseComponent {
	const SECTION_IDS = [23, 24];

    /**
     * @return $this
     */
    protected function prepareData() {

        $this->arResult['data'] = [
            'items' => $this->getItems()
        ];

        return $this;
    }

    protected function _getSections() {
        $sections = [];

        $dbl = CIBlockSection::GetList([], [
            "ID" => self::SECTION_IDS
        ]);

        while ($row = $dbl->fetch()){
            $sections[$row["ID"]] = $row;
        }

        return $sections;
    }

    protected function getItems() {
        $items = [];

        $sections = $this->_getSections();

        foreach (self::SECTION_IDS as $id){
            $rates = $this->getRates($id);
            if (count($rates) > 0){
                $items[] = [
                    "title" => $sections[$id]["NAME"],
                    "initial" => [
                        'rates' => $rates,
                        'priceDictionary' => ServerProduct::PRICE_MODES,
                        'currentPeriodMode' => 'monthly'
                    ]
                ];
            }
        }

        return $items;
    }
}