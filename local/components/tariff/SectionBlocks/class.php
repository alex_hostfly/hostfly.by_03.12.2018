<?php

class TariffSectionBlocksBaseComponent extends \Local\HostFly\TariffBaseComponent {
	
	/**
	 * @param $params
	 * @return array
	 */
	public function onPrepareComponentParams($params) {
		$params = parent::onPrepareComponentParams($params);
		$params['SECTION_ID'] = $params['SECTION_ID'] ?? static::SECTION_ID;
		
		return $params;
	}
	
	/**
	 * @return $this
	 */
	protected function fetchData() {
		$this->arResult['TARIFF_GROUPS'] = $this->fetchNeighborTariffGroups($this->arParams['SECTION_ID']);
		return $this;
	}
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'types' => $this->getNeighborsGroups()
		];
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	protected function getNeighborsGroups() {
		
		$data = [];
		
		foreach($this->arResult['TARIFF_GROUPS'] as $group) {
			
			$data[] = [
				'title' => $group['NAME'],
				'text' => $group['DESCRIPTION'],
				'imageUrl' => $this->getFile($group['PICTURE']),
				'button' => [
					'text' => 'Подробнее',
					'href' => $this->fetchTariffPageByCode($group['CODE']),
				]
			];
		}
		
		return $data;
	}
}