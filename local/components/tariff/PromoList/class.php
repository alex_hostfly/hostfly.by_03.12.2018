<?php

use Local\HostFly\Products\ServerProduct;

class TariffPromoListBaseComponent extends \Local\HostFly\TariffBaseComponent {
	
	/**
	 * @return $this
	 */
	protected function fetchData() {
		
		$this->arResult['TARIFF_GROUPS'] = $this->fetchAllTariffGroups();
		$this->arResult['TARIFFS'] = $this->fetchTariffs();
		
		return $this;
	}
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'rates' => $this->getTariffsData(),
            'priceDictionary' => ServerProduct::PRICE_MODES
		];
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	protected function getTariffsData() {
		
		$data = [];
		
		$this->bindBestTariffsToGroups();
		
		foreach($this->arResult['TARIFF_GROUPS'] as $group) {
			
			/* @var ServerProduct $bestTariff */
			
			$resultGroup = [
				'type' => $group['NAME'],
				'name' => $group['PROPS']['NAME_ALT'],
				'features' => $group['PROPS']['FEATURES'],
			];
			
			$resultGroup['button'] = [
				'text' => 'Тарифы',
				'href' => $this->fetchTariffPageByCode($group['CODE'])
			];
			
			$bestTariff = $group['BEST_TARIFF'];
			
			if($bestTariff === null) {
				continue;
			}

            $resultGroup['hit'] = $bestTariff->getField('HIT') === 'Y';
			$resultGroup['color'] = $bestTariff->getColorClassName();
			
			$priceData = $bestTariff->getPriceData();
			
			$resultGroup['price'] = $priceData;
			
			$resultGroup['description'] = $bestTariff->getDescription();
			
			$data[] = $resultGroup;
		}
		
		return $data;
	}
	
	/**
	 * @return $this
	 */
	protected function bindBestTariffsToGroups() {
		
		$tariffsInSection = [];
		
		foreach($this->arResult['TARIFFS'] as $tariff) {
			
			/* @var ServerProduct $tariff */
			$sectionId = $tariff->getParentSectionId();
			
			$tariffsInSection[$sectionId][] = $tariff;
		}
		
		foreach($tariffsInSection as $sectionId => $tariffs) {
			
			uasort($tariffs, function($element1, $element2) {
				/* @var ServerProduct $element1 */
				/* @var ServerProduct $element2 */
				
				$price1 = $element1->getPriceData()['value'];
				$price2 = $element2->getPriceData()['value'];
				
				if ($price1 == $price2) {
					return 0;
				}
				return ($price1 < $price2) ? -1 : 1;
			});
			
			$bestTariff = array_pop(array_reverse($tariffs));
			
			if(!isset($this->arResult['TARIFF_GROUPS'][$sectionId])) {
				continue;
			}
			
			$this->arResult['TARIFF_GROUPS'][$sectionId]['BEST_TARIFF'] = $bestTariff;
		}
		
		return $this;
	}
}