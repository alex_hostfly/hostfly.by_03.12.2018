<?php

use Local\HostFly\Exchange\Api;
use Local\HostFly\Exchange\ApiExternalException;
use Local\HostFly\Exchange\ApiInternalException;
use Local\HostFly\Products\ServerProduct;
use Local\HostFly\Products\ProductFactory;
use Local\HostFly\Products\ServerSaveException;

class ExchangeComponent extends \Local\HostFly\HostFlyComponent {
	
	const IB_CODE = TARIFFS_IBLOCK_CODE;
	
	/**
	 * @param $params
	 * @return array
	 */
	public function onPrepareComponentParams($params) {
		return $params;
	}
	
	/**
	 * @return ExchangeComponent
	 * @throws ExchangeComponentException
	 */
	protected function updateProducts() {
		
		try {
			
			$api = Api::getInstance();
			$productFactory = new ProductFactory();
			
			$productsData = $api->GetProducts();
			
			$products = $productFactory->getProductsByApiData($productsData);
			
			foreach($products as $product) {
				try {
					/* @var $product ServerProduct */
					$product->save();
					$this->emitMessage("Element <{$product->getName()}> saved.");
				}  catch (ServerSaveException $exception) {
					$this->emitMessage("Failed to save <{$product->getName()}>: {$exception->getMessage()}");
				}
			}
			
		} catch (ApiInternalException $exception) {
			$this->emitMessage("Internal API error: {$exception->getMessage()}");
			throw new ExchangeComponentException();
		} catch (ApiExternalException $exception) {
			$this->emitMessage("External API error: {$exception->getMessage()}");
			throw new ExchangeComponentException();
		} catch (\Bitrix\Main\ArgumentException $exception) {
			$this->emitMessage("Internal Bitrix error: {$exception->getMessage()}");
			throw new ExchangeComponentException();
		}
		
		return $this;
	}
	
	/**
	 * @param $message
	 * @return $this
	 */
	protected function emitMessage($message) {
		
		echo $message . PHP_EOL . PHP_EOL;
		
		return $this;
	}
	
	
	/**
	 * @return $this
	 */
	public function executeComponent() {
		
		echo PHP_EOL;
		
		try {
			$this->updateProducts();
			$this->emitMessage('Import is completed');
		} catch(ExchangeComponentException $exception) {
			$this->emitMessage('Import failed');
		}

		return $this;
	}
}

class ExchangeComponentException extends \Exception {}
class ExchangeCycleException extends ExchangeComponentException {}