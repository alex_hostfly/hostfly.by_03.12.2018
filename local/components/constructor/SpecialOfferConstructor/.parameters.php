<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = [
	'PARAMETERS' => [
        'TITLE' => [
            'NAME'  => 'Заголовок',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '1000',
        ]
    ]
];