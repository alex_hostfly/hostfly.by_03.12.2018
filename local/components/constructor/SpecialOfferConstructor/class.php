<?
declare(strict_types=1);

class SpecialOfferConstructorComponent extends \Local\HostFly\ContentComponent
{
    /**
     * @return $this
     */
    protected function prepareData()
    {
        $this->arResult['data'] = [
            'title' => $this->getTitle(),
        ];

        return $this;
    }

    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return $this->arParams['TITLE'];
    }
}