<?
declare(strict_types=1);

class HundredTemplatesComponent extends \Local\HostFly\ContentComponent
{
    /**
     * @return $this
     */
    protected function prepareData()
    {
        $this->arResult['data'] = [
            'title' => $this->getTitle(),
            'imageUrl' => $this->getImage(),
        ];

        return $this;
    }

    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return $this->arParams['TITLE'];
    }

    /**
     * @return mixed
     */
    protected function getImage()
    {
        return $this->arParams['IMG'];
    }
}