<?

use Local\HostFly\Products\ProductFactory;
use Local\HostFly\Products\ServerProduct;

class FaqCommonList extends \Local\HostFly\HostFlyComponent {
	
	/**
	 * @param $params
	 * @return array
	 */
	public function onPrepareComponentParams($params) {
		
		$params = parent::onPrepareComponentParams($params);

		if ($params['SHOW_TOP_BACKGROUND'] !== false) {
		    $params['SHOW_TOP_BACKGROUND'] = true;
        }
		if ($params['WHITE_SECTION_LAST'] !== true) {
		    $params['WHITE_SECTION_LAST'] = false;
        }

		if($params['SECTION_ID'] === 'all') {
			unset($params['SECTION_ID']);
		}
		
		return $params;
	}
	
	/**
	 * @return $this
	 */
	protected function fetchData() {
		
		$this->arResult['FAQ_ELEMENTS'] = $this->fetchFaqElements();

		return $this;
	}
	
	/**
	 * @return array
	 */
	protected function fetchFaqElements() {
		
		$filter = [
			'IBLOCK_ID' => FAQ_IBLOCK_ID,
		];
		
		$sectionId = intval($this->arParams['SECTION_ID']);
		
		if($sectionId > 0) {
			$filter['SECTION_ID'] = $sectionId;
		}
		
		return $this->getElementsIB([
			'filter' => $filter,
			'select' => [
				'ID', 'NAME', 'DETAIL_TEXT'
			]
		]);
	}
	
	/**
	 * @return array
	 */
	protected function getFaqElements() {
		
		$data = [];
		
		foreach($this->arResult['FAQ_ELEMENTS'] as $element) {
			
			$data[] = [
				'title' => $element['NAME'],
				'text' => $element['DETAIL_TEXT']
			];
		}
		
		return $data;
	}
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
		    'whiteSectionLast' => $this->arParams['WHITE_SECTION_LAST'],
		    'showTopBackground' => $this->arParams['SHOW_TOP_BACKGROUND'],
			'questions' => $this->getFaqElements()
		];
		
		return $this;
	}
}