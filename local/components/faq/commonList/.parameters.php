<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

try {
	
	\Bitrix\Main\Loader::includeModule('iblock');
	
	$dbResult = \Bitrix\Iblock\SectionTable::getList([
		'filter' => [
			'IBLOCK_ID' => FAQ_IBLOCK_ID,
		],
		'select' => [
			'ID',
			'NAME'
		]
	]);
	
	$sections = [
		'all' => 'Все'
	];

	while($section = $dbResult->fetch()) {
		$sections[ $section['ID'] ] = $section['NAME'];
	}
	
	$arComponentParameters = [
		'PARAMETERS' => [
			'SECTION_ID' => [
				'NAME'  => 'Раздел FAQ',
				'PARENT' => 'BASE',
				'TYPE'  => 'LIST',
				'MULTIPLE' => 'N',
				'VALUES' => $sections
			],
		]
	];
	
} catch (\Bitrix\Main\ObjectPropertyException $e) {

} catch (\Bitrix\Main\ArgumentException $e) {

} catch (\Bitrix\Main\SystemException $e) {

} catch (\Bitrix\Main\LoaderException $e) {

}