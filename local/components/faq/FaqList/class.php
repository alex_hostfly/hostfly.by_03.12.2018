<?
declare(strict_types=1);

class FaqListComponent extends \Local\HostFly\HostFlyComponent {

	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'data' => $this->getFaqElements()
		];
		
		return $this;
	}

    /**
     * @return array
     */
	protected function getFaqElements(): array
    {
        $elementsParams = [
            'filter' => [
                'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE'],
            ],
            'select' => [
                'NAME', 'DETAIL_TEXT'
            ]
        ];

        $sectionsParams = [
            'filter' => [
                'SECTION_ID' => $this->arParams['SECTION_ID']
            ],
            'select' => [
                'NAME', 'IBLOCK_ID'
            ]
        ];

        $dataIb = $this->fetchDataIb($elementsParams, $sectionsParams);

        $result = [];

        foreach ($dataIb as $blockData) {
            $resultBlock = [];
            $resultBlock['category'] = $blockData['NAME'];

            foreach ($blockData['ELEMENTS'] as $element) {
                $resultBlock['questions'][] = [
                    'title' => $element['NAME'],
                    'answer' => $element['DETAIL_TEXT']
                ];
            }

            $result[] = $resultBlock;
        }

        return $result;
    }
}