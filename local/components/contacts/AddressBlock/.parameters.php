<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
	'PARAMETERS' => [
        'TITLE' => [
            'NAME'  => 'Заголовок',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '30',
        ],
        'TEXT' => [
            'NAME'  => 'Текст',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'EMAIL' => [
            'NAME'  => 'Email',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '30',
        ]
    ]
];