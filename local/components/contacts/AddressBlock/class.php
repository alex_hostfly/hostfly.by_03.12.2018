<?
declare(strict_types=1);

class AddressBlockComponent extends \Local\HostFly\ContentComponent
{
    /**
     * @return $this
     */
    protected function prepareData()
    {
        $this->arResult['data'] = [
            'title' => $this->getTitle(),
            'text' => $this->getText(),
            'email' => $this->getEmail(),
        ];

        return $this;
    }

    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return $this->arParams['TITLE'];
    }

    /**
     * @return mixed
     */
    protected function getText()
    {
        return $this->arParams['TEXT'];
    }

    /**
     * @return mixed
     */
    protected function getEmail()
    {
        return $this->arParams['EMAIL'];
    }
}