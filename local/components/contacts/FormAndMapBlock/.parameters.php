<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
	'PARAMETERS' => [
        'TITLE' => [
            'NAME'  => 'Заголовок формы',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'COORDINATION_Y' => [
            'NAME'  => 'Координаты балуна на карте (Y)',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'COORDINATION_X' => [
            'NAME'  => 'Координаты балуна на карте (X)',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ]
    ]
];