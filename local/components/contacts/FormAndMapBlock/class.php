<?
declare(strict_types=1);

class FormAndMapBlockComponent extends \Local\HostFly\ContentComponent
{
    protected $errors = [];

    protected $fields = [
        'NAME' => [
            'name' => 'NAME',
            'value' => '',
            'label' => 'Как вас зовут',
            'type' => 'text',
            'required' => ''
        ],
        'EMAIL' => [
            'name' => 'EMAIL',
            'label' => 'Электронная почта',
            'type' => 'email',
            'value' => '',
            'validator' => '\Local\Validators\Email',
            'required' => ''
        ],
        'THEME' => [
            'name' => 'THEME',
            'label' => 'Тема вопроса',
            'type' => 'text',
            'value' => '',
            'required' => ''
        ],
        'COMMENT' => [
            'name' => 'COMMENT',
            'label' => 'Комментарий',
            'type' => 'textarea',
            'rules' => 'textRules',
            'value' => '',
            'required' => ''
        ],
    ];

    /**
     * @return $this
     */
    protected function prepareData()
    {
        $assetManager = \Bitrix\Main\Page\Asset::getInstance();

        $assetManager->addJs('https://maps.googleapis.com/maps/api/js');

        $this->arResult['data'] = [
            'api' => '/local/api/contacts/form/',
            'title' => $this->arParams['TITLE'],
            'coordinationCenterY' => $this->arParams['COORDINATION_Y'],
            'coordinationCenterX' => $this->arParams['COORDINATION_X'],
            'form' => $this->fields
        ];

        return $this;
    }

    /**
     * @return \Local\HostFly\ContentComponent|void
     */
    protected function processApi()
    {
        $success = true;

        try {
            $this->readInputData()->validate();

            if (empty($this->errors)) {
                $this->save();
            } else {
                $success = false;
            }

        } catch (FormException $exception) {
            $success = false;
        }

        $this->arResult['data'] = [
            'success' => $success,
            'errors' => $this->errors
        ];

        $this->printJson();
    }

    /**
     * @return $this
     * @throws FormException
     */
    protected function readInputData()
    {
        $body = json_decode(file_get_contents('php://input'), true);

        if (!$body) {
            throw new FormException('Не получилось прочитать входящий запрос');
        }

        $this->arResult['FORM_DATA'] = $body['fields'];
        return $this;
    }

    /**
     * @return $this
     */
    protected function validate()
    {
        $this->errors = [];

        foreach ($this->arResult['FORM_DATA'] as $code => $value) {

            $data = $this->fields[$code];

            if (isset($data['required'])) {
                if (empty($value)) {
                    $this->errors[$code] = "Поле '{$data['label']}' обязательно для заполнения";
                    continue;
                }
            }

            if (empty($data['validator'])) {
                continue;
            }

            if (!class_exists($data['validator'])) {
                continue;
            }

            if (!method_exists($data['validator'], 'validate')) {
                continue;
            }

            $className = $data['validator'];
            $method = 'validate';

            $result = $className::$method($value);

            if ($result !== true) {
                $this->errors[$code] = $result['error'];
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function save()
    {
        $elementObj = new CIBlockElement();

        $fields = $this->arResult['FORM_DATA'];

        $elementFields = [
            'NAME' => "{$fields['NAME']} ({$fields['EMAIL']})",
            'IBLOCK_ID' => getIblockIdByCode(FEEDBACK_IB_CODE),
            'ACTIVE' => 'Y',
            'PREVIEW_TEXT' => $fields['THEME'],
            'DETAIL_TEXT' => $fields['COMMENT']
        ];

        \CEvent::Send('FEEDBACK_NOTIFY', SITE_ID, $fields, 'Y');

        $newElementId = $elementObj->Add($elementFields);

        return $this;
    }
}

class FormException extends Exception
{
}