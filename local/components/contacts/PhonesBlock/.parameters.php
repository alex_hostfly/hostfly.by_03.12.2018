<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
	'PARAMETERS' => [
        'TITLE_SALE_DEPARTMENT' => [
            'NAME'  => 'Заголовок левого блока',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'MAIN_PHONE_SALE_DEPARTMENT' => [
            'NAME'  => 'Главный телефон для левого блока',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'EMAIL_SALE_DEPARTMENT' => [
            'NAME'  => 'Email левого блока',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'CONTACTS_SALE_DEPARTMENT' => [
            'NAME'  => 'Контакты и график работ для левого блока в формате: НОМЕР|ОПЕРАТОР|ВРЕМЯ_РАБОТЫ',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
            'MULTIPLE' => 'Y',
        ],
        'TITLE_SUPPORT' => [
            'NAME'  => 'Заголовок правого блока',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'TEXT_AFTER_IMAGE' => [
            'NAME'  => 'Текст после картинки правого блока',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'BUTTON_TEXT' => [
            'NAME'  => 'Текст кнопки правого блока',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'BUTTON_HREF' => [
            'NAME'  => 'Ссылка кнопки правого блока',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'EMAIL_RIGHT_BLOCK' => [
            'NAME'  => 'Email правого блока',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
    ]
];