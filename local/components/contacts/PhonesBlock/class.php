<?
declare(strict_types=1);

class PhonesBlockComponent extends \Local\HostFly\ContentComponent
{
    /**
     * @return $this
     */
    protected function prepareData()
    {
        $this->arResult['data'] = [
            'saleDepartment' => [
                'title' => $this->arParams['TITLE_SALE_DEPARTMENT'],
                'mainPhone' => $this->arParams['MAIN_PHONE_SALE_DEPARTMENT'],
                'email' => $this->arParams['EMAIL_SALE_DEPARTMENT'],
            ],
            'support' => [
                'title' => $this->arParams['TITLE_SUPPORT'],
                'textAfterImage' => $this->arParams['TEXT_AFTER_IMAGE'],
                'button' => [
                    'text' => $this->arParams['BUTTON_TEXT'],
                    'href' => $this->arParams['BUTTON_HREF']
                ],
                'email' => $this->arParams['EMAIL_RIGHT_BLOCK']
            ],
        ];

        $this->arResult['data']['saleDepartment']['additionalContacts'] = $this->getContacts();

        $this->arResult['data']['saleDepartment']['workTime'] = $this->getWorkTime();

        return $this;
    }

    /**
     * @return array
     */
    protected function getContacts(): array
    {
        $result = [];
        foreach ($this->arParams['CONTACTS_SALE_DEPARTMENT'] as $contact) {

            $contactParts = explode('|', $contact);
            $contactResult = [];

            if (isset($contactParts[0])) {
                $contactResult['phone'] = $contactParts[0];
            }

            if (isset($contactParts[1])) {
                $contactResult['operator'] = $contactParts[1];
            }

            if (empty($contactResult)) continue;

            $result[] = $contactResult;

        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getWorkTime(): array
    {
        $result = [];
        foreach ($this->arParams['CONTACTS_SALE_DEPARTMENT'] as $contact) {

            $contactParts = explode('|', $contact);

            if (isset($contactParts[2])) {
                $result[] = $contactParts[2];
            }

        }

        return $result;
    }
}