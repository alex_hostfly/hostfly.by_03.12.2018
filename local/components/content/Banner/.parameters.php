<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
	'PARAMETERS' => [
		'BANNER_CLASS' => [
			'NAME'  => 'CSS-класс баннера',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'IMG' => [
			'NAME'  => 'Путь до картинки баннера',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'TITLE' => [
			'NAME'  => 'Заголовок',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'DESCRIPTION' => [
			'NAME'  => 'Описание',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'TEXT_BEFORE_BUTTON' => [
			'NAME'  => 'Текст перед кнопкой',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'BUTTON_TEXT' => [
			'NAME'  => 'Текст кнопки',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'BUTTON_LINK' => [
			'NAME'  => 'Ссылка на кнопке',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'BUTTON_TYPE' => [
			'NAME'  => 'Тип кнопки',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
	]
];