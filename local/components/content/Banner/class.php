<?

class BannerComponent extends \Local\HostFly\ContentComponent {
	
	/**
	 * @return $this
     */
    protected function prepareData()
    {
        $this->arResult['data'] = [
            'bannerClass' => $this->getBannerClass(),
            'img' => $this->getImagePath(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'textBeforeButton' => $this->getTextBeforeButton(),
            'subscribe' => $this->getSubscribe(),
        ];

        $buttonKey = 'button';

        if ($this->arResult['data']['textBeforeButton']) {
            $buttonKey = 'onlineChat';
        }

        $this->arResult['data'][$buttonKey] = $this->getButton();

        return $this;
    }
	
	/**
	 * @return string
	 */
	protected function getBannerClass() {
		return $this->arParams['BANNER_CLASS'];
	}
	
	/**
	 * @return string
	 */
	protected function getImagePath() {
		return $this->arParams['IMG'];
	}
	
	/**
	 * @return string
	 */
	protected function getTitle() {
		return $this->arParams['TITLE'];
	}
	
	/**
	 * @return string
	 */
	protected function getDescription() {
		return $this->arParams['DESCRIPTION'];
	}
	
	/**
	 * @return string
	 */
	protected function getTextBeforeButton() {
		return $this->arParams['TEXT_BEFORE_BUTTON'];
	}

    /**
     * @return string
     */
    protected function getSubscribe() {
        return $this->arParams['SUBSCRIBE'];
    }
	
	/**
	 * @return array
	 */
	protected function getButton() {
		
		if(!$this->arParams['BUTTON_TEXT']) {
			return null;
		}
		
		return [
			'text' => $this->arParams['BUTTON_TEXT'] ?? null,
			'href' => $this->arParams['BUTTON_LINK'] ?? null,
			'btnType' => $this->arParams['BUTTON_TYPE'] ?? null
		];
	}
	
}