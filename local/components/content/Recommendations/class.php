<?php

class RatesRecommendationComponent extends \Local\HostFly\HostFlyComponent {
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'title' => $this->getTitle(),
			'list' => $this->getBenefits(),
			'imageSrc' => $this->getImg(),
			'button' => $this->getButton()
		];
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	protected function getTitle() {
		return $this->arParams['TITLE'];
	}
	
	/**
	 * @return array
	 */
	protected function getBenefits() {
		return array_filter($this->arParams['BENEFITS_LIST'], function ($element) {
			return !empty($element);
		});
	}
	
	/**
	 * @return string
	 */
	protected function getImg() {
		return $this->arParams['IMG'];
	}
	
	/**
	 * @return array
	 */
	protected function getButton() {
		return [
			'text' => $this->arParams['BUTTON_TEXT'],
			'href' => $this->arParams['BUTTON_LINK']
		];
	}
}