<?php

class FooterComponent extends \Local\HostFly\ContentComponent {
	
	protected static $wrappedParamsModel = [
		'SOCIAL_NETWORKS' => [
			'NAME' => 'Социальная сеть',
			'FIELDS' => [
				'CLASS' => [
					'NAME'  => "HTML-Класс",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'LINK' => [
					'NAME'  => "Ссылка",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				]
			]
		],
		'PRETTY_LINKS' => [
			'NAME' => 'Красивая ссылка',
			'FIELDS' => [
				'CLASS' => [
					'NAME'  => "HTML-Класс",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'TITLE' => [
					'NAME'  => "Надпись",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'BUTTON_TEXT' => [
					'NAME'  => "Текст кнопки",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'BUTTON_LINK' => [
					'NAME'  => "Ссылка кнопки",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				]
			]
		]
	];
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'bottomMenu' => $this->getMenu(),
			'prettyLinks' => $this->getPrettyLinks(),
			'cards' => $this->getCards(),
			'socialNetworks' => $this->getSocialNetworks(),
			'copyright' => $this->getCopyright()
		];
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	protected function getMenu() {
		
		global $APPLICATION;
		
		$menu = [];
		
		$bottomMenuRaw = $APPLICATION->IncludeComponent("bitrix:menu","", array(
				"ROOT_MENU_TYPE" => "bottom",
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "bottom",
				"USE_EXT" => "Y",
				"DELAY" => "N",
				"RETURN" => "Y"
			)
		);
		
		foreach($bottomMenuRaw as $element) {
			$menu[] = [
				'text' => $element['TEXT'],
				'href' => $element['LINK']
			];
		}
		
		return $menu;
	}
	
	/**
	 * @return array
	 */
	protected function getPrettyLinks() {
		
		$params = $this->getWrappedParams('PRETTY_LINKS');
		
		$data = [];
		
		foreach($params as $fields) {
			
			$data[] = [
				'class' => $fields['CLASS'],
				'title' => html_entity_decode($fields['TITLE']),
				'buttonText' => $fields['BUTTON_TEXT'],
				'buttonLink' => $fields['BUTTON_LINK']
			];
			
		}
		
		return $data;
	}
	
	/**
	 * @return array
	 */
	protected function getCards() {
                $result = [];
                foreach( $this->arParams['CARDS'] as $sUrlImg ) {
                    $result[] = ['imgUrl' => $sUrlImg];
                }
            
                return $result;
		return array_filter($this->arParams['CARDS'], function($element) {
			return strlen($element) > 0;
		});
	}
	
	/**
	 * @return array
	 */
	protected function getSocialNetworks() {
		
		$params = $this->getWrappedParams('SOCIAL_NETWORKS');
		
		$data = [];
		
		foreach($params as $fields) {
			
			$data[] = [
				'class' => $fields['CLASS'],
				'href' => $fields['LINK'],
			];
		}

		return $data;
	}
	
	/**
	 * @return string
	 */
	protected function getCopyright() {
		return $this->arParams['~COPYRIGHT'];
	}
}