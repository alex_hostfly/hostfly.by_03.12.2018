<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
	'PARAMETERS' => [
		'COPYRIGHT' => [
			'NAME'  => 'Копирайт (html)',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'CARDS' => [
			'NAME'  => 'Картинки платёжных систем',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
			'MULTIPLE' => 'Y'
		],
	]
];

\CBitrixComponent::includeComponentClass($componentName);
$model = FooterComponent::getWrappedParamsModel();
$parametersObject = new \Local\Base\ParamsWrapper($model);
$arComponentParameters = $parametersObject->buildBitrixParameters($arComponentParameters, $arCurrentValues);