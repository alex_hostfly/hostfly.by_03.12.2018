<?php

class ImageText extends \Local\HostFly\ContentComponent {
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'root_class' => $this->getRootClass(),
			'image' => $this->getImage(),
			'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'link' => $this->getLink(),
            'link_text' => $this->getLinkText(),
		];
		
		return $this;
	}

    /**
     * @return string
     */
    protected function getTitle() {
        return $this->arParams['TITLE'];
    }

    /**
     * @return string
     */
    protected function getDescription() {
        return $this->arParams['DESCRIPTION'];
    }

    /**
     * @return string
     */
    protected function getLink() {
        return $this->arParams['BUTTON_LINK'];
    }

    /**
     * @return string
     */
    protected function getLinkText() {
        return $this->arParams['BUTTON_TEXT'];
    }

    /**
     * @return string
     */
    protected function getImage() {
        return $this->arParams['IMAGE'];
    }

    /**
     * @return string
     */
    protected function getRootClass() {
        return $this->arParams['ROOT_CLASS'];
    }
}