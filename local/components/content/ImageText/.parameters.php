<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
    'PARAMETERS' => [
        'ROOT_CLASS' => [
            'NAME'  => 'CSS-класс',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '30',
        ],

        'TITLE' => [
            'NAME'  => 'Заголовок',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '30',
        ],

        'IMAGE' => [
            'NAME'  => 'Изображение',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT'
        ],

        'DESCRIPTION' => [
            'NAME'  => 'Описание',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '30',
        ],

        'BUTTON_TEXT' => [
            'NAME'  => 'Текст кнопки',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '30',
        ],

        'BUTTON_LINK' => [
            'NAME'  => 'Ссылка на кнопке',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '30',
        ]
    ]
];