<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
	'PARAMETERS' => [
		'PHONE' => [
			'NAME'  => 'Телефон',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
	]
];

\CBitrixComponent::includeComponentClass($componentName);
$model = HeaderComponent::getWrappedParamsModel();
$parametersObject = new \Local\Base\ParamsWrapper($model);
$arComponentParameters = $parametersObject->buildBitrixParameters($arComponentParameters, $arCurrentValues);