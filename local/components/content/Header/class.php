<?php

use Local\HostFly\Exchange\Api;

class HeaderComponent extends \Local\HostFly\ContentComponent {
	
	protected static $wrappedParamsModel = [
		'CONTACT' => [
			'NAME' => 'Контакт',
			'FIELDS' => [
				'TITLE' => [
					'NAME'  => 'Название отдела',
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'PHONE' => [
					'NAME'  => 'Основной телефон',
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'EMAIL' => [
					'NAME'  => 'Email',
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'ADDITIONAL_CONTACTS' => [
					'NAME'  => 'Дополнительные данные в формате: НОМЕР|ОПЕРАТОР|ВРЕМЯ_РАБОТЫ',
					'TYPE'  => 'TEXT',
					'SIZE' => '30',
					'MULTIPLE' => 'Y',
					'DEFAULT' => ''
				]
			]
		]
	];
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'phone' => $this->getPhone(),
			'authHref' => $this->getAuthLink(),
			'menu' => $this->getMenu(),
			'contacts' => $this->getContacts()
		];
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	protected function getPhone() {
		return (string)$this->arParams['PHONE'];
	}
	
	/**
	 * @return array
	 */
	protected function getMenu() {
		
		global $APPLICATION;
		
		$menu = [];
		
		$topMenuRaw = $APPLICATION->IncludeComponent("bitrix:menu","", array(
				"ROOT_MENU_TYPE" => "top",
				"MAX_LEVEL" => "2",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "Y",
				"DELAY" => "N",
				"RETURN" => "Y"
			)
		);
		
		$bitrixMenu = [];
		
		$lastRootElement = null;
		$lastRootElementHash = null;
		
		foreach($topMenuRaw as $menuElement) {
			
			$elementHash = sha1($menuElement['TEXT'] . ':' . $menuElement['LINK']);
			
			if($menuElement['DEPTH_LEVEL'] === 1) {
				$lastRootElement = $menuElement;
				$bitrixMenu[ $elementHash ] = $lastRootElement;
				$lastRootElementHash = $elementHash;
			}
			
			if($menuElement['DEPTH_LEVEL'] === 2) {
				$bitrixMenu[ $lastRootElementHash ]['CHILD'][$elementHash] = $menuElement;
			}
		}
		
		foreach($bitrixMenu as $rootElement) {
			
			$preparedRootElement = [
				'text' => $rootElement['TEXT'],
				'href' => $rootElement['LINK'],
				'subMenu' => []
			];
			
			foreach($rootElement['CHILD'] as $childElement) {
				
				$preparedChildElement = [
					'text' => $childElement['TEXT'],
					'href' => $childElement['LINK']
				];
				
				$preparedRootElement['subMenu'][] = $preparedChildElement;
			}
			
			$menu[] = $preparedRootElement;
		}
		
		return $menu;
	}
	
	/**
	 * @return array
	 */
	protected function getContacts() {
		
		$params = $this->getWrappedParams('CONTACT');
		
		$data = [];
		
		foreach($params as $fields) {
			
			$additionalContactsStrings = $fields['ADDITIONAL_CONTACTS'];
			
			$additionalContacts = [];
			
			foreach($additionalContactsStrings as $additionalContactsString) {
				
				if(empty($additionalContactsString)) {
					continue;
				}
				
				$additionalContactsArray = explode('|', $additionalContactsString);
				
				$additionalContactsElement = [
					'phone' => $additionalContactsArray[0] ?? '',
					'operator' => $additionalContactsArray[1] ?? '',
					'schedule' => $additionalContactsArray[2] ?? '',
				];
				
				$additionalContacts[] = $additionalContactsElement;
			}
			
			$data[] = [
				'title' => $fields['TITLE'],
				'mainPhone' => $fields['PHONE'],
				'email' => $fields['EMAIL'],
				'additionalContacts' => $additionalContacts
			];
		}
		
		return $data;
	}
	
	/**
	 * @return string
	 */
	protected function getAuthLink() {
		
		try {
			$api = Api::getInstance();
		} catch (\Local\HostFly\Exchange\ApiInternalException $e) {
			return '#';
		}
		
		return $api->getGateway() . '/clientarea.php';
	}
}