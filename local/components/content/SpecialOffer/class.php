<?php

class SpecialOfferComponent extends \Local\HostFly\ContentComponent {
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'title' => $this->getTitle(),
            'button' => $this->getButton()
		];
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	protected function getTitle() {
		return $this->arParams['TITLE'];
	}
	
	/**
	 * @return array
	 */
	protected function getButton() {
		
		if(!$this->arParams['BUTTON_TEXT']) {
			return null;
		}
		
		return [
			'text' => $this->arParams['BUTTON_TEXT'],
			'href' => $this->arParams['BUTTON_LINK'],
		];
	}
}