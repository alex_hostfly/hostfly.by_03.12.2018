<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
	'PARAMETERS' => [
		'TITLE' => [
			'NAME'  => 'Заголовок',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'BUTTON_TEXT' => [
			'NAME'  => 'Текст кнопки',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
		
		'BUTTON_LINK' => [
			'NAME'  => 'Ссылка на кнопке',
			'PARENT' => 'BASE',
			'TYPE'  => 'TEXT',
			'SIZE' => '30',
		],
	]
];