<?

class SearchAndTransferDomain extends \Local\HostFly\ContentComponent {

    protected static $wrappedParamsModel = [
        'DOMAIN' => [
            'NAME' => 'Домен',
            'FIELDS' => [
                'CODE' => [
                    'NAME'  => "Код",
                    'TYPE'  => 'TEXT',
                    'DEFAULT' => ''
                ],
                'TEXT' => [
                    'NAME'  => "Текст",
                    'TYPE'  => 'TEXT',
                    'DEFAULT' => ''
                ]
            ]
        ],
    ];
	
	/**
	 * @return $this
     */
    protected function prepareData()
    {
        $this->arResult['data'] = [
            'rootClass' => $this->getRootClass(),
            'domains' => $this->getDomains()
        ];

        return $this;
    }

	/**
	 * @return string
	 */
	protected function getRootClass() {
		return $this->arParams['ROOT_CLASS'];
	}

    /**
     * @return array
     */
    protected function getDomains() {

        $params = $this->getWrappedParams('DOMAIN');

        $data = [];

        foreach($params as $fields) {

            $data[] = [
                'code' => $fields['CODE'],
                'text' => $fields['TEXT']
            ];

        }

        return $data;
    }
	
}