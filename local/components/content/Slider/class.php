<?php

class SlidesComponent extends \Local\HostFly\ContentComponent {
	
	protected static $wrappedParamsModel = [
		'SLIDES' => [
			'NAME' => 'Слайд',
			'FIELDS' => [
				'TITLE' => [
					'NAME'  => "Заголовок",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'DESCRIPTION' => [
					'NAME'  => "Описание",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'IMG' => [
					'NAME'  => "Путь до картинки",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'BUTTON_TEXT' => [
					'NAME'  => "Текст кнопки",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				],
				'BUTTON_LINK' => [
					'NAME'  => "Ссылка кнопки",
					'TYPE'  => 'TEXT',
					'DEFAULT' => ''
				]
			]
		],
	];
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'slides' => $this->getSlides()
		];
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	protected function getSlides() {
		
		$params = $this->getWrappedParams('SLIDES');
		
		$data = [];
		
		foreach($params as $fields) {
			
			$data[] = [
				'title' => $fields['TITLE'],
				'description' => $fields['DESCRIPTION'],
				'imageUrl' => $fields['IMG'],
				'button' => [
					'text' => $fields['BUTTON_TEXT'],
					'href' => $fields['BUTTON_LINK'],
				],
			];
			
		}
		
		return $data;
	}
}