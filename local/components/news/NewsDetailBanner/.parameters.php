<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
    'PARAMETERS' => [
        'BANNER_CLASS' => [
            'NAME'  => 'Класс для баннера',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'IMG' => [
            'NAME'  => 'Ссылка на картинку баннера',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'TITLE' => [
            'NAME'  => 'Заголовок',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'DESCRIPTION' => [
            'NAME'  => 'Текст баннера',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'BUTTON_TEXT' => [
            'NAME'  => 'Текст кнопки перехода',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'BUTTON_HREF' => [
            'NAME'  => 'Ссылка для перехода',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ]
    ]
];