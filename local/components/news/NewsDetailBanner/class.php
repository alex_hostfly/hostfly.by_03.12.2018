<?
declare(strict_types=1);

class NewsDetailBanner extends \Local\HostFly\HostFlyComponent {

    /**
     * @return $this
     */
    protected function prepareData() {

        $this->arResult['data'] = [
            'bannerClass' => $this->arParams['BANNER_CLASS'],
            'img' => $this->arParams['IMG'],
            'title' => $this->arParams['TITLE'],
            'description' => $this->arParams['DESCRIPTION'],
            'goToListButton' => [
                'text' => $this->arParams['BUTTON_TEXT'],
                'href' => $this->arParams['BUTTON_HREF']
            ],
        ];

        return $this;
    }
}