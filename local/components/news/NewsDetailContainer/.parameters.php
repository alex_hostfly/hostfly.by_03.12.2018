<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
    'PARAMETERS' => [
        'LAST_NEWS_TITLE' => [
            'NAME'  => 'Заголовок блока последних новостей',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ]
    ]
];