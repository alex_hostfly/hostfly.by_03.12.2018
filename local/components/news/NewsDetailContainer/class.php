<?
declare(strict_types=1);

class NewsDetailContainer extends \Local\HostFly\HostFlyComponent
{
    const NEWS_SECTION_CODE = 'news';

    /**
     * @return $this
     */
    public function executeComponent()
    {
        $this->arResult['data'] = [
            'lastNews' => [
                'title' => $this->arParams['LAST_NEWS_TITLE'],
                'elements' => $this->getLastNews(),
            ],
            'currentNews' => $this->getCurrentNews($_REQUEST['CODE'])
        ];

        $this->prepareDataJson();

        if(!empty($this->arResult['data'])) {
            $this->renderComponent();
        }

        return $this;
    }

    /**
     * @param string $code
     * @return array
     */
    protected function getCurrentNews(string $code): array
    {
        global $APPLICATION;

        $elementParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
                'CODE' => $code
            ],
            'select' => [
                'ID', 'NAME', 'DETAIL_TEXT', 'DETAIL_PICTURE',
                'DATE_CREATE'
            ]
        ];

        $element = $this->getElementsIB($elementParams);

        if (empty($element)) LocalRedirect('/');

        $this->fetchFiles();

        $element = $element[key($element)];

        $iblockData = $this->getIbData(['CODE' => NEWS_IBLOCK_CODE]);

        $element['SEO'] = $this->getSeoData($iblockData['ID'], $element['ID']);

        if ($element['DETAIL_PICTURE']) {
            $element['DETAIL_PICTURE'] = $this->arResult['FILES'][$element['DETAIL_PICTURE']];
        }

        $APPLICATION->SetTitle($element['NAME']);

        $this->setOpenGraphData($element);

        if (isset($element['SEO']['META_TITLE'])) {
            $APPLICATION->SetTitle($element['SEO']['META_TITLE']);
        }

        if (isset($element['SEO']['META_DESCRIPTION'])) {
            $APPLICATION->SetPageProperty("description", $element['SEO']['META_DESCRIPTION']);
        }

        if (isset($element['SEO']['META_KEYWORDS'])) {
            $APPLICATION->SetPageProperty("keywords", $element['SEO']['META_KEYWORDS']);
        }

        $neededSeo = [
            'DETAIL_PICTURE_FILE_ALT',
            'DETAIL_PICTURE_FILE_TITLE',
            'PAGE_TITLE',
        ];

        $image = $element['DETAIL_PICTURE']
            ? Local\Tools\Resizer::resize($element['DETAIL_PICTURE'], null, 500)
            : false;

        return [
            'title' => $element['NAME'] ?? '',
            'date' => FormatDate('j F Y', MakeTimeStamp($element['DATE_CREATE'])),
            'imageUrl' => $image,
            'text' => $element['DETAIL_TEXT'] ?? '',
            'seo' => $this->getSeoDataForClient($neededSeo, $element['SEO']),
        ];
    }

    protected function setOpenGraphData(array $element)
    {
        global $APPLICATION;

        $APPLICATION->SetPageProperty('og:image', getHost() . $element['DETAIL_PICTURE']);
        $APPLICATION->SetPageProperty('og:description', $element['DETAIL_TEXT']);
        $APPLICATION->SetPageProperty('og:title', $element['NAME']);
    }

    /**
     * @return array
     */
    protected function getLastNews(): array
    {
        $elementsParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
                'SECTION_CODE' => static::NEWS_SECTION_CODE,
            ],
            'select' => [
                'PREVIEW_PICTURE', 'DATE_CREATE', 'NAME', 'ID'
            ],
            'nav' => [
                'nTopCount' => 3
            ],
            'order' => ['CREATE_DATE' => 'DESC']
        ];

        $sectionsParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
                'CODE' => static::NEWS_SECTION_CODE
            ],
            'select' => [
                'ID', 'NAME', 'CODE'
            ]
        ];

        $newsDataWithoutPictures = $this->fetchDataIb($elementsParams, $sectionsParams);

        $this->fetchFiles();

        $dataWithPictures = $this->getDataWithPictures($newsDataWithoutPictures);

        $elements = $dataWithPictures[key($dataWithPictures)]['ELEMENTS'];

        $result = [];
        foreach ($elements as $element) {

            $image = $element['PREVIEW_PICTURE']
                ? Local\Tools\Resizer::resize($element['PREVIEW_PICTURE'], null, 205)
                : false;

            $result[] = [
                'imageUrl' => $image,
                'date' => FormatDate('j F Y', MakeTimeStamp($element['DATE_CREATE'])),
                'title' => $element['NAME'],
                'href' => $element['DETAIL_PAGE_URL'],
            ];
        }

        return $result;
    }
}