<?
declare(strict_types=1);

class ShareSocialNetworks extends \Local\HostFly\HostFlyComponent
{
    const NEWS_SECTION_CODE = 'news';

    /**
     * @return $this
     */
    public function executeComponent()
    {
        $this->arResult['data'] = [
            'news' => $this->getNewsData($_REQUEST['CODE'])
        ];

        $this->prepareDataJson();

        if(!empty($this->arResult['data'])) {
            $this->renderComponent();
        }
        return $this;
    }

    protected function getNewsData(string $code): array
    {
        $elementParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
                'CODE' => $code
            ],
            'select' => [
                'ID', 'NAME', 'DETAIL_TEXT', 'PREVIEW_PICTURE',
                'DATE_CREATE'
            ]
        ];

        $element = $this->getElementsIB($elementParams);

        if (empty($element)) LocalRedirect('/');

        $this->fetchFiles();

        $element = $element[key($element)];

        if ($element['PREVIEW_PICTURE']) {
            $element['PREVIEW_PICTURE'] = $this->arResult['FILES'][$element['PREVIEW_PICTURE']];
        }

        return [
            'title' => $element['NAME'] ?? '',
            'previewImage' => getHost() . $element['PREVIEW_PICTURE'],
            'previewText' => $element['DETAIL_TEXT'] ?? '',
            'detailPageUrl' => getHost() . $element['DETAIL_PAGE_URL']
        ];
    }
}