<?
declare(strict_types=1);

class NewsShortList extends \Local\HostFly\HostFlyComponent {

    /**
     * @return $this
     */
    protected function prepareData() {

        $elements = $this->getLastElements();

        $this->arResult['data'] = [
            'title' => $this->arParams['NEWS_TITLE'],
            'elements' => $elements,
            'button' => [
                'text' => $this->arParams['BUTTON_TEXT'],
                'href' => $this->arParams['BUTTON_HREF'],
            ],
        ];

        return $this;
    }

    /**
     * @return array
     */
    protected function getLastElements(): array
    {
        $elementsParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
            ],
            'select' => [
                'ID', 'NAME', 'PREVIEW_PICTURE', 'DATE_CREATE'
            ],
            'order' => ['DATE_CREATE' => 'DESC'],
            'nav' => [
                'nTopCount' => 3
            ]
        ];

        $elements = $this->getElementsIB($elementsParams);

        $this->fetchFiles();

        $modifiedElements = $this->getModifiedElements($elements);

        return $modifiedElements;
    }

    /**
     * @param array $elements
     * @return array
     */
    protected function getModifiedElements(array $elements): array
    {
        $result = [];

        foreach ($elements as $id => $elementData) {

            $image = $elementData['PREVIEW_PICTURE']
                ? Local\Tools\Resizer::resize($this->arResult['FILES'][$elementData['PREVIEW_PICTURE']], null, 210)
                : false;

            $result[] = [
                'imageUrl' => $image,
                'date' => FormatDate('j F Y', MakeTimeStamp($elementData['DATE_CREATE'])),
                'title' => $elementData['NAME'],
                'detailUrl' => $elementData['DETAIL_PAGE_URL'],
            ];
        }

        return $result;
    }
}