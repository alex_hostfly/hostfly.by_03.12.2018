<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
    'PARAMETERS' => [
        'NEWS_TITLE' => [
            'NAME'  => 'Заголовок списка новостей',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'BUTTON_TEXT' => [
            'NAME'  => 'Текст кнопки',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ],
        'BUTTON_HREF' => [
            'NAME'  => 'Ссылка кнопки списка всех новостей',
            'PARENT' => 'BASE',
            'TYPE'  => 'TEXT',
            'SIZE' => '300',
        ]
    ]
];