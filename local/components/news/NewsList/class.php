<?
declare(strict_types=1);

class NewsList extends \Local\HostFly\HostFlyComponent {

    const NEWS_SECTION_CODE = 'news';
    const SHARES_SECTION_CODE = 'shares';

    /**
     * @return $this
     */
    protected function prepareData() {

        $newsElements = $this->getNewsElements();

        $sharesElements = $this->getSharesElements();

        $allData = array_merge($newsElements, $sharesElements);

        $this->arResult['data'] = [
            'tabs' => $this->getTabs($allData),
            'elements' => $this->getAllElementsFromSections($allData)
        ];

        return $this;
    }

    /**
     * @return array
     */
    protected function getSharesElements():array
    {
        $elementsParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
                'SECTION_CODE' => static::SHARES_SECTION_CODE
            ],
            'select' => [
                'ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'ACTIVE_FROM', 'ACTIVE_TO'
            ]
        ];

        $sectionsParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
                'CODE' => static::SHARES_SECTION_CODE
            ],
            'select' => [
                'ID', 'NAME', 'CODE'
            ]
        ];

        $newsDataWithoutPictures = $this->fetchDataIb($elementsParams, $sectionsParams);

        $this->fetchFiles();

        return $this->getDataWithPictures($newsDataWithoutPictures);
    }

    /**
     * @return array
     */
    protected function getNewsElements():array
    {
        $elementsParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
                'SECTION_CODE' => static::NEWS_SECTION_CODE
            ],
            'select' => [
                'ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE',
                'DATE_CREATE'
            ]
        ];

        $sectionsParams = [
            'filter' => [
                'IBLOCK_CODE' => NEWS_IBLOCK_CODE,
                'CODE' => static::NEWS_SECTION_CODE
            ],
            'select' => [
                'ID', 'NAME', 'CODE'
            ]
        ];

        $newsDataWithoutPictures = $this->fetchDataIb($elementsParams, $sectionsParams);

        $this->fetchFiles();

        return $this->getDataWithPictures($newsDataWithoutPictures);
    }

    /**
     * @param array $allData
     * @return array
     */
    protected function getTabs(array $allData):array
    {
        $tabs = [];

        $tabs['all'] = [
            'name' => 'Все',
            'class' => ''
        ];

        foreach ($allData as $sectionData) {

            if (!isset($sectionData['ID'])) return [];

            $tabs[$sectionData['CODE']] = [
                'name' => $sectionData['NAME'],
                'class' => ''
            ];
        }

        return $tabs;
    }

    /**
     * @param array $allData
     * @return array
     */
    protected function getAllElementsFromSections(array $allData):array
    {
        $result = [];

        foreach ($allData as $section) {

            foreach ($section['ELEMENTS'] as $element) {

                $image = $element['PREVIEW_PICTURE']
                    ? Local\Tools\Resizer::resize($element['PREVIEW_PICTURE'], null, 260)
                    : false;

                $modifiedSectionData = [
                    'category' => $section['CODE'],
                    'title' => $element['NAME'],
                    'description' => $element['PREVIEW_TEXT'],
                    'imageUrl' => $image,
                    'detailUrl' => $element['DETAIL_PAGE_URL'] ?? null,
                ];

                if ($section['CODE'] === 'news') {
                    $modifiedSectionData['newsDate'] = FormatDate('j F Y', MakeTimeStamp($element['DATE_CREATE']));
                } elseif ($section['CODE'] === 'shares') {

                    if ($element['ACTIVE_FROM'] && $element['ACTIVE_TO']) {
                        $modifiedSectionData['newsDate'] = [
                            'from' => FormatDate('j F', MakeTimeStamp($element['ACTIVE_FROM'])),
                            'to' => FormatDate('j F Y', MakeTimeStamp($element['ACTIVE_TO']))
                        ];
                    } else {
                        $modifiedSectionData['finishedShare'] = true;
                    }
                }

                $result[] = $modifiedSectionData;
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    protected function getNameOfSectionAll() {
        return $this->arParams['NAME_OF_SECTION_ALL_NEWS'];
    }

    /**
     * @return string
     */
    protected function getNameOfNewsSection() {
        return $this->arParams['NAME_OF_NEWS_SECTION'];
    }

    /**
     * @return string
     */
    protected function getNameOfSharesSection() {
        return $this->arParams['NAME_OF_SHARES_SECTION'];
    }
}