<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
    'PARAMETERS' => [
        'TITLE' => [
            'NAME'  => "Заголовок",
            'TYPE'  => 'TEXT',
            'DEFAULT' => ''
        ],
        'BUTTON_TEXT' => [
            'NAME'  => "Текст кнопки",
            'TYPE'  => 'TEXT',
            'DEFAULT' => ''
        ],
    ]
];