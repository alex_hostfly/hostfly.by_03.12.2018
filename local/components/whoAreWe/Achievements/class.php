<?
declare(strict_types=1);

class AchievementsComponent extends \Local\HostFly\ContentComponent
{
    /**
     * @return $this
     */
    protected function prepareData()
    {
        $this->arResult['data'] = [
            'title' => $this->getTitle(),
            'elements' => $this->getItems(),
            'button' => [
                'text' => $this->getButtonText()
            ],
        ];

        return $this;
    }

    /**
     * @return array
     */
    protected function getItems(): array
    {
        $items = $this->getElementsIB([
            'filter' => [
                'IBLOCK_CODE' => ACHIEVEMENTS_IBLOCK_CODE,
            ],
            'select' => [
                'ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'DETAIL_TEXT',
                'PROPS' => ['POPUP_TITLE']
            ]
        ]);

        $this->fetchFiles();

        $modifiedItems = $this->getModifiedItems($items);

        return $modifiedItems;
    }

    protected function getModifiedItems(array $items): array
    {
        $result = [];

        foreach ($items as $item) {

            $itemData = [];
            $itemData['title'] = $item['NAME'];

            if ($item['PREVIEW_PICTURE']) {
                $itemData['imageUrl'] = Local\Tools\Resizer::resize($this->arResult['FILES'][$item['PREVIEW_PICTURE']], null, 90);
            }

            if (!empty($item['PROPS']['POPUP_TITLE'])) {

                if ($item['DETAIL_PICTURE']) {
                    $itemData['popup']['imageUrl']  = Local\Tools\Resizer::resize($this->arResult['FILES'][$item['DETAIL_PICTURE']], 500, null);
                } else {
                    $itemData['popup']['imageUrl'] = false;
                }

                if ($item['DETAIL_TEXT']) {
                    $itemData['popup']['description'] = $item['DETAIL_TEXT'];
                }

                $itemData['popup']['title'] = $item['PROPS']['POPUP_TITLE'];
            }

            $result[] = $itemData;
        }

        return $result;
    }

    /**
     * @return mixed
     */
    protected function getTitle()
    {
        return $this->arParams['TITLE'];
    }

    /**
     * @return mixed
     */
    protected function getButtonText()
    {
        return $this->arParams['BUTTON_TEXT'];
    }
}