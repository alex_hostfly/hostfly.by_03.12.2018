<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
    'PARAMETERS' => [
        'TITLE' => [
            'NAME'  => "Заголовок",
            'TYPE'  => 'TEXT',
            'DEFAULT' => ''
        ]
    ]
];

\CBitrixComponent::includeComponentClass($componentName);
$model = ClientsComponent::getWrappedParamsModel();
$parametersObject = new \Local\Base\ParamsWrapper($model);
$arComponentParameters = $parametersObject->buildBitrixParameters($arComponentParameters, $arCurrentValues);