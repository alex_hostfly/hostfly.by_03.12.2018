<?
declare(strict_types=1);

class ClientsComponent extends \Local\HostFly\ContentComponent {

    protected static $wrappedParamsModel = [
        'CLIENTS' => [
            'NAME' => 'Логотипы клиентов',
            'FIELDS' => [
                'SLIDE' => [
                    'NAME'  => "Логотип",
                    'TYPE'  => 'TEXT',
                    'DEFAULT' => ''
                ]
            ]
        ]
    ];

	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
		    'title' => $this->getTitle(),
            'slides' => $this->getSlides()
		];
		
		return $this;
	}

	protected function getTitle()
    {
        return $this->arParams['TITLE'];
    }

    /**
     * @return array
     */
	protected function getSlides(): array
    {
        $params = $this->getWrappedParams('CLIENTS');

        $data = [];

        foreach($params as $fields) {
            $data[] = $fields['SLIDE'];
        }

        return $data;
    }
}