<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

$arComponentParameters = [
    'PARAMETERS' => []
];

\CBitrixComponent::includeComponentClass($componentName);
$model = BlockAboutUsComponent::getWrappedParamsModel();
$parametersObject = new \Local\Base\ParamsWrapper($model);
$arComponentParameters = $parametersObject->buildBitrixParameters($arComponentParameters, $arCurrentValues);