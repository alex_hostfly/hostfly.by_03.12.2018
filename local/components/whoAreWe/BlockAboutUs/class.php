<?
declare(strict_types=1);

class BlockAboutUsComponent extends \Local\HostFly\ContentComponent {

    protected static $wrappedParamsModel = [
        'BLOCK_ABOUT_US' => [
            'NAME' => 'Инфоблок',
            'FIELDS' => [
                'TITLE' => [
                    'NAME'  => "Заголовок",
                    'TYPE'  => 'TEXT',
                    'DEFAULT' => ''
                ],
                'DESCRIPTION' => [
                    'NAME'  => "Описание",
                    'TYPE'  => 'TEXT',
                    'DEFAULT' => ''
                ],
                'IMG' => [
                    'NAME'  => "Путь до картинки",
                    'TYPE'  => 'TEXT',
                    'DEFAULT' => ''
                ],
                'ADVANTAGE_TITLE' => [
                    'NAME'  => "Заголовок выгоды",
                    'TYPE'  => 'TEXT',
                    'DEFAULT' => ''
                ],
                'ADVANTAGE_TEXT' => [
                    'NAME'  => "Описание выгоды",
                    'TYPE'  => 'TEXT',
                    'DEFAULT' => ''
                ]
            ]
        ]
    ];

	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
            'blocks' => $this->getBlocks()
		];
		
		return $this;
	}

    /**
     * @return array
     */
	protected function getBlocks(): array
    {
        $params = $this->getWrappedParams('BLOCK_ABOUT_US');

        $data = [];

        foreach($params as $fields) {
            $data[] = [
                'title' => $fields['TITLE'],
                'description' => $fields['DESCRIPTION'],
                'imageUrl' => $fields['IMG'],
                'advantages' => [
                    'title' => $fields['ADVANTAGE_TITLE'],
                    'text' => $fields['ADVANTAGE_TEXT']
                ]
            ];
        }

        return $data;
    }
}