<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Контакты</title>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/134893/SVGMarker.js"></script>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app">
    <?
include 'include/header.php';
?>

<?
$addressBlockData = [
    'title' => 'Контакты',
    'text' => 'ООО «ХостФлай», проспект Победителей, дом 00, офис 200<br>г. Минск, 220020, Республика Беларусь',
    'email' => 'info@hostfly.by',
];
?>

<div class="vue-component" data-component="AddressBlock" data-initial='<?= json_encode($addressBlockData); ?>'></div>
<!-- /.vue-component -->

<?
$phonesBlockData = [
    'saleDepartment' => [
        'title' => 'Отдел продаж',
        'mainPhone' => '+375 17 567-12-34',
        'email' => 'sales@hostfly.by',
        'additionalContacts' => [
            [
                'phone' => '+375 29 123-12-12',
                'operator' => 'Velcom',
            ],
            [
                'phone' => '+375 29 123-12-12',
                'operator' => 'MTS',
            ],
            [
                'phone' => '+375 29 123-12-12',
                'operator' => 'Life',

            ],
            [
                'phone' => '+375 29 123-12-12',
                'operator' => 'Факс',
            ],
        ],
        'workTime' => [
            'Пн-Пт: с 9:00 до 20:00',
            'Сб-Вс: с 9:00 до 21:00',

        ]
    ],
    'support' => [
        'title' => 'Техническая поддержка',
        'textAfterImage' => "Круглосуточно <br>+375 17 123-45-67",
        'button' => [
            'text' => 'online-чат',
            'href' => '#'
        ],
        'email' => 'support@hostfly.by'
    ],
];
?>

<div class="vue-component" data-component="PhonesBlock" data-initial='<?= json_encode($phonesBlockData); ?>'></div>
<!-- /.vue-component -->

<?
$formAndMapBlockData = [
    'title' => 'Написать нам',
    'button' => 'Отправить',
    'coordinationCenterY' => 30.267074,
    'coordinationCenterX' => -97.743473,
    'form' => [
            'NAME' => [
                    'name'=>'NAME',
                    'value' => '',
                    'label' => 'Как вас зовут',
                    'type'        => 'text',
			        'required'    => false,
            ],
            'EMAIL' => [
            'name' => 'EMAIL',
            'label' => 'Электронная почта',
            'type'        => 'email',

            ],
            'THEME' => [
            'name' => 'THEME',
            'label' => 'Тема вопроса',
            'type'        => 'text',
            ],
            'COMMENT' => [
            'name' => 'COMMENT',
            'label' => 'Комментарий',
            'type'        => 'textarea',
                'rules'    => 'textRules',
            ],
    ]
];
?>

<div class="vue-component" data-component="FormAndMapBlock"
     data-initial='<?= json_encode($formAndMapBlockData); ?>'></div>
<!-- /.vue-component -->

<?
include 'include/footer.php';
?>

<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</div>
<!-- /#app -->
</body>
</html>
