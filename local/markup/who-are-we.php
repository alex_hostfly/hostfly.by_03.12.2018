<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Кто мы</title>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app">
<?
include 'include/header.php';
?>

<?
$bannerData = [
    'img' => '/local/assets/images/bg/bg_main.png',
    'title' => 'HOSTFLY — хостинг от профессионалов',
    'description' => 'HOSTFLY— это провайдер облачных решений, профессиональный хостинг и регистрация доменов. От простой регистрации домена до выстраивания сложнейшей облачной инфраструктуры и ее администрирования.',
    'button' => [
        'text' => 'Попробовать бесплатно',
        'href' => '#',
    ]
];
?>

<div class="vue-component" data-component="ButtonBanner" data-initial='<?= json_encode($bannerData); ?>'></div>
<!-- /.vue-component -->

<?
$benefitsData = [
    'title' => "Быстрый и масштабируемый хостинг мирового класса — у вас под рукой",
    'benefits' => [
        [
            'title' => 'Мощные процессоры',
            'description' => 'Бесперебойня работа на уровне 99,98% благодаря профессиональному серверному и сетевому оборудованию.',
            'imageUrl' => '/local/assets/images/icons/download_cloud.svg',
        ],
        [
            'title' => 'Персональные  SSL
сертификаты',
            'description' => 'Подключение SSL-сертификата обеспечивает работу сайта через защищённый, шифрованный канал.',
            'imageUrl' => '/local/assets/images/icons/protect_page.svg',
        ],
        [
            'title' => 'Оптимизированный WordPress ',
            'description' => 'Доступны тысячи бесплатных тем и плагинов, которые позволяют упростить разработку сайта.',
            'imageUrl' => '/local/assets/images/icons/connect-world.svg',
        ],
        [
            'title' => 'Конструктор сайтов',
            'description' => 'Самый простой способ создать сайт бесплатно, не прибегая к помощи программистов, дизайнеров и верстальщиков. ',
            'imageUrl' => '/local/assets/images/icons/customise_site.svg',
        ],
        [
            'title' => 'Оперативная и бесплатная техподдержка',
            'description' => 'Вы бесплатно получаете услуги администрирования премиум-класса, с правом на неограниченное количество запросов.',
            'imageUrl' => '/local/assets/images/icons/pretty-link-leaf.svg',
        ],
        [
            'title' => 'Простое управление',
            'description' => 'Удобная контрольная панель, позволяющая управлять всеми сайтами, размещенными на разных серверах.',
            'imageUrl' => '/local/assets/images/icons/windows.svg',
        ],
        [
            'title' => 'Система уведомлений',
            'description' => 'Вы получите уведомления о скором окончании оплаченного периода по e-mail, SMS и по телефону.',
            'imageUrl' => '/local/assets/images/icons/mail-alert.svg',
        ],
        [
            'title' => 'Максимум лояльности',
            'description' => 'Даже если вы превысите предоставленные по договору лимиты ресурсопотребления (например, во время активной рекламной кампании), ваш сайт не заблокируют.',
            'imageUrl' => '/local/assets/images/icons/friends.svg',
        ]
    ]
];
?>

<div class="vue-component" data-component="Benefits" data-initial='<?= json_encode($benefitsData); ?>'></div>
<!-- /.vue-component -->

<?
$clientsData = [
    'title' => 'Более 1000 довольных клиентов',
    'slides' => [

            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/atlant.png',
            '/local/assets/images/partners/bmw.png',
            '/local/assets/images/partners/cola.png',
            '/local/assets/images/partners/komunarka.png',
            '/local/assets/images/partners/lego.png',
            '/local/assets/images/partners/lidskae.png',
            '/local/assets/images/partners/maz.png',
            '/local/assets/images/partners/nokia.png',
            '/local/assets/images/partners/vitex.png',

            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/atlant.png',
            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/cola.png',
            '/local/assets/images/partners/komunarka.png',
            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/lidskae.png',
            '/local/assets/images/partners/maz.png',
            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/vitex.png',

            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/atlant.png',
            '/local/assets/images/partners/bmw.png',
            '/local/assets/images/partners/cola.png',
            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/lidskae.png',
            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/apple.png',
            '/local/assets/images/partners/vitex.png',




        ]
];
?>

<div class="vue-component" data-component="Clients" data-initial='<?= json_encode($clientsData); ?>'></div>
<!-- /.vue-component -->

<?
$blockAboutUs = [
    'blocks' => [
        [
            'title' => 'Круглосуточный мониторинг состояния серверов',
            'description' => 'Наш веб-хостинг построен на современном оборудовании, которое было тщательно отобрано для его превосходного качества. Мы тщательно тестируем все аппаратные средства, чтобы обеспечить соответствие наших высоких стандартов.',
            'imageUrl' => '/local/assets/images/bg/care.png',
            'advantages' => [
                'title' => 'Оперативная техподдержка',
                'text' => 'Решим любую техническую задачу в течение  1 часа без приобретения дополнительных платных пакетов.'
            ]
        ],
        [
            'title' => 'Наша команда',
            'description' => 'Обладая 17-летним опытом и тысячами размещенных веб-сайтов на наших серверах, наши сотрудники знают все о веб-хостинге. Каждый член нашей команды полностью обучен нашим услугам, поэтому, когда вы разговариваете с нами, вы можете быть уверены, что мы знаем, о чем мы говорим.
',
            'imageUrl' => '/local/assets/images/bg/team.png',
            'advantages' => [
                'title' => 'Почему мы',
                'text' => 'Мы профессионалы своего дела и ценим ваше время, поэтому нам можно доверять свой бизнес.'
            ]
        ],
        [
            'title' => 'Мы за инновации',
            'description' => 'Наш веб-хостинг построен на современном оборудовании, которое было тщательно отобрано для его превосходного качества. Мы тщательно тестируем все аппаратные средства, чтобы обеспечить соответствие наших высоких стандартов.',
            'imageUrl' => '/local/assets/images/bg/innovation.png',
            'advantages' => [
                'title' => 'Современное оборудование',
                'text' => 'Только самое современное оборудование высшего качества.'
            ]
        ],
    ]
];
?>


<div class="vue-component" data-component="BlockAboutUs" data-initial='<?= json_encode($blockAboutUs); ?>'></div>
<!-- /.vue-component -->

<?
$achievementsData = [
    'title' => 'Наши награды и сертификаты',
    'elements' => [
        [
            'imageUrl' => '/local/assets/images/awards/microsoft.png',
            'title' => 'Microsoft Gold Partner',
            'href' => '',
            'popup' => [
                'title' => 'Хостинг-партнер 1С-Битрикс',
                'imageUrl' => '/local/assets/images/awards/1c-logo.png',
                'description' => 'Ура! мы стали хостинг-партнером 1С Битрикс.Текстовое сопровождеие к награде ии сертификату, если нет самой бумаги. Здесь можно посавить картинку, наример логотип, и написать текст, почему этот сертификат крутой.'
            ]
        ],
        [
            'imageUrl' => '/local/assets/images/awards/jsonparners.png',
            'title' => '3-е место в рейтинге крупнейших IaaS провайдеров России’2012 - J’son & Partners',
            'href' => '#',
            'popup' => [
                'title' => 'Свидетельство о регистрации резидента Парка высоких технологий',
                'imageUrl' => '/local/assets/images/awards/sert.png',
            ]
        ],
        [
            'imageUrl' => '/local/assets/images/awards/webhosts.png',
            'title' => '1-е место среди хостинг-провайдеров Беларуси по данным WebHosting.info',
            'href' => '#',
        ],
        [
            'imageUrl' => '/local/assets/images/awards/cnews.png',
            'title' => '5-е место в рейтинге крупнейших SaaS-провайдеров России’2012 - Cnews',
            'href' => '#',
        ],
        [
            'imageUrl' => '/local/assets/images/awards/parkvys.png',
            'title' => 'Свидетельство о регистрации резидента Парка высоких технологий',
            'href' => '#',
        ],
        [
            'imageUrl' => '/local/assets/images/awards/bx-host.png',
            'title' => 'Хостинг-партнер 1С-Битрикс',
            'href' => '#',
        ],
        [
            'imageUrl' => '/local/assets/images/awards/bx-business.png',
            'title' => 'Бизнес-партнер 1С-Битрикс',
            'href' => '#',
        ],
        [
            'imageUrl' => '/local/assets/images/awards/lits.png',
            'title' => 'Лицензия на деятельность в области связи',
            'href' => '#',
        ],

    ],
    'button' => [
        'text' => 'Показать больше сертификатов'
    ],
];
?>

<div class="vue-component" data-component="Achievements" data-initial='<?= json_encode($achievementsData); ?>'></div>
<!-- /.vue-component -->

<?
include 'include/footer.php';
?>

<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</div>
<!-- /#app -->
</body>
</html>
