<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Веб хостинг</title>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app"  class="hosting-style">
<?
include 'include/header.php';
?>

<?
$bannerData = [
    'bannerClass' => 'banner-top',
    'img' => '/local/assets/images/bg/bg_host.png',
    'title' => 'Облачный веб-хостинг — быстрый, надежный ',
    'description' => 'Мы предоставляем общий облачный хостинг для предприятий малого и среднего бизнеса по доступной цене. Наша простая в использовании панель управления имеет все необходимые инструменты, чтобы сделать ваш сайт успешным.',
];
?>

<div class="vue-component" data-component="ButtonBanner" data-initial='<?= json_encode($bannerData); ?>'></div>
<!-- /.vue-component -->

<?
$ratesData = [
    'rates' => [
        [
            'period' => '1 месяц',
            'name' => 'Мини Cloud',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Идеально подходит для личных веб- сайтов и блогов. Сэкономьте 10% — зарегестрируйтесь на 2 года.',
            'features' => [
                [
                    'name' => 'Кол-во сайтов',
                    'value' => '1',
                ],
                [
                    'name' => 'Трафик',
                    'value' => 'unlim-ico',
                ],
                [
                    'name' => 'SSD хранилище, Гб',
                    'value' => 'unlim-ico',
                ],
                [
                    'name' => 'Память, Гб',
                    'value' => '2',
                ],
                [
                    'name' => 'Процессор',
                    'value' => '2 core',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
            'buttonToTry' => [
                'text' => 'Попробовать бесплатно',
                'href' => '#'
            ]
        ],
        [
            'period' => '1 месяц',
            'name' => 'Cтандарт Cloud',
            'price' => [
                'value' => '49.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '49.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Отлично подходит для малого и среднего бизнеса. Сэкономьте 10% с подпиской на 1 год или 20% — на 2 года.',
            'features' => [
                [
                    'name' => 'Кол-во сайтов',
                    'value' => '1',
                ],
                [
                    'name' => 'Трафик',
                    'value' => 'unlim-ico',
                ],
                [
                    'name' => 'SSD хранилище, Гб',
                    'value' => 'unlim-ico',
                ],
                [
                    'name' => 'Память, Гб',
                    'value' => '2',
                ],
                [
                    'name' => 'Процессор',
                    'value' => '2 core',
                ],
                [
                    'name' => 'Сертификаты',
                    'value' => 'Lets Encrypt',
//                    'value' => 'Let\'s Encrypt',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '1 месяц',
            'name' => 'Cloud хостинг 1 month',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'Кол-во сайтов',
                    'value' => '1',
                ],
                [
                    'name' => 'Трафик',
                    'value' => 'unlim-ico',
                ],
                [
                    'name' => 'SSD хранилище, Гб',
                    'value' => 'unlim-ico',
                ],
                [
                    'name' => 'Память, Гб',
                    'value' => '2',
                ],
                [
                    'name' => 'Процессор',
                    'value' => '2 core',
                ],
                [
                    'name' => 'Сертификаты',
//                    'value' => 'Let\'s Encrypt',
                    'value' => 'Lets Encrypt',
                ],
                [
                    'name' => 'Выделенный IP фдрес',
                    'value' => '1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '3 месяца',
            'name' => 'Cloud хостинг 3 months',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '3 месяца',
            'name' => 'Cloud хостинг 3 months',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '3 месяца',
            'name' => 'Cloud хостинг 3 months',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '1 год',
            'name' => 'Cloud хостинг 1 year',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '1 год',
            'name' => 'Cloud хостинг 1 year',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '1 год',
            'name' => 'Cloud хостинг 1 year',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '2 года',
            'name' => 'Cloud хостинг 2 years',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '2 года',
            'name' => 'Cloud хостинг 2 years',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ],
        [
            'period' => '2 года',
            'name' => 'Cloud хостинг 2 years',
            'price' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'discountPrice' => [
                'value' => '10.50',
                'currency' => 'рублей',
                'period' => 'месяц',
            ],
            'description' => 'Описание',
            'features' => [
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ],
                [
                    'name' => 'хараткеристика 1',
                    'value' => 'значение 1',
                ]
            ],
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => '#'
            ],
        ]
    ]
];
?>

<div class="vue-component" data-component="RatesFullList" data-initial='<?= json_encode($ratesData); ?>'></div>
<!-- /.vue-component -->

<?
$specialOfferData = [
    'title' => 'Тариф на веб-хостинг для детских специальных учреждений',
    'button' => [
        'text' => 'Подробнее',
        'href' => '#'
    ]
];
?>

<div class="vue-component" data-component="SpecialOffer" data-initial='<?= json_encode($specialOfferData); ?>'></div>
<!-- /.vue-component -->

<?
$ratesFaqData = [
    'questions' => [
        [
            'title' => 'Что такое хостинг-аккаунт?',
            'text' => '<div>
<p>Прежде чем начать создавать сайт, заниматься его наполнением и продвижением нужно четко осознать, какой именно тип хостинга выбрать для будущего проекта. Необходимо определить ресурсоемкость сайта, для того, чтобы подобрать для себя оптимальный тип хостинга. Ведь именно исходя из нагрузки на сайт, выбирается хостинг.</p>
<p>
<img src="/local/assets/images/markup_image/servers.png" alt="">
<span class="note color--gray">Поясняющая подпись к картинке, если она нужна.</span>
<!-- /.note -->
</p>
<h3>Какой хостинг выбрать для сайта?</h3>
<ul>
<li>Если у вас блог, небольшой портал или каталог статей, то берите самый дешевый <a href="#">веб-хостинг</a>. Никакие нагрузки вам страшны не будут</li>
<li>Если у вас посещаемый сайт от 2к. и выше, киносайт, и тому подобное, берите выделенный <a href="#">виртуальный сервер</a> (VPS). После правильной настройки сайт никогда не будет нагружать хостинг</li>
<li>Если вы счастливый обладатель высоконагруженного проекта, с огромным трафиком и большим количеством скачиваний, необходимо брать <a href="#">выделенный сервер VDS.</a></li>
</ul>
<p>
<img src="/local/assets/images/markup_image/scheme.png" alt="">
</p>
<h3>Стоимость дополнительных услуг</h3>
<div class="mobile-table-wrap">
 <table>
    <tr>
        <td>Название услуги</td>
        <td>Примечание</td>
        <td>Стоимость</td>
    </tr>
    <tr>
        <td><b>Добавочные 1 Гб SSD</b></td>
        <td></td>
        <td><b>10,50</b> рублей в месяц</td>
    </tr>
    <tr>
        <td><b>Дополнительный VPS на сервере</b></td>
        <td></td>
        <td><b>20,00</b> рублей в месяц</td>
    </tr>
    <tr>
        <td><b>Дополнительный IP-адрес</b></td>
        <td>-10% при годовой подписке</td>
        <td><b>12,00</b> рублей в месяц</td>
    </tr>
    <tr>
        <td><b>Дополнительный IP-адрес</b></td>
        <td>-10% при годовой подписк</td>
        <td><b>12,00</b> рублей в месяц</td>
    </tr>
    <tr>
        <td><b>Back-up сервера с администрированием</b></td>
        <td>Хранение копий на внешнем сервере</td>
        <td><b>20,00</b> рублей в месяц</td>
    </tr>
</table>
 <!-- /.decorated-table table--pinky -->
</div>
</div>',
        ],
        [
            'title' => 'Сколько сайтов я могу разместить на одном аккаунте?',
            'text' => 'Вы можете размещать неограниченное количество сайтов в рамках одного аккаунта на Unix-платформе. Лимиты всех тарифных планов касаются лишь процессорного времени, дискового пространства и количества баз данных. Лимиты на процессорное время являются среднесуточными. В моменты пиковой нагрузки ваши сайты могут потребить и 100% ресурсов ядра процессора, однако общая загрузка за сутки не должна превышать ограничений тарифного плана.',
        ],
        [
            'title' => 'Полное заполнение раздела

',
            'text' => 'тект 1',
        ],
        [
            'title' => 'Как создать поддомен?',
            'text' => 'тект 1',
        ],
        [
            'title' => 'Что если у меня уже есть доменное имя?',
            'text' => 'тект 1',
        ],
        [
            'title' => 'Как осуществляется трансфер доменного имени между регистраторами?',
            'text' => 'тект 1',
        ],
    ]

];
?>

<div class="vue-component" data-component="RatesFaq" data-initial='<?= json_encode($ratesFaqData); ?>'></div>
<!-- /.vue-component -->

<?
$rateRecommedationData = [
    'title' => 'Вам подходит веб-хостинг, если',
    'list' => [
        'вы хотите разместить сайты с невысоким потреблением ресурсов,',
        'планируете посещаемость на уровне 300-400 человек в день,',
        'вам достаточно стандартных мер по обеспечению конфиденциальности вашей информации,',
        'вы хотите разместить сайты с невысоким потреблением ресурсов,',
        'планируете посещаемость на уровне 300-400 человек в день.',
    ],
    'imageSrc' => '/local/assets/images/bg/man-and-tetris.png',
    'button' => [
        'text' => 'Список тарифов',
        'href' => '#',
    ]
];
?>

<div class="vue-component" data-component="RatesRecommendations" data-initial='<?= json_encode($rateRecommedationData); ?>'></div>
<!-- /.vue-component -->

<?
$typesRatesData = [
    'types' => [
        [
            'title' => 'Виртуальный сервер',
            'text' => 'Отличное решение, если ваши потребности вышли за рамки виртуального хостинга.',
            'imageUrl' => '/local/assets/images/bg/pinky-card.png',
            'button' => [
                'text' => 'Подробнее',
                'href' => '#',
            ]
        ],
        [
            'title' => 'Выделенный сервер',
            'text' => 'Физически отдельный сервер, полностью находящийся в вашем распоряжении.',
            'imageUrl' => '/local/assets/images/bg/sea-card.png',
            'button' => [
                'text' => 'Подробнее',
                'href' => '#',
            ]
        ]
    ]
];
?>

<div class="vue-component" data-component="TypesRates" data-initial='<?= json_encode($typesRatesData); ?>'></div>
<!-- /.vue-component -->

<?
include 'include/footer.php';
?>

<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</div>
<!-- /#app -->
</body>
</html>
