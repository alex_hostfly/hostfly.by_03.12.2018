<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Список страниц</title>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app">
<?
include 'include/header.php';
?>
<div class="container page-content" style="min-height: 70vh;">
    <h1 style="padding-top: 100px; margin-bottom: 15px;">Список страниц</h1>
    <ul>
        <li>
            <a href="main.php">Главная</a>
        </li>
        <li>
            <a href="web-hosting.php">Веб-хостинг</a>
        </li>
        <li>
            <a href="vps.php">VPS</a>
        </li>
        <li>
            <a href="dedicated-server.php">Выделенный сервер</a>
        </li>
        <li>
            <a href="site-constructor.php">Конструктор сайтов</a>
        </li>
        <li>
            <a href="news.php">Новости</a>
        </li>
        <li>
            <a href="news-detail.php">Новости детальная</a>
        </li>
        <li>
            <a href="faq.php">FAQ</a>
        </li>
        <li>
            <a href="contacts.php">Контакты</a>
        </li>
        <li>
            <a href="who-are-we.php">О нас</a>
        </li>
        <li>
            <a href="404.php">404</a>
        </li>
    </ul>
</div>
<!-- /.container page-content -->
<hr>
<?
include 'include/footer.php';
?>



<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</div>
<!-- /#app -->
</body>
</html>

