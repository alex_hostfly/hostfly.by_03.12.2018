<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Главная</title>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app">

    <?
    include 'include/header.php';
    ?>

    <?
    $bannerData = [
        'img' => '/local/assets/images/bg/bg_404.png',
        'title' => '404',
        'description' => '<h2>Ошибка!</h2><p>Страница не найдена или не существует. <br>Но вы всегда можете вернуться на <a href="/">главную</a>.</p>',
    ];
    ?>

    <div class="vue-component" data-component="Banner404" data-initial='<?= json_encode($bannerData); ?>'></div>
    <!-- /.vue-component -->

</div>
<!-- /#app -->
<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</body>
</html>
