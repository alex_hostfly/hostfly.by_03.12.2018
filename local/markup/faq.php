<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FAQ</title>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app">
<?
include 'include/header.php';
?>

<?
$bannerData = [
    'bannerClass' => 'banner-top',
    'img' => '/local/assets/images/bg/bg_main.png',
    'title' => 'Часто задаваемые вопросы',
    'description' => 'Специально для вас мы собрали список самых популярных вопросов. Но если вы н нашли ответа — у нас работает круглосуточная техподдержка',
    'onlineChat' => [
        'text' => 'Online-чат',
        'href' => '#'
    ],
    'textBeforeButton' => "Круглосуточно <br> +375 17 123-45-67"
];
?>

<div class="vue-component" data-component="OnlineChatBanner" data-initial='<?= json_encode($bannerData); ?>'></div>
<!-- /.vue-component -->

<?
$faqData = [
    'data' => [
        [
            'category' => 'Популярные вопросы',
            'questions' => [
                [
                    'title' => 'Как сменить владельца домена .BY, .БЕЛ?
',
                    'answer' => '
<h3>Текущему владельцу:</h3>
<p><b>Для физических лиц и индивидуальных предпринимателей, работающих без печати:</b>
<p>Приехать с паспортом в наш офис (г. Минск, пр-т Дзержинского, 57, 8 этаж) и написать заявление на смену владельца домена, а также предоставить копию договора передачи (уступки) прав на администрирование домена, подписанную текущим владельцем и лицом, которое принимает домен.</p>
<p>Если возможности приехать в офис нет, то заявление на смену владельца пишется удаленно и заверяется у нотариуса. Оригинал заверенного заявления и копию договора уступки необходимо прислать по почте.</p>
<p><b>Для юридических лиц и индивидуальных предпринимателей, работающих с печатью:</b></p>
<p>Прислать по бумажной почте (220089, г. Минск, пр-т Дзержинского, 57, 8 этаж, комната 38-2) либо привезти в наш офис заявление на смену владельца домена на фирменном бланке с подписью директора/индивидуального предпринимателя и печатью, а также предоставить копию договора передачи (уступки) прав на администрирование домена, подписанную текущим владельцем и лицом, которое принимает домен.</p>
<div class="notebox">
Срок действия заявления составляет 30 дней со дня подачи. На момент переоформления домен должен быть оплаченным и находится в статусе «активный».    
</div>
<h3>Новому владельцу:</h3>
<p>Зарегистрироваться на сайте по <a href="#">ссылке</a> (если новый владелец ранее не заказывал услуги в нашей компании) и сообщить логин от личного кабинета текущему владельцу домена для дальнейшего указания в заявлении.</p>
<div class="notebox">
При смене владельца сайта необходимо переоформлять 2 (две) услуги - домен и хостинг, которые требуют отдельного заявления для каждой.
    
</div>'
                ],
                [
                    'title' => 'Как сменить владельца хостинга?
',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Как войти в личный кабинет или восстановить доступ к нему?',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Как сменить владельца облачного хостинга или VPS?',
                    'answer' => "ответ <br> ответ"
                ],

            ]
        ],
        [
            'category' => 'Домены',
            'questions' => [
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
            ]
        ],
        [
            'category' => 'Оплата услуг',
            'questions' => [
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
            ]
        ],
        [
            'category' => 'Регистрация в БелГИЭ',
            'questions' => [
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
            ]
        ],
        [
            'category' => 'Рекуррентные (автоматические) платежи',
            'questions' => [
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
            ]
        ],
        [
            'category' => 'Безопасность, общие технические вопросы',
            'questions' => [
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
                [
                    'title' => 'Подзапрос',
                    'answer' => "ответ <br> ответ"
                ],
            ]
        ],
    ]
];
?>

<div class="vue-component" data-component="FaqList" data-initial='<?= json_encode($faqData); ?>'></div>
<!-- /.vue-component -->


<?
include 'include/footer.php';
?>

<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</div>
<!-- /#app -->
</body>
</html>
