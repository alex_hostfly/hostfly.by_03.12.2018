<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Конструктор сайтов</title>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app">
    <?
    include 'include/header.php';
    ?>
    <?
    $bannerData = [
        'bannerClass' => 'banner-top banner-site-constructor',
        'img' => '/local/assets/images/bg/bg_site_constructor.png',
        'title' => 'Конструктор сайтов ',
        'description' => 'Конструктор сайтов — это самый простой способ создать сайт бесплатно, не прибегая к помощи программистов, дизайнеров и верстальщиков. Конструктор сайтов не включает скрытых платежей и нежелательной рекламы на ваших ресурсах и разворачивается прямо из панели управления хостингом. ',
        'button' => [
            'text' => 'Попробовать бесплатно',
            'href' => '#',
            'btnType' => 'default'
        ]
    ];
    ?>

    <div class="vue-component" data-component="ButtonBanner" data-initial='<?= json_encode($bannerData); ?>'></div>
    <!-- /.vue-component -->

    <?
    $benefitsData = [
        'title' => "Современный сайт — инструмент для бизнеса",
        'benefits' => [
            [
                'title' => 'Простое использование',
                'description' => 'вам н нужно быть профессионалом, чтобы создать свой сайт. Все интуитивно понятно.',
                'imageUrl' => '/local/assets/images/icons/site_constructor/icon-1.png',
            ],
            [
                'title' => 'Готовый дизайн',
                'description' => 'Более 1000 шаблонов для любого вида бизнеса. Просто берете и панолняете своим контентом.',
                'imageUrl' => '/local/assets/images/icons/site_constructor/icon-2.png',
            ],
            [
                'title' => 'Адаптивность сайта',
                'description' => 'Готовые версии сайта для мобильных устройств и всевозможных гаджетов.',
                'imageUrl' => '/local/assets/images/icons/site_constructor/icon-3.png',
            ],
            [
                'title' => 'Поисковая оптимизация',
                'description' => 'Сайты уже готовы к раскрутке в поисковых системах и выводятся на первых позициях.',
                'imageUrl' => '/local/assets/images/icons/site_constructor/icon-4.png',
            ]
        ],
        'fitsClass' => "site-constructor"
    ];
    ?>

    <div class="vue-component" data-component="Benefits" data-initial='<?= json_encode($benefitsData); ?>'></div>
    <!-- /.vue-component -->

    <?
    $HundredTemplatesData = [
        'title' => "Более 1000 шаблонов",
        'imageUrl' => '/local/assets/images/bg/bg_100_tamplates.png',
    ];
    ?>

    <div class="vue-component" data-component="HundredTemplates" data-initial='<?= json_encode($HundredTemplatesData); ?>'></div>


    <?
    $ratesServiceData = [
        'questions' => [
            [
                'active' => true,
                'title' => 'Сервисные возможности',
                'text' => '<ul class="list">
<li>удобный интерфейс на русском языке;</li>
<li>простое управление элементами сайта по принципу «drag-and-drop»;</li>
<li>более 170 отличных шаблонов с возможностью добавлять галереи, карты и десяток других модулей;</li>
<li>готовое решение для создания сайтов, не требующее знания языков программирования и верстки;</li>
<li>возможность создавать мобильные версии сайтов, адаптированные под меньший экран;</li>
<li>подробное руководство «Как пользоваться конструктором сайтов?»</li>
</ul>',
            ],
            [
                    'active' => true,
                'title' => 'Руководство пользователя',
                'subQuestions' => [
                    [
                        'title' => 'Как пользоваться конструктором сайтов?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Не могу найти иконку конструктора сайтов в панели управления хостингом, что делать?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как изменить шрифт, используемый в шаблоне?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как создать новую страницу сайта?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как увеличить размер страницы?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как добавить возможность скачивания файла по нажатию кнопки?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как сделать лендинг из любого шаблона?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как создать географическую метку с описанием?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как создать иноязычную версию сайта?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как добавить счетчик Google Analytics?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как добавить счетчик Яндекс-метрики?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как добавить сервис онлайн-консультаций?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как сохранить резервную копию сайта себе на компьютер?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как восстановить сайт из резервной копии в конструкторе?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как создать иерархию в меню?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как создать якорь?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Как добавить на сайт каталог товаров?',
                        'text' => 'test ',
                    ], [
                        'title' => 'Использование Google Maps в конструкторе',
                        'text' => 'test ',
                    ],
                ],
            ]
        ],
    ];
    ?>

    <div class="vue-component" data-component="RatesUserGuide" data-initial='<?= json_encode($ratesServiceData); ?>'></div>
    <!-- /.vue-component -->

    <?
    $specialOfferData = [
        'title' => 'Если вы не нашли ответ на свой вопрос в базе знаний, мы с радостью ответим на него по электронной почте. Просто напишите на <a href="#">info@hostfly.by.</a>',
    ];
    ?>

    <div class="vue-component" data-component="SpecialOfferConstructor" data-initial='<?= json_encode($specialOfferData); ?>'></div>
    <!-- /.vue-component -->

    <?
    include 'include/footer.php';
    ?>

    <?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</div>
<!-- /#app -->
</body>
</html>
