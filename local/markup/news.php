<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Новости</title>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app">
<?
include 'include/header.php';
?>

<?
$bannerData = [
    'bannerClass' => 'banner-top',
    'img' => '/local/assets/images/bg/bg_main.png',
    'title' => 'Новости и акции HOSTFLY',
    'description' => 'Будьте в курсе всех событий и выгодных предложений. Никакого спама, только самое свежее и интересное.',
    'subscribe' => [
        'text' => 'Подписаться на рассылку',
        'href' => '#',
    ]
];
?>

<div class="vue-component" data-component="SubscribeBanner" data-initial='<?= json_encode($bannerData); ?>'></div>
<!-- /.vue-component -->


<?
$newsData = [
    'tabs' => [
        'all' => [
            'name' => 'Все',
            'class' => '',
        ],
        'news' => [
            'name' => 'Новости',
            'class' => '',
        ],
        'shares' => [
            'name' => 'Акции',
            'class' => '',
        ],
    ],
    'elements' => [
        [
            'category' => 'news',
            'title' => 'Впервые HostFly организует Минскую неделю интернета',
            'newsDate' => '20 мая 2018',
            'description' => 'Чем отличается хороший системный инженер от рядового админа? Умением комплексно мыслить и строить красивые решения. Мы в hoster.by думаем, что это во многом похоже на дизайн в самом высоком понимании этого слова. ',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'Бывают ли решения “для всех” и как облака меняют бизнес в Беларуси',
            'newsDate' => '20 мая 2018',
            'description' => 'Облачный хостинг-провайдер HostFly запустил первый в стране каталог готовых решений для бизнеса “из облака”. Сделать заказ и получить работающий сервис здесь почти так же просто, как установить приложение на смартфон.',
            'imageUrl' => '/local/assets/images/markup_image/news1.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'Бывают ли решения “для всех” и как облака меняют бизнес в Беларуси',
            'newsDate' => '20 мая 2018',
            'description' => 'Бывают ли решения “для всех” и как облака меняют бизнес в Беларуси',
            'imageUrl' => '/local/assets/images/markup_image/news2.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'Названы добрые сайты, которые делают байнет лучше: проголосуйте и предложите свой сервис',
            'newsDate' => '20 мая 2018',
            'description' => 'Положительные эмоции и действия вместо слов — это то, в чем как в воздухе нуждается байнет. Кто сказал? Здравый смысл и технический администратор домена .BY — HostFly.',
            'imageUrl' => '/local/assets/images/markup_image/news3.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'Заглушка для новости без фотографии',
            'newsDate' => '20 мая 2018',
            'description' => 'Положительные эмоции и действия вместо слов — это то, в чем как в воздухе нуждается байнет. Кто сказал? Здравый смысл и технический администратор домена .BY — HostFly.',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'заголовок',
            'newsDate' => '20 мая 2018',
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news2.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'Бесплатный семинар «Формула сайта: Ключевые шаги к эффективному интернет-магазину»',
            'newsDate' => '20 мая 2018',
            'description' => '14 апреля в Минске в партнерстве с HostFly состоится бесплатный семинар компаний 1С-Битрикс и Studio8, на котором профессионалы из сферы интернет-маркетинга и e-commerce поделятся своим опытом и успешными кейсами из своей работы.',
            'imageUrl' => '/local/assets/images/markup_image/news1.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'Belarus IGF: примите участие в исследовании спроса на открытые данные',
            'newsDate' => '20 мая 2018',
            'description' => 'Belarus IGF прошел чуть более месяца назад, однако работа по развитию белорусского интернета на этом не останавливается. Один из первых шагов по результатам Форума - исследование спроса на открытые данные.',
            'imageUrl' => '/local/assets/images/markup_image/news3.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'IT-пятница в HostFly: компании переосмысливают общение с клиентами в сторону интерактива',
            'newsDate' => '20 мая 2018',
            'description' => 'Вечером в пятницу, 11 августа, состоялась первая IT-пятница от HostFly Компания, которая отвечает за работоспособность доменных зон .BY и .БЕЛ, а также предоставляет облачные сервисы для бизнеса, решила в корне изменить традиционный подход к общению.',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'заголовок',
            'newsDate' => '20 мая 2018',
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'news',
            'title' => 'заголовок',
            'newsDate' => '20 мая 2018',
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#',
            'finishedShare' => true
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
        [
            'category' => 'shares',
            'title' => 'заголовок',
            'newsDate' => [
                'from' => '10 мая',
                'to' => '21 июля 2018'
            ],
            'description' => 'описание',
            'imageUrl' => '/local/assets/images/markup_image/news4.png',
            'detailUrl' => '#'
        ],
    ]
];
?>

<div class="vue-component" data-component="News" data-initial='<?= json_encode($newsData); ?>'></div>
<!-- /.vue-component -->

<?
include 'include/footer.php';
?>

<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</div>
<!-- /#app -->
</body>
</html>
