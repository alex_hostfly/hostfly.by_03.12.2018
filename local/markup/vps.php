<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Виртуальный хостинг</title>

    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.header.html'); ?>
</head>
<body>
<div id="app" class="virtual-server-style">
    <?
    include 'include/header.php';
    ?>

    <?
    $bannerData = [
        'bannerClass' => 'banner-top',
        'img' => '/local/assets/images/bg/bg_vps.png',
        'title' => 'VPS-хостинг — управляемый, масштабируемый',
        'description' => 'Все наши облачные серверы VPS предоставляют вам гибкость для масштабирования ваших ресурсов. В отличие от других облачных хостинговых компаний, все наши планы включают стоимость управляемых услуг.',
    ];
    ?>

    <div class="vue-component" data-component="ButtonBanner" data-initial='<?= json_encode($bannerData); ?>'></div>
    <!-- /.vue-component -->

    <?
    $featuresListData = [

        'rates' => [
            [
                'hit' => true,
                'color' => 'leaf',
                'name' => 'Cloud VPS-1',
                'limitedOffer' => true,
                'price' => [
                    'value' => '11.50',
                    'currency' => 'рублей',
                    'period' => 'месяц',
                ],
                'features' => [
                    [
                        'name' => 'Оперативная память',
                        'value' => '8 Гб DDR3',
                    ],
                    [
                        'name' => 'Виртуальный ЦП',
                        'value' => '2',
                        'help' => '2 х 2000+ MHz',
                    ],
                    [
                        'name' => 'SSD хранилище',
                        'value' => '6 Гб',
                    ],
                    [
                        'name' => 'Пропускная способность',
                        'value' => '5 Тб',
                    ],

                ],
                'buttonToOrder' => [
                    'text' => 'Заказать',
                    'href' => '#'
                ],
            ],
            [
                'hit' => true,
                'color' => 'pinky',
                'name' => 'Cloud VPS-1',
                'limitedOffer' => true,
                'price' => [
                    'value' => '11.50',
                    'currency' => 'рублей',
                    'period' => 'месяц',
                ],
                'features' => [
                    [
                        'name' => 'Оперативная память',
                        'value' => '8 Гб DDR3',
                    ],
                    [
                        'name' => 'Виртуальный ЦП',
                        'value' => '2',
                        'help' => '2 х 2000+ MHz',
                    ],
                    [
                        'name' => 'SSD хранилище',
                        'value' => '6 Гб',
                    ],
                    [
                        'name' => 'Пропускная способность',
                        'value' => '5 Тб',
                    ],

                ],
                'buttonToOrder' => [
                    'text' => 'Заказать',
                    'href' => '#'
                ],
            ],
            [
                'hit' => true,
                'color' => 'sea',
                'name' => 'Cloud VPS-1',
                'limitedOffer' => true,
                'price' => [
                    'value' => '11.50',
                    'currency' => 'рублей',
                    'period' => 'месяц',
                ],
                'features' => [
                    [
                        'name' => 'Оперативная память',
                        'value' => '8 Гб DDR3',
                    ],
                    [
                        'name' => 'Виртуальный ЦП',
                        'value' => '2',
                        'help' => '2 х 2000+ MHz',
                    ],
                    [
                        'name' => 'SSD хранилище',
                        'value' => '6 Гб',
                    ],
                    [
                        'name' => 'Пропускная способность',
                        'value' => '5 Тб',
                    ],

                ],
                'buttonToOrder' => [
                    'text' => 'Заказать',
                    'href' => '#'
                ],
            ],
            [
                'name' => 'Cloud VPS-1',
                'limitedOffer' => true,
                'price' => [
                    'value' => '11.50',
                    'currency' => 'рублей',
                    'period' => 'месяц',
                ],
                'features' => [
                    [
                        'name' => 'Оперативная память',
                        'value' => '8 Гб DDR3',
                    ],
                    [
                        'name' => 'Виртуальный ЦП',
                        'value' => '2',
                        'help' => '2 х 2000+ MHz',
                    ],
                    [
                        'name' => 'SSD хранилище',
                        'value' => '6 Гб',
                    ],
                    [
                        'name' => 'Пропускная способность',
                        'value' => '5 Тб',
                    ],

                ],
                'buttonToOrder' => [
                    'text' => 'Заказать',
                    'href' => '#'
                ],
            ],
            [
                'name' => 'Cloud VPS-1',
                'limitedOffer' => true,
                'price' => [
                    'value' => '11.50',
                    'currency' => 'рублей',
                    'period' => 'месяц',
                ],
                'features' => [
                    [
                        'name' => 'Оперативная память',
                        'value' => '8 Гб DDR3',
                    ],
                    [
                        'name' => 'Виртуальный ЦП',
                        'value' => '2',
                        'help' => '2 х 2000+ MHz',
                    ],
                    [
                        'name' => 'SSD хранилище',
                        'value' => '6 Гб',
                    ],
                    [
                        'name' => 'Пропускная способность',
                        'value' => '5 Тб',
                    ],

                ],
                'buttonToOrder' => [
                    'text' => 'Заказать',
                    'href' => '#'
                ],
            ],
        ],

    ];
    ?>

    <div class="vue-component" data-component="FeaturesList"
         data-initial='<?= json_encode($featuresListData); ?>'></div>
    <!-- /.vue-component -->


    <?
    $specialOfferData = [
        'title' => 'Летние цены на виртуальный сервер. 14 дней бесплатный пробный период',
        'button' => [
            'text' => 'Подробнее',
            'href' => '#'
        ]
    ];
    ?>

    <div class="vue-component" data-component="SpecialOffer"
         data-initial='<?= json_encode($specialOfferData); ?>'></div>
    <!-- /.vue-component -->

    <?
    $ratesFaqData = [
        'questions' => [
            [
                'title' => 'Что такое VPS / VDS?',
                'text' => 'VPS (Virtual Private Server) или VDS (Virtual Dedicated Server) — это программная эмуляция физически выделенного сервера. Каждый VPS имеет собственную операционную и файловую систему, выделенный IP-адрес и уникальный набор программного обеспечения, обслуживающий только того клиента, который использует этот виртуальный сервер. Внутри одного выделенного сервера может находиться несколько VPS.',
            ],
            [
                'title' => 'Какие ресурсы VPS хостинга?',
                'text' => 'Каждый виртуальный сервер получает гарантированный объем ресурсов физического сервера — процессорного времени и памяти. Это дает сайтам на вашем виртуальном сервере полную независимость от работы сайтов других клиентов.',
            ],
            [
                'title' => 'Полное заполнение раздела',
                'text' => 'тект 1',
            ],
            [
                'title' => 'Как создать поддомен?',
                'text' => 'тект 1',
            ],
        ]

    ];
    ?>

    <div class="vue-component" data-component="RatesFaq" data-initial='<?= json_encode($ratesFaqData); ?>'></div>
    <!-- /.vue-component -->

    <?
    $infoBlockData = [
        'title' => 'Вам подходит VPS сервер, если',
        'list' => [
            'вы представляете посещаемый интернет-проект, крупную компанию или веб-студию',
            'вы хотите разместить проект повышенной ресурсоемкости',
            'для вас критична повышенная безопасность и сохранность данных',
            'для вас важна постоянная доступность сайта и наличие свободных серверных ресурсов.',
        ],
        'imageSrc' => '/local/assets/images/bg/man-and-tetris.png',
        'button' => [
            'text' => 'Список тарифов',
            'href' => '#',
        ]
    ];
    ?>

    <div class="vue-component" data-component="InfoBlock" data-initial='<?= json_encode($infoBlockData); ?>'></div>
    <!-- /.vue-component -->

    <?
    $typesRatesData = [
        'types' => [
            [
                'title' => 'Веб-хостинг',
                'text' => 'Размещайте ваш сайт на надежном сервере с поддержкой всех актуальных технологий.',
                'imageUrl' => '/local/assets/images/bg/leaf-card.png',
                'button' => [
                    'text' => 'Подробнее',
                    'href' => '#',
                ]
            ],
            [
                'title' => 'Выделенный сервер',
                'text' => 'Физически отдельный сервер, полностью находящийся в вашем распоряжении.',
                'imageUrl' => '/local/assets/images/bg/sea-card.png',
                'button' => [
                    'text' => 'Подробнее',
                    'href' => '#',
                ]
            ]
        ]
    ];
    ?>

    <div class="vue-component" data-component="TypesRates" data-initial='<?= json_encode($typesRatesData); ?>'></div>
    <!-- /.vue-component -->

    <?
    include 'include/footer.php';
    ?>

    <?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/assets/build/assets.footer.html'); ?>
</div>
<!-- /#app -->
</body>
</html>
