
        <?$APPLICATION->IncludeComponent(
	"content:Footer", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COPYRIGHT" => "© 2018 ООО «Хостфлай». Провайдер хостинга и облачных решений в Беларуси.<br>",
		"CARDS" => array(
			0 => "/local/assets/images/icons/MTBANK_logo.png",
			1 => "",
			2 => "",
		),
		"SOCIAL_NETWORKS_COUNT" => "5",
		"PRETTY_LINKS_COUNT" => "2",
		"SOCIAL_NETWORKS_1_CLASS" => "svg-icon-vk",
		"SOCIAL_NETWORKS_1_LINK" => "https://vk.com",
		"SOCIAL_NETWORKS_2_CLASS" => "svg-icon-fb",
		"SOCIAL_NETWORKS_2_LINK" => "https://facebook.com",
		"SOCIAL_NETWORKS_3_CLASS" => "svg-icon-insta",
		"SOCIAL_NETWORKS_3_LINK" => "https://instagram.com",
		"PRETTY_LINKS_1_CLASS" => "sea",
		"PRETTY_LINKS_1_TITLE" => "Почему нам доверяют <br><br>",
		"PRETTY_LINKS_1_BUTTON_TEXT" => "Подробнее о нас",
		"PRETTY_LINKS_1_BUTTON_LINK" => "/about/who-are-we/",
		"PRETTY_LINKS_2_CLASS" => "leaf",
		"PRETTY_LINKS_2_TITLE" => "Круглосуточно <br><a class=\"footer-tel\" href=\"tel:+375173435532\">+375 17 343-55-32</a>",
		"PRETTY_LINKS_2_BUTTON_TEXT" => "Online-чат",
		"PRETTY_LINKS_2_BUTTON_LINK" => "https://sosedi.by",
		"SOCIAL_NETWORKS_4_CLASS" => "svg-icon-ok",
		"SOCIAL_NETWORKS_4_LINK" => "https://odnoklassniki.ru",
		"SOCIAL_NETWORKS_5_CLASS" => "svg-icon-tw",
		"SOCIAL_NETWORKS_5_LINK" => "https://twitter.com",
		"PRETTY_" => ""
	),
	false
);
        ?>

    </div>
    <!-- /#app -->
    
    <?php $APPLICATION->IncludeFile('/local/assets/build/assets.footer.html'); ?>
</body>

</html>