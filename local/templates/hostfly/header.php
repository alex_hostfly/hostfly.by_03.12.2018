<?
global $APPLICATION;
CJSCore::Init();
$URL = getProtocol() . getServerName() . $APPLICATION->GetCurPage();
?>

<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
	<title><? $APPLICATION->ShowTitle(); ?></title>

    <meta property="og:type" content="website"/>
    <meta property="og:site_name" content="HostFly"/>
    <meta property="og:url" content="<?=$URL?>"/>

    <meta property="og:title" content="<?=$APPLICATION->ShowProperty('og:title')?>"/>
    <meta property="og:image" content="<?=$APPLICATION->ShowProperty('og:image')?>"/>
    <meta property="og:description" content="<?=$APPLICATION->ShowProperty('og:description')?>"/>

    <link rel="icon" sizes="16x16 24x24 32x32 64x64" type="image/x-icon" href="/local/assets/images/favicon.ico?v=2">

	<meta charset="<?= LANG_CHARSET ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?
    $APPLICATION->IncludeFile('/local/assets/build/assets.header.html');
    $APPLICATION->ShowHead();
    $APPLICATION->IncludeFile('/local/include/analytics.php');
    ?>
</head>

<body>

	<div id="panel">
		<? $APPLICATION->ShowPanel(); ?>
	</div>
	<!-- /#panel -->
 
	<div id="app" class="<? $APPLICATION->ShowProperty('APP_CLASS')?>">
  
		<?$APPLICATION->IncludeComponent(
	"content:Header", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PHONE" => "+375 17 343-55-32",
		"CONTACT_COUNT" => "2",
		"CONTACT_1_ADDITIONAL_CONTACTS" => array(
		),
		"CONTACT_2_ADDITIONAL_CONTACTS" => array(
			0 => "+375 17 343-55-32|Velcom|Пн-Пт: с 9:00 до 20:00",
			1 => "+375 29 123-12-12|MTS|Сб-Вс: с 9:00 до 21:00",
			2 => "+375 29 123-12-12|Life",
			3 => "+375 29 123-12-12|Факс",
			4 => "",
		),
		"CONTACT_1_TITLE" => "Круглосуточная техподдержка",
		"CONTACT_1_PHONE" => "+375 17 343-55-32",
		"CONTACT_1_EMAIL" => "support@hostfly.by",
		"CONTACT_2_TITLE" => "Отдел продаж",
		"CONTACT_2_PHONE" => "+375 17 343-55-32",
		"CONTACT_2_EMAIL" => "sales@hostfly.by"
	),
	false
);
		?>