<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="{$charset}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{if $kbarticle.title}{$kbarticle.title} - {/if}{$pagetitle} - {$companyname}</title>

    {include file="$template/includes/head.tpl"}

    {$headoutput}

</head>
<body data-phone-cc-input="{$phoneNumberInputStyle}" id="app">

{$headeroutput}

<div data-v-6acd124c="" class="container header row page-content">
    <div data-v-6acd124c="" class="layout">
        <div data-v-6acd124c="" class="flex header__logo just-num-1 xs6"><a data-v-6acd124c=""
                                                                            href="http://hostfly.stage.newsite.by/">
                <svg data-v-6acd124c="" class="logo">
                    <use data-v-6acd124c="" xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="/templates/hostfly/img/icons/_sprite.svg#logo"></use>
                </svg>
            </a></div>
        <div data-v-6acd124c="" class="flex header__info just-num-2 xs12 lg6">
            <div data-v-6acd124c="" class="header__info__support row just-num-46">
                <div data-v-6acd124c="" class="header__info__support__text heading color--lemon"><span
                            data-v-6acd124c="" class="hidden-xs-down">24/7 Поддержка</span></div>
                <div data-v-6acd124c="" class="note-popup phones-popup">
                    <div data-v-6acd124c="" class="contact wrap">
                        <div data-v-6acd124c="">
                            <div data-v-6acd124c="" class="h3">Круглосуточная техподдержка</div>
                            <div data-v-6acd124c="" class="h2">+375 17 123-45-67</div>
                            <div data-v-6acd124c="" class="just-num-79 just-wrap ">
                                <div data-v-6acd124c="" class="adittional-contact-list"></div>
                                <div data-v-6acd124c="" class="adittional-contact-list"></div>
                            </div>
                            <p data-v-6acd124c=""><a data-v-6acd124c="" href="mailto:support@hostfly.by">support@hostfly.by</a>
                            </p></div>
                    </div>
                    <div data-v-6acd124c="" class="contact wrap">
                        <div data-v-6acd124c="">
                            <div data-v-6acd124c="" class="h3">Отдел продаж</div>
                            <div data-v-6acd124c="" class="h2">+375 17 567-12-34</div>
                            <div data-v-6acd124c="" class="just-num-79 just-wrap ">
                                <div data-v-6acd124c="" class="adittional-contact-list">
                                    <div data-v-6acd124c="" class="adittional-contact-list__item just-num-46">
                                        <div data-v-6acd124c="" class="num">
                                            +375 29 123-12-12
                                        </div>
                                        <div data-v-6acd124c="" class="operator"><span
                                                    data-v-6acd124c="">Velcom</span></div>
                                    </div>
                                    <div data-v-6acd124c="" class="adittional-contact-list__item just-num-46">
                                        <div data-v-6acd124c="" class="num">
                                            +375 29 123-12-12
                                        </div>
                                        <div data-v-6acd124c="" class="operator"><span data-v-6acd124c="">MTS</span>
                                        </div>
                                    </div>
                                    <div data-v-6acd124c="" class="adittional-contact-list__item just-num-46">
                                        <div data-v-6acd124c="" class="num">
                                            +375 29 123-12-12
                                        </div>
                                        <div data-v-6acd124c="" class="operator"><span
                                                    data-v-6acd124c="">Life</span></div>
                                    </div>
                                    <div data-v-6acd124c="" class="adittional-contact-list__item just-num-46">
                                        <div data-v-6acd124c="" class="num">
                                            +375 29 123-12-12
                                        </div>
                                        <div data-v-6acd124c="" class="operator"><span
                                                    data-v-6acd124c="">Факс</span></div>
                                    </div>
                                </div>
                                <div data-v-6acd124c="" class="adittional-contact-list"></div>
                            </div>
                            <p data-v-6acd124c=""><a data-v-6acd124c=""
                                                     href="mailto:sales@hostfly.by">sales@hostfly.by</a>
                            </p></div>
                    </div>
                </div>
                <div data-v-6acd124c="" class="header__info__support__phone"><span data-v-6acd124c=""><span
                                data-v-6acd124c="" class="phonecode">+375 17</span> <span data-v-6acd124c=""
                                                                                          class="phonenum">123-45-67</span></span>
                </div>
                <div data-v-6acd124c="" class="header__info__support__auth just-num-6 just-nowrap"><a
                            data-v-6acd124c="" href="{if $smarty.session.upw}/logout.php{else}/clientarea.php{/if}"
                            class="btn--lemon v-btn v-btn--small" float="">
                        <div class="v-btn__content">
                            {if $smarty.session.upw}
                                Выйти
                            {else}
                                Войти
                            {/if}
                        </div>
                    </a>
                    <div data-v-6acd124c="" class="menu-button just-num-5">
                        <div data-v-6acd124c="" class="lines">
                            <div data-v-6acd124c="" class="line"></div>
                            <div data-v-6acd124c="" class="line"></div>
                            <div data-v-6acd124c="" class="line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-v-6acd124c="" class="header__info__menu top-menu">
                <ul data-v-6acd124c="" class="top-menu__list just-num-46">
                    <li data-v-6acd124c="" class="mobile-only top-menu__item heading"><a data-v-6acd124c=""
                                                                                         href="http://my.hostfly.by/clientarea.php">Личный
                            кабинет</a></li>
                    <li data-v-6acd124c="" class="top-menu__item heading parent js-toggle-self"><a
                                data-v-6acd124c="" href="http://hostfly.stage.newsite.by/tariffs/">Тарифы</a>
                        <div data-v-6acd124c="" class="submenu">
                            <div data-v-6acd124c=""><a data-v-6acd124c=""
                                                       href="http://hostfly.stage.newsite.by/tariffs/hosting/"
                                                       class="link">Веб-хостинг</a></div>
                            <div data-v-6acd124c=""><a data-v-6acd124c=""
                                                       href="http://hostfly.stage.newsite.by/tariffs/virtual-server/"
                                                       class="link">Виртуальные сервера</a></div>
                            <div data-v-6acd124c=""><a data-v-6acd124c=""
                                                       href="http://hostfly.stage.newsite.by/tariffs/dedicated-server/"
                                                       class="link">Выделенный сервер</a></div>
                        </div>
                    </li>
                    <li data-v-6acd124c="" class="top-menu__item heading"><a data-v-6acd124c=""
                                                                             href="http://hostfly.stage.newsite.by/domains/">Домены</a>
                        <!----></li>
                    <li data-v-6acd124c="" class="top-menu__item heading"><a data-v-6acd124c=""
                                                                             href="http://hostfly.stage.newsite.by/site-constructor/">Конструктор
                            сайтов</a> <!----></li>
                    <li data-v-6acd124c="" class="top-menu__item heading parent js-toggle-self"><a
                                data-v-6acd124c="" href="http://hostfly.stage.newsite.by/about/">О компании</a>
                        <div data-v-6acd124c="" class="submenu">
                            <div data-v-6acd124c=""><a data-v-6acd124c=""
                                                       href="http://hostfly.stage.newsite.by/about/who-are-we/"
                                                       class="link">Кто мы</a></div>
                            <div data-v-6acd124c=""><a data-v-6acd124c=""
                                                       href="http://hostfly.stage.newsite.by/about/news/"
                                                       class="link">Новости и акции</a></div>
                            <div data-v-6acd124c=""><a data-v-6acd124c=""
                                                       href="http://hostfly.stage.newsite.by/about/faq/"
                                                       class="link">Часто задаваемые вопросы</a></div>
                            <div data-v-6acd124c=""><a data-v-6acd124c=""
                                                       href="http://hostfly.stage.newsite.by/about/contacts/"
                                                       class="link">Контакты</a></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div data-v-6acd124c="" class="clearglass"></div>
</div>

{*<section id="header">
    <div class="container">
        <ul class="top-nav">
            {if $languagechangeenabled && count($locales) > 1}
                <li>
                    <a href="#" class="choose-language" data-toggle="popover" id="languageChooser">
                        {$activeLocale.localisedName}
                        <b class="caret"></b>
                    </a>
                    <div id="languageChooserContent" class="hidden">
                        <ul>
                            {foreach $locales as $locale}
                                <li>
                                    <a href="{$currentpagelinkback}language={$locale.language}">{$locale.localisedName}</a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                </li>
            {/if}
            {if $loggedin}
                <li>
                    <a href="#" data-toggle="popover" id="accountNotifications" data-placement="bottom">
                        {$LANG.notifications}
                        {if count($clientAlerts) > 0}
                            <span class="label label-info">{lang key='notificationsnew'}</span>
                        {/if}
                        <b class="caret"></b>
                    </a>
                    <div id="accountNotificationsContent" class="hidden">
                        <ul class="client-alerts">
                        {foreach $clientAlerts as $alert}
                            <li>
                                <a href="{$alert->getLink()}">
                                    <i class="fas fa-fw fa-{if $alert->getSeverity() == 'danger'}exclamation-circle{elseif $alert->getSeverity() == 'warning'}exclamation-triangle{elseif $alert->getSeverity() == 'info'}info-circle{else}check-circle{/if}"></i>
                                    <div class="message">{$alert->getMessage()}</div>
                                </a>
                            </li>
                        {foreachelse}
                            <li class="none">
                                {$LANG.notificationsnone}
                            </li>
                        {/foreach}
                        </ul>
                    </div>
                </li>
                <li class="primary-action">
                    <a href="{$WEB_ROOT}/logout.php" class="btn">
                        {$LANG.clientareanavlogout}
                    </a>
                </li>
            {else}
                <li>
                    <a href="{$WEB_ROOT}/clientarea.php">{$LANG.login}</a>
                </li>
                {if $condlinks.allowClientRegistration}
                    <li>
                        <a href="{$WEB_ROOT}/register.php">{$LANG.register}</a>
                    </li>
                {/if}
                <li class="primary-action">
                    <a href="{$WEB_ROOT}/cart.php?a=view" class="btn">
                        {$LANG.viewcart}
                    </a>
                </li>
            {/if}
            {if $adminMasqueradingAsClient || $adminLoggedIn}
                <li>
                    <a href="{$WEB_ROOT}/logout.php?returntoadmin=1" class="btn btn-logged-in-admin" data-toggle="tooltip" data-placement="bottom" title="{if $adminMasqueradingAsClient}{$LANG.adminmasqueradingasclient} {$LANG.logoutandreturntoadminarea}{else}{$LANG.adminloggedin} {$LANG.returntoadminarea}{/if}">
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>
            {/if}
        </ul>

        {if $assetLogoPath}
            <a href="{$WEB_ROOT}/index.php" class="logo"><img src="{$assetLogoPath}" alt="{$companyname}"></a>
        {else}
            <a href="{$WEB_ROOT}/index.php" class="logo logo-text">{$companyname}</a>
        {/if}

    </div>
</section>

<section id="main-menu">

    <nav id="nav" class="navbar navbar-default navbar-main" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="primary-nav">

                <ul class="nav navbar-nav">

                    {include file="$template/includes/navbar.tpl" navbar=$primaryNavbar}

                </ul>

                <ul class="nav navbar-nav navbar-right">

                    {include file="$template/includes/navbar.tpl" navbar=$secondaryNavbar}

                </ul>

            </div><!-- /.navbar-collapse -->
        </div>
    </nav>

</section>*}

{if $templatefile == 'homepage'}
    <script>
      window.location.href = '/clientarea.php';
    </script>
    <section id="home-banner">
        <div class="container text-center">
            {if $registerdomainenabled || $transferdomainenabled}
                <h2>{$LANG.homebegin}</h2>
                <form method="post" action="domainchecker.php">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                            <div class="input-group input-group-lg">
                                <input type="text" class="form-control" name="domain"
                                       placeholder="{$LANG.exampledomain}" autocapitalize="none"/>
                                <span class="input-group-btn">
                                    {if $registerdomainenabled}
                                        <input type="submit" class="btn search" value="{$LANG.search}"/>
                                    {/if}
                                    {if $transferdomainenabled}
                                        <input type="submit" name="transfer" class="btn transfer"
                                               value="{$LANG.domainstransfer}"/>
                                    {/if}
                                </span>
                            </div>
                        </div>
                    </div>

                    {include file="$template/includes/captcha.tpl"}
                </form>
            {else}
                <h2>{$LANG.doToday}</h2>
            {/if}
        </div>
    </section>
    <div class="home-shortcuts">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-sm hidden-xs text-center">
                    <p class="lead">
                        {$LANG.howcanwehelp}
                    </p>
                </div>
                <div class="col-sm-12 col-md-8">
                    <ul>
                        {if $registerdomainenabled || $transferdomainenabled}
                            <li>
                                <a id="btnBuyADomain" href="domainchecker.php">
                                    <i class="fas fa-globe"></i>
                                    <p>
                                        {$LANG.buyadomain} <span>&raquo;</span>
                                    </p>
                                </a>
                            </li>
                        {/if}
                        <li>
                            <a id="btnOrderHosting" href="cart.php">
                                <i class="far fa-hdd"></i>
                                <p>
                                    {$LANG.orderhosting} <span>&raquo;</span>
                                </p>
                            </a>
                        </li>
                        <li>
                            <a id="btnMakePayment" href="clientarea.php">
                                <i class="fas fa-credit-card"></i>
                                <p>
                                    {$LANG.makepayment} <span>&raquo;</span>
                                </p>
                            </a>
                        </li>
                        <li>
                            <a id="btnGetSupport" href="submitticket.php">
                                <i class="far fa-envelope"></i>
                                <p>
                                    {$LANG.getsupport} <span>&raquo;</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
{/if}

{include file="$template/includes/verifyemail.tpl"}

<section id="main-body">
    <div class="container{if $skipMainBodyContainer}-fluid without-padding{/if}">
        <div class="row">

            {if !$inShoppingCart && ($primarySidebar->hasChildren() || $secondarySidebar->hasChildren())}
                {if $primarySidebar->hasChildren() && !$skipMainBodyContainer}
                    <div class="col-md-9 pull-md-right">
                        {include file="$template/includes/pageheader.tpl" title=$displayTitle desc=$tagline showbreadcrumb=true}
                    </div>
                {/if}
                <div class="col-md-3 pull-md-left sidebar">
                    {include file="$template/includes/sidebar.tpl" sidebar=$primarySidebar}
                </div>
            {/if}
            <!-- Container for main page display content -->
            <div class="{if !$inShoppingCart && ($primarySidebar->hasChildren() || $secondarySidebar->hasChildren())}col-md-9 pull-md-right{else}col-xs-12{/if} main-content">
                {if !$primarySidebar->hasChildren() && !$showingLoginPage && !$inShoppingCart && $templatefile != 'homepage' && !$skipMainBodyContainer}
                    {include file="$template/includes/pageheader.tpl" title=$displayTitle desc=$tagline showbreadcrumb=true}
                {/if}
