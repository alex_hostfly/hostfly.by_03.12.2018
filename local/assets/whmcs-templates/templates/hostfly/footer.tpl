</div><!-- /.main-content -->
{if !$inShoppingCart && $secondarySidebar->hasChildren()}
    <div class="col-md-3 pull-md-left sidebar sidebar-secondary">
        {include file="$template/includes/sidebar.tpl" sidebar=$secondarySidebar}
    </div>
{/if}
<div class="clearfix"></div>
</div>
</div>
</section>

<div data-v-62d44d30="" class="footer column">
    <div data-v-62d44d30="" class="pretty-links-column">
        <div data-v-62d44d30="" class="container">
            <div data-v-62d44d30="" class="layout page-content footer__prettyLinks">
                <div data-v-62d44d30="" class="flex footer__prettyLink xs12 sm6 sea">
                    <div data-v-62d44d30=""><p data-v-62d44d30="">HOSTFLY — хостинг от профессионалов</p>
                        <div data-v-62d44d30="">
                            <button data-v-62d44d30="" type="button" class="v-btn v-btn--flat btn--sea">
                                <div class="v-btn__content">
                                    Как мы работаем
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
                <div data-v-62d44d30="" class="flex footer__prettyLink xs12 sm6 leaf">
                    <div data-v-62d44d30=""><p data-v-62d44d30="">Круглосуточно <br><a class="footer-tel"
                                                                                       href="tel:+375171234567">+375
                                17 123-45-67</a></p>
                        <div data-v-62d44d30="">
                            <button data-v-62d44d30="" type="button" class="v-btn v-btn--flat btn--leaf">
                                <div class="v-btn__content">
                                    Online-чат
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-v-62d44d30="" class="footer__cards row page-content just-num-5 just-wrap">
        <div data-v-62d44d30="" class="footer__card just-num-5"><img data-v-62d44d30=""></div>
        <div data-v-62d44d30="" class="footer__card just-num-5"><img data-v-62d44d30=""></div>
        <div data-v-62d44d30="" class="footer__card just-num-5"><img data-v-62d44d30=""></div>
        <div data-v-62d44d30="" class="footer__card just-num-5"><img data-v-62d44d30=""></div>
        <div data-v-62d44d30="" class="footer__card just-num-5"><img data-v-62d44d30=""></div>
        <div data-v-62d44d30="" class="footer__card just-num-5"><img data-v-62d44d30=""></div>
    </div>
    <div data-v-62d44d30="" class="footer__bottomMenu  page-content row just-num-5 just-wrap">
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/about/">О
                компании</a></div>
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/about/news/">Новости</a>
        </div>
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/about/faq/">FAQ</a>
        </div>
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/about/achievements/">Награды
                и сертификаты</a></div>
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/domain-registration/">Регистрация
                доменов</a></div>
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/site-constructor/">Конструктор
                сайтов</a></div>
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/technical-support/">Техническая
                поддержка</a></div>
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/tariffs/">Тарифы</a>
        </div>
        <div data-v-62d44d30="" class="footer__bottomMenu__item"><a data-v-62d44d30=""
                                                                    href="http://hostfly.test2.newsite.by/about/contacts/">Контакты</a>
        </div>
    </div>
    <div data-v-62d44d30="" class="footer__socialNetworks just-num-5 row">
        <div data-v-62d44d30="" class="footer__socialNetwork col"><a data-v-62d44d30="" class="socialNetwork__link">
                <svg data-v-62d44d30="" class="social-icon">
                    <use data-v-62d44d30="" xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="/templates/hostfly/img/icons/_sprite.svg#svg-icon-vk"></use>
                </svg>
            </a></div>
        <div data-v-62d44d30="" class="footer__socialNetwork col"><a data-v-62d44d30="" class="socialNetwork__link">
                <svg data-v-62d44d30="" class="social-icon">
                    <use data-v-62d44d30="" xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="/templates/hostfly/img/icons/_sprite.svg#svg-icon-fb"></use>
                </svg>
            </a></div>
        <div data-v-62d44d30="" class="footer__socialNetwork col"><a data-v-62d44d30="" class="socialNetwork__link">
                <svg data-v-62d44d30="" class="social-icon">
                    <use data-v-62d44d30="" xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="/templates/hostfly/img/icons/_sprite.svg#svg-icon-insta"></use>
                </svg>
            </a></div>
        <div data-v-62d44d30="" class="footer__socialNetwork col"><a data-v-62d44d30="" class="socialNetwork__link">
                <svg data-v-62d44d30="" class="social-icon">
                    <use data-v-62d44d30="" xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="/templates/hostfly/img/icons/_sprite.svg#svg-icon-ok"></use>
                </svg>
            </a></div>
        <div data-v-62d44d30="" class="footer__socialNetwork col"><a data-v-62d44d30="" class="socialNetwork__link">
                <svg data-v-62d44d30="" class="social-icon">
                    <use data-v-62d44d30="" xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="/templates/hostfly/img/icons/_sprite.svg#svg-icon-tw"></use>
                </svg>
            </a></div>
    </div>
    <div data-v-62d44d30="" class="footer__copyright page-content just-num-8 row"><p class="footer__bottomText" data-v-62d44d30="">© 2018 ООО
            «Хостфлай». Провайдер хостинга и облачных решений в Беларуси.<br><a class="link--copyright"
                                                                                href="http://newsite.by/"
                                                                                target="_blank">Разработка сайта</a> —
            Новый сайт</p>
    </div>
</div>

{*<section id="footer">
    <div class="container">
        <a href="#" class="back-to-top"><i class="fas fa-chevron-up"></i></a>
        <p>Copyright &copy; {$date_year} {$companyname}. All Rights Reserved.</p>
    </div>
</section>*}

<div class="modal system-modal fade" id="modalAjax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Title</h4>
            </div>
            <div class="modal-body panel-body">
                Loading...
            </div>
            <div class="modal-footer panel-footer">
                <div class="pull-left loader">
                    <i class="fas fa-circle-notch fa-spin"></i> Loading...
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary modal-submit">
                    Submit
                </button>
            </div>
        </div>
    </div>
</div>

{$footeroutput}
<script>
  $('.header__info__support__phone').click(function (e) {
    e.preventDefault()
    e.stopPropagation()
    $(this).siblings('.note-popup.phones-popup').fadeToggle(100)
  })
  $('.note-popup.phones-popup').click(function (e) {
    e.stopPropagation()
  })
  $('body').click(function (e) {
    $('.note-popup.phones-popup').fadeOut()
  })
  $('.edit-price-kopecs-js').each(function () {
    var arraySplitSum = $(this).text().split('.')
    arraySplitSum[1] = arraySplitSum[1].substr(0, 2)
    $(this).html('<div class="style-price">\n' +
      '    <span class="number-integer">' + arraySplitSum[0] + '</span>\n' +
      '    <span class="number-kopec">' + arraySplitSum[1] + ' руб.</span>\n' +
      '</div>')
  });
  $(document).ready(function () {
    $('a[href="https://www.whmcs.com/"]').parent('p').hide();
    $('.block-left-nav .list-group a.list-group-item.active').parents('.panel.panel-sidebar').addClass('active');
    $('.block-left-nav .panel').each(function () {
      if( !$(this).find('.list-group-item.active').length ){
            /*$(this).find('.panel-heading .panel-minimise').trigger('click');*/
            $(this).find('.panel-heading .panel-minimise').addClass('minimised');
            $(this).find('.panel-body, .list-group, .panel-footer').hide(0)
      }
    });

    jQuery('.panel-heading').click(function(e) {
      e.preventDefault();
      if (jQuery(this).find('.panel-minimise').hasClass('minimised')) {
        jQuery(this).parents('.panel').addClass('active');
        jQuery(this).parents('.panel').find('.panel-body, .list-group, .panel-footer').slideDown();
        jQuery(this).find('.panel-minimise').removeClass('minimised');
      } else {
        jQuery(this).parents('.panel').removeClass('active');
        jQuery(this).parents('.panel').find('.panel-body, .list-group, .panel-footer').slideUp();
        jQuery(this).find('.panel-minimise').addClass('minimised');
      }
    });
    $("html, body").animate({ scrollTop: 0 }, 0);
  });
</script>


</body>
</html>
