<!-- Styling -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600|Raleway:400,700" rel="stylesheet">
<link href="{$WEB_ROOT}/templates/{$template}/css/all.min.css?v={$versionHash}" rel="stylesheet">
<link href="{$WEB_ROOT}/templates/{$template}/css/custom.css?v={$versionHash}" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script type="text/javascript">
    var csrfToken = '{$token}',
        markdownGuide = '{lang key="markdown.title"}',
        locale = '{if !empty($mdeLocale)}{$mdeLocale}{else}en{/if}',
        saved = '{lang key="markdown.saved"}',
        saving = '{lang key="markdown.saving"}',
        whmcsBaseUrl = "{\WHMCS\Utility\Environment\WebHelper::getBaseUrl()}",
        recaptchaSiteKey = "{$recaptchaSiteKey}";
</script>
<script src="{$WEB_ROOT}/templates/{$template}/js/scripts.min.js?v={$versionHash}"></script>
{*<script src="/templates/hostfly/js/bundle-app.47feb162a1f72654e0b3.js" class="js-webpack-asset"></script>*}
<script src="/templates/hostfly/js/bundle-polyfills.47feb162a1f72654e0b3.js" class="js-webpack-asset"></script>

<script src="/templates/hostfly/js/bundle-styles.7250101abd564d295d53.js" class="js-webpack-asset"></script>
<style type="text/css">
    #app.dedicated-server-style .header .header__logo .logo[data-v-6acd124c] {
        color: #01adc3
    }

    #app.hosting-style .header .header__logo .logo[data-v-6acd124c] {
        color: #8dc43b
    }

    #app .header[data-v-6acd124c] {
        height: 130px;
        z-index: 50;
        padding: 0
    }

    #app .header .header__logo[data-v-6acd124c] {
        padding-bottom: 0
    }

    #app .header .header__logo .logo[data-v-6acd124c] {
        width: 222px;
        height: 65px;
        max-width: 100%;
        color: #eb276d
    }

    #app .header .header__logo a[data-v-6acd124c]:hover {
        border-color: transparent
    }

    #app .header .header__info[data-v-6acd124c] {
        padding-bottom: 0
    }

    #app .header .header__info__support[data-v-6acd124c] {
        position: absolute;
        top: 0;
        right: 0;
        width: 420px;
        background: rgba(0, 0, 0, .3);
        padding: 10px 18px;
        -webkit-border-radius: 0 0 8px 8px;
        -moz-border-radius: 0 0 8px 8px;
        border-radius: 0 0 8px 8px
    }

    #app .header .header__info__support[data-v-6acd124c]:after {
        display: none
    }

    #app .header .header__info__support__phone[data-v-6acd124c] {
        cursor: pointer;
        -webkit-transition: .5s;
        -o-transition: .5s;
        -moz-transition: .5s;
        transition: .5s;
        position: relative
    }

    #app .header .header__info__support__phone[data-v-6acd124c]:after {
        content: '';
        width: 6px;
        height: 4px;
        background-image: url(/templates/hostfly/img/icons/arrow-down-mini.png);
        -webkit-background-size: 6px 4px;
        -moz-background-size: 6px 4px;
        -o-background-size: 6px 4px;
        background-size: 6px 4px;
        position: absolute;
        top: 13px;
        right: -10px;
        margin-left: 4px
    }

    #app .header .header__info__support__phone[data-v-6acd124c]:hover {
        text-shadow: 0 0 18px
    }

    #app .header .header__info__support__phone .phonecode[data-v-6acd124c] {
        font-size: 14px
    }

    #app .header .phones-popup[data-v-6acd124c] {
        top: 80px;
        width: 520px;
        left: -100px;
        -webkit-box-shadow: 0 5px 6px rgba(0, 0, 0, .2);
        -moz-box-shadow: 0 5px 6px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 6px rgba(0, 0, 0, .2)
    }

    #app .header .phones-popup .h3[data-v-6acd124c] {
        margin-bottom: 10px
    }

    #app .header .phones-popup .h2[data-v-6acd124c] {
        margin-bottom: 0
    }

    #app .header .phones-popup a[data-v-6acd124c] {
        border-color: #eb276d
    }

    #app .header .phones-popup a[data-v-6acd124c]:hover {
        border-color: transparent
    }

    #app .header .phones-popup .num[data-v-6acd124c] {
        width: 85%;
        white-space: nowrap
    }

    #app .header .phones-popup .worktime[data-v-6acd124c] {
        white-space: nowrap
    }

    #app .header .phones-popup .operator[data-v-6acd124c] {
        width: 15%;
        font-size: 15px;
        color: #919191;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: end;
        -webkit-align-items: flex-end;
        -moz-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end
    }

    #app .header__info__menu[data-v-6acd124c] {
        width: 100%
    }

    #app .top-menu__list[data-v-6acd124c] {
        list-style-type: none;
        padding: 0;
        margin-bottom: 0
    }

    #app .top-menu__list .top-menu__item[data-v-6acd124c] {
        font-size: 18px;
        font-weight: 500
    }

    #app .top-menu__list .top-menu__item[data-v-6acd124c]:before {
        display: none !important
    }

    #app .top-menu__list .top-menu__item a[data-v-6acd124c] {
        color: #fff
    }

    #app .top-menu__list .top-menu__item a[data-v-6acd124c]:hover {
        color: #fff;
        border-color: #c4dc02
    }

    #app .top-menu__list .top-menu__item .submenu[data-v-6acd124c] {
        position: absolute;
        background: #fff;
        padding: 35px;
        min-width: 280px;
        -webkit-border-radius: 18px;
        -moz-border-radius: 18px;
        border-radius: 18px;
        margin-top: 25px;
        opacity: 0;
        visibility: hidden;
        -webkit-transition: .5s .5s;
        -o-transition: .5s .5s;
        -moz-transition: .5s .5s;
        transition: .5s .5s
    }

    #app .top-menu__list .top-menu__item .submenu[data-v-6acd124c]:before {
        content: '';
        width: 20px;
        height: 20px;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg);
        position: absolute;
        top: -10px;
        left: 25px;
        background: #fff
    }

    #app .top-menu__list .top-menu__item .submenu .link[data-v-6acd124c], #app .top-menu__list .top-menu__item .submenu a[data-v-6acd124c] {
        color: #333;
        font-size: 18px;
        text-transform: none
    }

    #app .top-menu__list .top-menu__item .submenu .link[data-v-6acd124c]:hover, #app .top-menu__list .top-menu__item .submenu a[data-v-6acd124c]:hover {
        color: #eb276d;
        border-color: transparent
    }

    #app .top-menu__list .top-menu__item .submenu div[data-v-6acd124c] {
        margin-top: 25px
    }

    #app .top-menu__list .top-menu__item .submenu div[data-v-6acd124c]:first-child {
        margin-top: 0
    }

    #app .top-menu__list .top-menu__item:hover .submenu[data-v-6acd124c] {
        opacity: 1;
        visibility: visible;
        -webkit-transition: .5s .2s;
        -o-transition: .5s .2s;
        -moz-transition: .5s .2s;
        transition: .5s .2s
    }

    #app .top-menu__list .top-menu__item:last-child .submenu[data-v-6acd124c] {
        right: 0
    }

    #app .top-menu__list .top-menu__item:last-child .submenu[data-v-6acd124c]:before {
        left: 80%
    }

    #app .menu-button[data-v-6acd124c] {
        width: 50px;
        height: 50px;
        cursor: pointer;
        display: none
    }

    #app .menu-button .lines[data-v-6acd124c] {
        width: 26px;
        height: 29px;
        position: relative
    }

    #app .menu-button .lines .line[data-v-6acd124c] {
        background: #c4dc02;
        height: 4px;
        position: absolute;
        left: 0;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-transition: .4s;
        -o-transition: .4s;
        -moz-transition: .4s;
        transition: .4s
    }

    #app .menu-button .lines .line[data-v-6acd124c]:nth-child(1) {
        width: 90%;
        top: 0
    }

    #app .menu-button .lines .line[data-v-6acd124c]:nth-child(2) {
        width: 100%;
        top: -webkit-calc(50% - 1px);
        top: -moz-calc(50% - 1px);
        top: calc(50% - 1px)
    }

    #app .menu-button .lines .line[data-v-6acd124c]:nth-child(3) {
        width: 80%;
        top: -webkit-calc(100% - 4px);
        top: -moz-calc(100% - 4px);
        top: calc(100% - 4px)
    }

    #app .menu-button:hover .lines .line[data-v-6acd124c]:nth-child(1) {
        width: 100%
    }

    #app .menu-button:hover .lines .line[data-v-6acd124c]:nth-child(3) {
        width: 100%
    }

    #app .menu-button.open .lines .line[data-v-6acd124c]:nth-child(1) {
        width: 100%;
        top: 48%;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg)
    }

    #app .menu-button.open .lines .line[data-v-6acd124c]:nth-child(2) {
        width: 0;
        left: 50%;
        -webkit-transition: .1s;
        -o-transition: .1s;
        -moz-transition: .1s;
        transition: .1s
    }

    #app .menu-button.open .lines .line[data-v-6acd124c]:nth-child(3) {
        width: 100%;
        top: 48%;
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg)
    }

    @media screen and (max-width: 1150px) {
        #app .top-menu__list .top-menu__item[data-v-6acd124c] {
            font-size: 16px
        }
    }

    @media screen and (max-width: 959px) {
        #app .header__logo[data-v-6acd124c] {
            position: absolute;
            top: 15px;
            padding: 0
        }

        #app .header__info[data-v-6acd124c] {
            padding: 0
        }

        #app .header .header__info__support[data-v-6acd124c] {
            right: 0
        }
    }

    @media screen and (max-width: 767px) {
        #app .header__info__menu[data-v-6acd124c] {
            display: block;
            visibility: hidden;
            pointer-events: none;
            opacity: 0;
            max-height: 0;
            -webkit-transition: .3s;
            -o-transition: .3s;
            -moz-transition: .3s;
            transition: .3s;
            background: rgba(0, 0, 0, .7);
            min-width: 70vw;
            max-width: 100vw;
            position: absolute;
            right: 0;
            top: 90px
        }

        #app .header__info__menu.open[data-v-6acd124c] {
            visibility: visible;
            opacity: 1;
            max-height: 1000px;
            pointer-events: all
        }

        #app .header__info__menu .top-menu__list[data-v-6acd124c] {
            display: block;
            padding: 20px
        }

        #app .header__logo[data-v-6acd124c] {
            top: 0;
            padding: 0 15px 15px 30px;
            position: relative;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -moz-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            background: #1f1250;
            height: 80px;
            -webkit-flex-basis: 40%;
            -ms-flex-preferred-size: 40%;
            flex-basis: 40%
        }

        #app .header__logo svg[data-v-6acd124c] {
            width: 120px;
            height: 45px
        }

        #app .header .phones-popup[data-v-6acd124c] {
            width: 100vw;
            left: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0
        }

        #app .header .header__info[data-v-6acd124c] {
            -webkit-flex-basis: 60%;
            -ms-flex-preferred-size: 60%;
            flex-basis: 60%;
            max-width: 60%;
            height: 80px;
            background: #1f1250
        }

        #app .header .header__info__support[data-v-6acd124c] {
            max-width: 100vw;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            min-width: 0;
            width: auto
        }

        #app .header__info__support__phone[data-v-6acd124c] {
            margin-right: 25px
        }

        #app .btn--lemon[data-v-6acd124c] {
            display: none
        }

        #app .menu-button[data-v-6acd124c] {
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            padding: 5px;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px
        }
    }

    @media screen and (max-width: 575px) {
        #app .header[data-v-6acd124c] {
            height: 60px;
            position: fixed
        }

        #app .header .header__logo[data-v-6acd124c] {
            -webkit-flex-basis: 60%;
            -ms-flex-preferred-size: 60%;
            flex-basis: 60%;
            max-width: 60%
        }

        #app .header .header__logo svg[data-v-6acd124c] {
            width: 100%;
            height: 100%
        }

        #app .header .header__info[data-v-6acd124c] {
            -webkit-flex-basis: 40%;
            -ms-flex-preferred-size: 40%;
            flex-basis: 40%;
            max-width: 40%;
            background: #1f1250
        }

        #app .header .header__info__support[data-v-6acd124c] {
            top: 0;
            position: static
        }

        #app .header .header__info__support__phone[data-v-6acd124c] {
            font-size: 0;
            display: block;
            width: 35px;
            height: 35px;
            background: url(/templates/hostfly/img/icons/phone-call-icon.svg) center no-repeat;
            -webkit-background-size: 80% 80%;
            -moz-background-size: 80% 80%;
            -o-background-size: 80% 80%;
            background-size: 80% 80%
        }

        #app .header .header__info__support__phone .phonecode[data-v-6acd124c] {
            display: none
        }

        #app .header .header__info__support__phone[data-v-6acd124c]:after {
            display: none
        }

        #app .header .header__info__menu[data-v-6acd124c] {
            width: 100vw;
            min-height: 100vh;
            right: 0;
            top: 80px;
            background: #1f1250
        }

        #app .header .header__info__menu[data-v-6acd124c]:after {
            content: '';
            display: block;
            width: 100%;
            height: 80px;
            background: -webkit-gradient(linear, left top, left bottom, from(#1f1250), to(transparent));
            background: -webkit-linear-gradient(#1f1250, transparent);
            background: -moz-linear-gradient(#1f1250, transparent);
            background: -o-linear-gradient(#1f1250, transparent);
            background: linear-gradient(#1f1250, transparent);
            position: absolute
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item[data-v-6acd124c] {
            padding-left: 25px;
            border-bottom: 1px solid rgba(250, 250, 250, .15);
            padding-bottom: 5px
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item.parent[data-v-6acd124c]:before {
            position: absolute;
            display: inline-block !important;
            width: 20px;
            height: 20px;
            background: url(/templates/hostfly/img/icons/arrow-down-mini.png) center no-repeat;
            -webkit-background-size: auto auto;
            -moz-background-size: auto;
            -o-background-size: auto;
            background-size: auto;
            content: '';
            margin-top: -4px
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item > a[data-v-6acd124c] {
            font-size: 19px
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item .submenu[data-v-6acd124c] {
            position: relative;
            background: 0 0;
            text-transform: uppercase;
            font-weight: 300;
            padding: 0 35px;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            margin-top: 0;
            opacity: 1;
            visibility: visible;
            max-height: 0;
            overflow: hidden;
            -webkit-transition: .4s;
            -o-transition: .4s;
            -moz-transition: .4s;
            transition: .4s
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item .submenu[data-v-6acd124c]:before {
            display: none
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item .submenu .link[data-v-6acd124c], #app .header .header__info__menu .top-menu__list .top-menu__item .submenu a[data-v-6acd124c] {
            color: #fff;
            font-size: 15px;
            text-transform: uppercase;
            line-height: 30px
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item .submenu div[data-v-6acd124c] {
            margin-top: 5px
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item .submenu div[data-v-6acd124c]:first-child {
            margin-top: 0
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item.open[data-v-6acd124c]:before {
            -webkit-transform: rotate(180deg);
            -moz-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            -o-transform: rotate(180deg);
            transform: rotate(180deg)
        }

        #app .header .header__info__menu .top-menu__list .top-menu__item.open .submenu[data-v-6acd124c] {
            max-height: 100vh;
            padding: 15px 35px
        }

        #app .header .header__info__support[data-v-6acd124c] {
            background: 0 0
        }

        #app .header .header__info__support__text[data-v-6acd124c] {
            display: none
        }

        #app .header .header__info__menu-button[data-v-6acd124c] {
            display: block
        }

        #app .header .note-popup .wrap[data-v-6acd124c] {
            padding: 11vw
        }
        #app .header .note-popup{
            opacity: 1;
            visibility: inherit;
        }
    }

    /*# sourceURL=/home/www/hostfly/data/www/project/local/assets/source/vue/components/template/Header/<input css 7> */
    /*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3d3dy9ob3N0Zmx5L2RhdGEvd3d3L3Byb2plY3QvbG9jYWwvYXNzZXRzL3NvdXJjZS92dWUvY29tcG9uZW50cy90ZW1wbGF0ZS9IZWFkZXIvPGlucHV0IGNzcyA3PiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEseUVBQXdELGFBQWE7Q0FBQztBQUFBLGdFQUErQyxhQUFhO0NBQUM7QUFBQSw4QkFBYSxhQUFhLFdBQVcsU0FBUztDQUFDO0FBQUEsNENBQTJCLGdCQUFnQjtDQUFDO0FBQUEsa0RBQWlDLFlBQVksWUFBWSxlQUFlLGFBQWE7Q0FBQztBQUFBLG9EQUFtQyx3QkFBd0I7Q0FBQztBQUFBLDRDQUEyQixnQkFBZ0I7Q0FBQztBQUFBLHFEQUFvQyxrQkFBa0IsTUFBTSxRQUFRLFlBQVksMEJBQTBCLGtCQUFrQixrQ0FBa0MsK0JBQStCLHlCQUF5QjtDQUFDO0FBQUEsMkRBQTBDLFlBQVk7Q0FBQztBQUFBLDREQUEyQyxlQUFlLHVCQUF1QixrQkFBa0Isb0JBQW9CLGVBQWUsaUJBQWlCO0NBQUM7QUFBQSxrRUFBaUQsV0FBVyxVQUFVLFdBQVcscUVBQXFFLGdDQUFnQyw2QkFBNkIsMkJBQTJCLHdCQUF3QixrQkFBa0IsU0FBUyxlQUFlO0NBQUM7QUFBQSxrRUFBaUQsb0JBQW9CO0NBQUM7QUFBQSx1RUFBc0QsY0FBYztDQUFDO0FBQUEsNENBQTJCLFNBQVMsWUFBWSxZQUFZLDRDQUE0Qyx5Q0FBeUMsbUNBQW1DO0NBQUM7QUFBQSxnREFBK0Isa0JBQWtCO0NBQUM7QUFBQSxnREFBK0IsZUFBZTtDQUFDO0FBQUEsOENBQTZCLG9CQUFvQjtDQUFDO0FBQUEsb0RBQW1DLHdCQUF3QjtDQUFDO0FBQUEsaURBQWdDLFVBQVUsa0JBQWtCO0NBQUM7QUFBQSxzREFBcUMsa0JBQWtCO0NBQUM7QUFBQSxzREFBcUMsVUFBVSxlQUFlLGNBQWMsb0JBQW9CLHFCQUFxQixpQkFBaUIsb0JBQW9CLGFBQWEsc0JBQXNCLDZCQUE2QixtQkFBbUIsbUJBQW1CLG9CQUFvQjtDQUFDO0FBQUEsMENBQXlCLFVBQVU7Q0FBQztBQUFBLHNDQUFxQixxQkFBcUIsVUFBVSxlQUFlO0NBQUM7QUFBQSxzREFBcUMsZUFBZSxlQUFlO0NBQUM7QUFBQSw2REFBNEMsc0JBQXNCO0NBQUM7QUFBQSx3REFBdUMsVUFBVTtDQUFDO0FBQUEsOERBQTZDLFdBQVcsb0JBQW9CO0NBQUM7QUFBQSwrREFBOEMsa0JBQWtCLGdCQUFnQixhQUFhLGdCQUFnQiwyQkFBMkIsd0JBQXdCLG1CQUFtQixnQkFBZ0IsVUFBVSxrQkFBa0IsMkJBQTJCLHNCQUFzQix3QkFBd0Isa0JBQWtCO0NBQUM7QUFBQSxzRUFBcUQsV0FBVyxXQUFXLFlBQVksZ0NBQWdDLDZCQUE2Qiw0QkFBNEIsMkJBQTJCLHdCQUF3QixrQkFBa0IsVUFBVSxVQUFVLGVBQWU7Q0FBQztBQUFBLHNJQUFvRyxXQUFXLGVBQWUsbUJBQW1CO0NBQUM7QUFBQSxrSkFBZ0gsY0FBYyx3QkFBd0I7Q0FBQztBQUFBLG1FQUFrRCxlQUFlO0NBQUM7QUFBQSwrRUFBOEQsWUFBWTtDQUFDO0FBQUEscUVBQW9ELFVBQVUsbUJBQW1CLDJCQUEyQixzQkFBc0Isd0JBQXdCLGtCQUFrQjtDQUFDO0FBQUEsMEVBQXlELE9BQU87Q0FBQztBQUFBLGlGQUFnRSxRQUFRO0NBQUM7QUFBQSxtQ0FBa0IsV0FBVyxZQUFZLGVBQWUsWUFBWTtDQUFDO0FBQUEsMENBQXlCLFdBQVcsWUFBWSxpQkFBaUI7Q0FBQztBQUFBLGdEQUErQixtQkFBbUIsV0FBVyxrQkFBa0IsT0FBTywwQkFBMEIsdUJBQXVCLGtCQUFrQix1QkFBdUIsa0JBQWtCLG9CQUFvQixjQUFjO0NBQUM7QUFBQSw2REFBNEMsVUFBVSxLQUFLO0NBQUM7QUFBQSw2REFBNEMsV0FBVyw0QkFBNEIseUJBQXlCLG1CQUFtQjtDQUFDO0FBQUEsNkRBQTRDLFVBQVUsNkJBQTZCLDBCQUEwQixvQkFBb0I7Q0FBQztBQUFBLG1FQUFrRCxVQUFVO0NBQUM7QUFBQSxtRUFBa0QsVUFBVTtDQUFDO0FBQUEsa0VBQWlELFdBQVcsUUFBUSxnQ0FBZ0MsNkJBQTZCLDRCQUE0QiwyQkFBMkIsdUJBQXVCO0NBQUM7QUFBQSxrRUFBaUQsUUFBUSxTQUFTLHVCQUF1QixrQkFBa0Isb0JBQW9CLGNBQWM7Q0FBQztBQUFBLGtFQUFpRCxXQUFXLFFBQVEsaUNBQWlDLDhCQUE4Qiw2QkFBNkIsNEJBQTRCLHdCQUF3QjtDQUFDO0FBQUE7QUFBcUMsc0RBQXFDLGNBQWM7Q0FBQztDQUFDO0FBQUE7QUFBb0Msb0NBQW1CLGtCQUFrQixTQUFTLFNBQVM7Q0FBQztBQUFBLG9DQUFtQixTQUFTO0NBQUM7QUFBQSxxREFBb0MsT0FBTztDQUFDO0NBQUM7QUFBQTtBQUFvQywwQ0FBeUIsY0FBYyxrQkFBa0Isb0JBQW9CLFVBQVUsYUFBYSx1QkFBdUIsa0JBQWtCLG9CQUFvQixlQUFlLDBCQUEwQixlQUFlLGdCQUFnQixrQkFBa0IsUUFBUSxRQUFRO0NBQUM7QUFBQSwrQ0FBOEIsbUJBQW1CLFVBQVUsa0JBQWtCLGtCQUFrQjtDQUFDO0FBQUEsMERBQXlDLGNBQWMsWUFBWTtDQUFDO0FBQUEsb0NBQW1CLE1BQU0seUJBQXlCLGtCQUFrQix5QkFBeUIsMkJBQTJCLHNCQUFzQixzQkFBc0IsbUJBQW1CLG1CQUFtQixZQUFZLHVCQUF1Qiw0QkFBNEIsY0FBYztDQUFDO0FBQUEsd0NBQXVCLFlBQVksV0FBVztDQUFDO0FBQUEsNENBQTJCLFlBQVksT0FBTyx3QkFBd0IscUJBQXFCLGVBQWU7Q0FBQztBQUFBLDRDQUEyQix1QkFBdUIsNEJBQTRCLGVBQWUsY0FBYyxZQUFZLGtCQUFrQjtDQUFDO0FBQUEscURBQW9DLGdCQUFnQix5QkFBeUIscUJBQXFCLGlCQUFpQixZQUFZLFVBQVU7Q0FBQztBQUFBLG9EQUFtQyxpQkFBaUI7Q0FBQztBQUFBLGtDQUFpQixZQUFZO0NBQUM7QUFBQSxtQ0FBa0Isb0JBQW9CLHFCQUFxQixpQkFBaUIsb0JBQW9CLGFBQWEsWUFBWSwwQkFBMEIsdUJBQXVCLGlCQUFpQjtDQUFDO0NBQUM7QUFBQTtBQUFvQyw4QkFBYSxZQUFZLGNBQWM7Q0FBQztBQUFBLDRDQUEyQix1QkFBdUIsNEJBQTRCLGVBQWUsYUFBYTtDQUFDO0FBQUEsZ0RBQStCLFdBQVcsV0FBVztDQUFDO0FBQUEsNENBQTJCLHVCQUF1Qiw0QkFBNEIsZUFBZSxjQUFjLGtCQUFrQjtDQUFDO0FBQUEscURBQW9DLE1BQU0sZUFBZTtDQUFDO0FBQUEsNERBQTJDLFlBQVksY0FBYyxXQUFXLFlBQVksZ0ZBQWdGLGdDQUFnQyw2QkFBNkIsMkJBQTJCLHVCQUF1QjtDQUFDO0FBQUEsdUVBQXNELFlBQVk7Q0FBQztBQUFBLGtFQUFpRCxZQUFZO0NBQUM7QUFBQSxrREFBaUMsWUFBWSxpQkFBaUIsUUFBUSxTQUFTLGtCQUFrQjtDQUFDO0FBQUEsd0RBQXVDLFdBQVcsY0FBYyxXQUFXLFlBQVksdUZBQXVGLHdEQUF3RCxxREFBcUQsbURBQW1ELGdEQUFnRCxpQkFBaUI7Q0FBQztBQUFBLGtGQUFpRSxrQkFBa0IsOENBQThDLGtCQUFrQjtDQUFDO0FBQUEsZ0dBQStFLGtCQUFrQiwrQkFBK0IsV0FBVyxZQUFZLGdGQUFnRixrQ0FBa0MsMEJBQTBCLHdCQUF3QixxQkFBcUIsV0FBVyxlQUFlO0NBQUM7QUFBQSxvRkFBbUUsY0FBYztDQUFDO0FBQUEsMkZBQTBFLGtCQUFrQixlQUFlLHlCQUF5QixnQkFBZ0IsZUFBZSx3QkFBd0IscUJBQXFCLGdCQUFnQixhQUFhLFVBQVUsbUJBQW1CLGFBQWEsZ0JBQWdCLHVCQUF1QixrQkFBa0Isb0JBQW9CLGNBQWM7Q0FBQztBQUFBLGtHQUFpRixZQUFZO0NBQUM7QUFBQSw4TEFBNEosV0FBVyxlQUFlLHlCQUF5QixnQkFBZ0I7Q0FBQztBQUFBLCtGQUE4RSxjQUFjO0NBQUM7QUFBQSwyR0FBMEYsWUFBWTtDQUFDO0FBQUEsOEZBQTZFLGlDQUFpQyw4QkFBOEIsNkJBQTZCLDRCQUE0Qix3QkFBd0I7Q0FBQztBQUFBLGdHQUErRSxpQkFBaUIsaUJBQWlCO0NBQUM7QUFBQSxxREFBb0MsY0FBYztDQUFDO0FBQUEsMkRBQTBDLFlBQVk7Q0FBQztBQUFBLHlEQUF3QyxhQUFhO0NBQUM7QUFBQSxnREFBK0IsWUFBWTtDQUFDO0NBQUMiLCJmaWxlIjoic3R5bGVzLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjYXBwLmRlZGljYXRlZC1zZXJ2ZXItc3R5bGUgLmhlYWRlciAuaGVhZGVyX19sb2dvIC5sb2dve2NvbG9yOiMwMWFkYzN9I2FwcC5ob3N0aW5nLXN0eWxlIC5oZWFkZXIgLmhlYWRlcl9fbG9nbyAubG9nb3tjb2xvcjojOGRjNDNifSNhcHAgLmhlYWRlcntoZWlnaHQ6MTMwcHg7ei1pbmRleDo1MDtwYWRkaW5nOjB9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2xvZ297cGFkZGluZy1ib3R0b206MH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9fbG9nbyAubG9nb3t3aWR0aDoyMjJweDtoZWlnaHQ6NjVweDttYXgtd2lkdGg6MTAwJTtjb2xvcjojZWIyNzZkfSNhcHAgLmhlYWRlciAuaGVhZGVyX19sb2dvIGE6aG92ZXJ7Ym9yZGVyLWNvbG9yOnRyYW5zcGFyZW50fSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZve3BhZGRpbmctYm90dG9tOjB9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2luZm9fX3N1cHBvcnR7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7cmlnaHQ6MDt3aWR0aDo0MjBweDtiYWNrZ3JvdW5kOnJnYmEoMCwwLDAsLjMpO3BhZGRpbmc6MTBweCAxOHB4Oy13ZWJraXQtYm9yZGVyLXJhZGl1czowIDAgOHB4IDhweDstbW96LWJvcmRlci1yYWRpdXM6MCAwIDhweCA4cHg7Ym9yZGVyLXJhZGl1czowIDAgOHB4IDhweH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fc3VwcG9ydDphZnRlcntkaXNwbGF5Om5vbmV9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2luZm9fX3N1cHBvcnRfX3Bob25le2N1cnNvcjpwb2ludGVyOy13ZWJraXQtdHJhbnNpdGlvbjouNXM7LW8tdHJhbnNpdGlvbjouNXM7LW1vei10cmFuc2l0aW9uOi41czt0cmFuc2l0aW9uOi41cztwb3NpdGlvbjpyZWxhdGl2ZX0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fc3VwcG9ydF9fcGhvbmU6YWZ0ZXJ7Y29udGVudDonJzt3aWR0aDo2cHg7aGVpZ2h0OjRweDtiYWNrZ3JvdW5kLWltYWdlOnVybCgvbG9jYWwvYXNzZXRzL2ltYWdlcy9pY29ucy9hcnJvdy1kb3duLW1pbmkucG5nKTstd2Via2l0LWJhY2tncm91bmQtc2l6ZTo2cHggNHB4Oy1tb3otYmFja2dyb3VuZC1zaXplOjZweCA0cHg7LW8tYmFja2dyb3VuZC1zaXplOjZweCA0cHg7YmFja2dyb3VuZC1zaXplOjZweCA0cHg7cG9zaXRpb246YWJzb2x1dGU7dG9wOjEzcHg7bWFyZ2luLWxlZnQ6NHB4fSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19zdXBwb3J0X19waG9uZTpob3Zlcnt0ZXh0LXNoYWRvdzowIDAgMThweH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fc3VwcG9ydF9fcGhvbmUgLnBob25lY29kZXtmb250LXNpemU6MTRweH0jYXBwIC5oZWFkZXIgLnBob25lcy1wb3B1cHt0b3A6ODBweDt3aWR0aDo1MjBweDtsZWZ0Oi0xMDBweDstd2Via2l0LWJveC1zaGFkb3c6MCA1cHggNnB4IHJnYmEoMCwwLDAsLjIpOy1tb3otYm94LXNoYWRvdzowIDVweCA2cHggcmdiYSgwLDAsMCwuMik7Ym94LXNoYWRvdzowIDVweCA2cHggcmdiYSgwLDAsMCwuMil9I2FwcCAuaGVhZGVyIC5waG9uZXMtcG9wdXAgLmgze21hcmdpbi1ib3R0b206MTBweH0jYXBwIC5oZWFkZXIgLnBob25lcy1wb3B1cCAuaDJ7bWFyZ2luLWJvdHRvbTowfSNhcHAgLmhlYWRlciAucGhvbmVzLXBvcHVwIGF7Ym9yZGVyLWNvbG9yOiNlYjI3NmR9I2FwcCAuaGVhZGVyIC5waG9uZXMtcG9wdXAgYTpob3Zlcntib3JkZXItY29sb3I6dHJhbnNwYXJlbnR9I2FwcCAuaGVhZGVyIC5waG9uZXMtcG9wdXAgLm51bXt3aWR0aDo4NSU7d2hpdGUtc3BhY2U6bm93cmFwfSNhcHAgLmhlYWRlciAucGhvbmVzLXBvcHVwIC53b3JrdGltZXt3aGl0ZS1zcGFjZTpub3dyYXB9I2FwcCAuaGVhZGVyIC5waG9uZXMtcG9wdXAgLm9wZXJhdG9ye3dpZHRoOjE1JTtmb250LXNpemU6MTVweDtjb2xvcjojOTE5MTkxO2Rpc3BsYXk6LXdlYmtpdC1ib3g7ZGlzcGxheTotd2Via2l0LWZsZXg7ZGlzcGxheTotbW96LWJveDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1hbGlnbjplbmQ7LXdlYmtpdC1hbGlnbi1pdGVtczpmbGV4LWVuZDstbW96LWJveC1hbGlnbjplbmQ7LW1zLWZsZXgtYWxpZ246ZW5kO2FsaWduLWl0ZW1zOmZsZXgtZW5kfSNhcHAgLmhlYWRlcl9faW5mb19fbWVudXt3aWR0aDoxMDAlfSNhcHAgLnRvcC1tZW51X19saXN0e2xpc3Qtc3R5bGUtdHlwZTpub25lO3BhZGRpbmc6MDttYXJnaW4tYm90dG9tOjB9I2FwcCAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVte2ZvbnQtc2l6ZToxOHB4O2ZvbnQtd2VpZ2h0OjUwMH0jYXBwIC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW06YmVmb3Jle2Rpc3BsYXk6bm9uZSFpbXBvcnRhbnR9I2FwcCAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtIGF7Y29sb3I6I2ZmZn0jYXBwIC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW0gYTpob3Zlcntjb2xvcjojZmZmO2JvcmRlci1jb2xvcjojYzRkYzAyfSNhcHAgLnRvcC1tZW51X19saXN0IC50b3AtbWVudV9faXRlbSAuc3VibWVudXtwb3NpdGlvbjphYnNvbHV0ZTtiYWNrZ3JvdW5kOiNmZmY7cGFkZGluZzozNXB4O21pbi13aWR0aDoyODBweDstd2Via2l0LWJvcmRlci1yYWRpdXM6MThweDstbW96LWJvcmRlci1yYWRpdXM6MThweDtib3JkZXItcmFkaXVzOjE4cHg7bWFyZ2luLXRvcDoyNXB4O29wYWNpdHk6MDt2aXNpYmlsaXR5OmhpZGRlbjstd2Via2l0LXRyYW5zaXRpb246LjVzIC41czstby10cmFuc2l0aW9uOi41cyAuNXM7LW1vei10cmFuc2l0aW9uOi41cyAuNXM7dHJhbnNpdGlvbjouNXMgLjVzfSNhcHAgLnRvcC1tZW51X19saXN0IC50b3AtbWVudV9faXRlbSAuc3VibWVudTpiZWZvcmV7Y29udGVudDonJzt3aWR0aDoyMHB4O2hlaWdodDoyMHB4Oy13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyk7LW1vei10cmFuc2Zvcm06cm90YXRlKDQ1ZGVnKTstbXMtdHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyk7LW8tdHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyk7dHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyk7cG9zaXRpb246YWJzb2x1dGU7dG9wOi0xMHB4O2xlZnQ6MjVweDtiYWNrZ3JvdW5kOiNmZmZ9I2FwcCAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtIC5zdWJtZW51IC5saW5rLCNhcHAgLnRvcC1tZW51X19saXN0IC50b3AtbWVudV9faXRlbSAuc3VibWVudSBhe2NvbG9yOiMzMzM7Zm9udC1zaXplOjE4cHg7dGV4dC10cmFuc2Zvcm06bm9uZX0jYXBwIC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW0gLnN1Ym1lbnUgLmxpbms6aG92ZXIsI2FwcCAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtIC5zdWJtZW51IGE6aG92ZXJ7Y29sb3I6I2ViMjc2ZDtib3JkZXItY29sb3I6dHJhbnNwYXJlbnR9I2FwcCAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtIC5zdWJtZW51IGRpdnttYXJnaW4tdG9wOjI1cHh9I2FwcCAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtIC5zdWJtZW51IGRpdjpmaXJzdC1jaGlsZHttYXJnaW4tdG9wOjB9I2FwcCAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtOmhvdmVyIC5zdWJtZW51e29wYWNpdHk6MTt2aXNpYmlsaXR5OnZpc2libGU7LXdlYmtpdC10cmFuc2l0aW9uOi41cyAuMnM7LW8tdHJhbnNpdGlvbjouNXMgLjJzOy1tb3otdHJhbnNpdGlvbjouNXMgLjJzO3RyYW5zaXRpb246LjVzIC4yc30jYXBwIC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW06bGFzdC1jaGlsZCAuc3VibWVudXtyaWdodDowfSNhcHAgLnRvcC1tZW51X19saXN0IC50b3AtbWVudV9faXRlbTpsYXN0LWNoaWxkIC5zdWJtZW51OmJlZm9yZXtsZWZ0OjgwJX0jYXBwIC5tZW51LWJ1dHRvbnt3aWR0aDo1MHB4O2hlaWdodDo1MHB4O2N1cnNvcjpwb2ludGVyO2Rpc3BsYXk6bm9uZX0jYXBwIC5tZW51LWJ1dHRvbiAubGluZXN7d2lkdGg6MjZweDtoZWlnaHQ6MjlweDtwb3NpdGlvbjpyZWxhdGl2ZX0jYXBwIC5tZW51LWJ1dHRvbiAubGluZXMgLmxpbmV7YmFja2dyb3VuZDojYzRkYzAyO2hlaWdodDo0cHg7cG9zaXRpb246YWJzb2x1dGU7bGVmdDowOy13ZWJraXQtYm9yZGVyLXJhZGl1czo0cHg7LW1vei1ib3JkZXItcmFkaXVzOjRweDtib3JkZXItcmFkaXVzOjRweDstd2Via2l0LXRyYW5zaXRpb246LjRzOy1vLXRyYW5zaXRpb246LjRzOy1tb3otdHJhbnNpdGlvbjouNHM7dHJhbnNpdGlvbjouNHN9I2FwcCAubWVudS1idXR0b24gLmxpbmVzIC5saW5lOm50aC1jaGlsZCgxKXt3aWR0aDo5MCU7dG9wOjB9I2FwcCAubWVudS1idXR0b24gLmxpbmVzIC5saW5lOm50aC1jaGlsZCgyKXt3aWR0aDoxMDAlO3RvcDotd2Via2l0LWNhbGMoNTAlIC0gMXB4KTt0b3A6LW1vei1jYWxjKDUwJSAtIDFweCk7dG9wOmNhbGMoNTAlIC0gMXB4KX0jYXBwIC5tZW51LWJ1dHRvbiAubGluZXMgLmxpbmU6bnRoLWNoaWxkKDMpe3dpZHRoOjgwJTt0b3A6LXdlYmtpdC1jYWxjKDEwMCUgLSA0cHgpO3RvcDotbW96LWNhbGMoMTAwJSAtIDRweCk7dG9wOmNhbGMoMTAwJSAtIDRweCl9I2FwcCAubWVudS1idXR0b246aG92ZXIgLmxpbmVzIC5saW5lOm50aC1jaGlsZCgxKXt3aWR0aDoxMDAlfSNhcHAgLm1lbnUtYnV0dG9uOmhvdmVyIC5saW5lcyAubGluZTpudGgtY2hpbGQoMyl7d2lkdGg6MTAwJX0jYXBwIC5tZW51LWJ1dHRvbi5vcGVuIC5saW5lcyAubGluZTpudGgtY2hpbGQoMSl7d2lkdGg6MTAwJTt0b3A6NDglOy13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyk7LW1vei10cmFuc2Zvcm06cm90YXRlKDQ1ZGVnKTstbXMtdHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyk7LW8tdHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyk7dHJhbnNmb3JtOnJvdGF0ZSg0NWRlZyl9I2FwcCAubWVudS1idXR0b24ub3BlbiAubGluZXMgLmxpbmU6bnRoLWNoaWxkKDIpe3dpZHRoOjA7bGVmdDo1MCU7LXdlYmtpdC10cmFuc2l0aW9uOi4xczstby10cmFuc2l0aW9uOi4xczstbW96LXRyYW5zaXRpb246LjFzO3RyYW5zaXRpb246LjFzfSNhcHAgLm1lbnUtYnV0dG9uLm9wZW4gLmxpbmVzIC5saW5lOm50aC1jaGlsZCgzKXt3aWR0aDoxMDAlO3RvcDo0OCU7LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKC00NWRlZyk7LW1vei10cmFuc2Zvcm06cm90YXRlKC00NWRlZyk7LW1zLXRyYW5zZm9ybTpyb3RhdGUoLTQ1ZGVnKTstby10cmFuc2Zvcm06cm90YXRlKC00NWRlZyk7dHJhbnNmb3JtOnJvdGF0ZSgtNDVkZWcpfUBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6MTE1MHB4KXsjYXBwIC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW17Zm9udC1zaXplOjE2cHh9fUBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6OTU5cHgpeyNhcHAgLmhlYWRlcl9fbG9nb3twb3NpdGlvbjphYnNvbHV0ZTt0b3A6MTVweDtwYWRkaW5nOjB9I2FwcCAuaGVhZGVyX19pbmZve3BhZGRpbmc6MH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fc3VwcG9ydHtyaWdodDowfX1AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjc2N3B4KXsjYXBwIC5oZWFkZXJfX2luZm9fX21lbnV7ZGlzcGxheTpibG9jazt2aXNpYmlsaXR5OmhpZGRlbjtwb2ludGVyLWV2ZW50czpub25lO29wYWNpdHk6MDttYXgtaGVpZ2h0OjA7LXdlYmtpdC10cmFuc2l0aW9uOi4zczstby10cmFuc2l0aW9uOi4zczstbW96LXRyYW5zaXRpb246LjNzO3RyYW5zaXRpb246LjNzO2JhY2tncm91bmQ6cmdiYSgwLDAsMCwuNyk7bWluLXdpZHRoOjcwdnc7bWF4LXdpZHRoOjEwMHZ3O3Bvc2l0aW9uOmFic29sdXRlO3JpZ2h0OjA7dG9wOjkwcHh9I2FwcCAuaGVhZGVyX19pbmZvX19tZW51Lm9wZW57dmlzaWJpbGl0eTp2aXNpYmxlO29wYWNpdHk6MTttYXgtaGVpZ2h0OjEwMDBweDtwb2ludGVyLWV2ZW50czphbGx9I2FwcCAuaGVhZGVyX19pbmZvX19tZW51IC50b3AtbWVudV9fbGlzdHtkaXNwbGF5OmJsb2NrO3BhZGRpbmc6MjBweH0jYXBwIC5oZWFkZXJfX2xvZ297dG9wOjA7cGFkZGluZzowIDE1cHggMTVweCAzMHB4O3Bvc2l0aW9uOnJlbGF0aXZlOy13ZWJraXQtYm94LWFsaWduOmNlbnRlcjstd2Via2l0LWFsaWduLWl0ZW1zOmNlbnRlcjstbW96LWJveC1hbGlnbjpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjtiYWNrZ3JvdW5kOiMxZjEyNTA7aGVpZ2h0OjgwcHg7LXdlYmtpdC1mbGV4LWJhc2lzOjQwJTstbXMtZmxleC1wcmVmZXJyZWQtc2l6ZTo0MCU7ZmxleC1iYXNpczo0MCV9I2FwcCAuaGVhZGVyX19sb2dvIHN2Z3t3aWR0aDoxMjBweDtoZWlnaHQ6NDVweH0jYXBwIC5oZWFkZXIgLnBob25lcy1wb3B1cHt3aWR0aDoxMDB2dztsZWZ0OjA7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjA7LW1vei1ib3JkZXItcmFkaXVzOjA7Ym9yZGVyLXJhZGl1czowfSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvey13ZWJraXQtZmxleC1iYXNpczo2MCU7LW1zLWZsZXgtcHJlZmVycmVkLXNpemU6NjAlO2ZsZXgtYmFzaXM6NjAlO21heC13aWR0aDo2MCU7aGVpZ2h0OjgwcHg7YmFja2dyb3VuZDojMWYxMjUwfSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19zdXBwb3J0e21heC13aWR0aDoxMDB2dzstd2Via2l0LWZsZXgtd3JhcDpub3dyYXA7LW1zLWZsZXgtd3JhcDpub3dyYXA7ZmxleC13cmFwOm5vd3JhcDttaW4td2lkdGg6MDt3aWR0aDphdXRvfSNhcHAgLmhlYWRlcl9faW5mb19fc3VwcG9ydF9fcGhvbmV7bWFyZ2luLXJpZ2h0OjI1cHh9I2FwcCAuYnRuLS1sZW1vbntkaXNwbGF5Om5vbmV9I2FwcCAubWVudS1idXR0b257ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi13ZWJraXQtZmxleDtkaXNwbGF5Oi1tb3otYm94O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4O3BhZGRpbmc6NXB4Oy13ZWJraXQtYm9yZGVyLXJhZGl1czo4cHg7LW1vei1ib3JkZXItcmFkaXVzOjhweDtib3JkZXItcmFkaXVzOjhweH19QG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo1NzVweCl7I2FwcCAuaGVhZGVye2hlaWdodDo2MHB4O3Bvc2l0aW9uOmZpeGVkfSNhcHAgLmhlYWRlciAuaGVhZGVyX19sb2dvey13ZWJraXQtZmxleC1iYXNpczo2MCU7LW1zLWZsZXgtcHJlZmVycmVkLXNpemU6NjAlO2ZsZXgtYmFzaXM6NjAlO21heC13aWR0aDo2MCV9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2xvZ28gc3Zne3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCV9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2luZm97LXdlYmtpdC1mbGV4LWJhc2lzOjQwJTstbXMtZmxleC1wcmVmZXJyZWQtc2l6ZTo0MCU7ZmxleC1iYXNpczo0MCU7bWF4LXdpZHRoOjQwJTtiYWNrZ3JvdW5kOiMxZjEyNTB9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2luZm9fX3N1cHBvcnR7dG9wOjA7cG9zaXRpb246c3RhdGljfSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19zdXBwb3J0X19waG9uZXtmb250LXNpemU6MDtkaXNwbGF5OmJsb2NrO3dpZHRoOjM1cHg7aGVpZ2h0OjM1cHg7YmFja2dyb3VuZDp1cmwoL2xvY2FsL2Fzc2V0cy9pbWFnZXMvaWNvbnMvcGhvbmUtY2FsbC1pY29uLnN2ZykgY2VudGVyIG5vLXJlcGVhdDstd2Via2l0LWJhY2tncm91bmQtc2l6ZTo4MCUgODAlOy1tb3otYmFja2dyb3VuZC1zaXplOjgwJSA4MCU7LW8tYmFja2dyb3VuZC1zaXplOjgwJSA4MCU7YmFja2dyb3VuZC1zaXplOjgwJSA4MCV9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2luZm9fX3N1cHBvcnRfX3Bob25lIC5waG9uZWNvZGV7ZGlzcGxheTpub25lfSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19zdXBwb3J0X19waG9uZTphZnRlcntkaXNwbGF5Om5vbmV9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2luZm9fX21lbnV7d2lkdGg6MTAwdnc7bWluLWhlaWdodDoxMDB2aDtyaWdodDowO3RvcDo4MHB4O2JhY2tncm91bmQ6IzFmMTI1MH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fbWVudTphZnRlcntjb250ZW50OicnO2Rpc3BsYXk6YmxvY2s7d2lkdGg6MTAwJTtoZWlnaHQ6ODBweDtiYWNrZ3JvdW5kOi13ZWJraXQtZ3JhZGllbnQobGluZWFyLGxlZnQgdG9wLGxlZnQgYm90dG9tLGZyb20oIzFmMTI1MCksdG8odHJhbnNwYXJlbnQpKTtiYWNrZ3JvdW5kOi13ZWJraXQtbGluZWFyLWdyYWRpZW50KCMxZjEyNTAsdHJhbnNwYXJlbnQpO2JhY2tncm91bmQ6LW1vei1saW5lYXItZ3JhZGllbnQoIzFmMTI1MCx0cmFuc3BhcmVudCk7YmFja2dyb3VuZDotby1saW5lYXItZ3JhZGllbnQoIzFmMTI1MCx0cmFuc3BhcmVudCk7YmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQoIzFmMTI1MCx0cmFuc3BhcmVudCk7cG9zaXRpb246YWJzb2x1dGV9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2luZm9fX21lbnUgLnRvcC1tZW51X19saXN0IC50b3AtbWVudV9faXRlbXtwYWRkaW5nLWxlZnQ6MjVweDtib3JkZXItYm90dG9tOjFweCBzb2xpZCByZ2JhKDI1MCwyNTAsMjUwLC4xNSk7cGFkZGluZy1ib3R0b206NXB4fSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19tZW51IC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW0ucGFyZW50OmJlZm9yZXtwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5OmlubGluZS1ibG9jayFpbXBvcnRhbnQ7d2lkdGg6MjBweDtoZWlnaHQ6MjBweDtiYWNrZ3JvdW5kOnVybCgvbG9jYWwvYXNzZXRzL2ltYWdlcy9pY29ucy9hcnJvdy1kb3duLW1pbmkucG5nKSBjZW50ZXIgbm8tcmVwZWF0Oy13ZWJraXQtYmFja2dyb3VuZC1zaXplOmF1dG8gYXV0bzstbW96LWJhY2tncm91bmQtc2l6ZTphdXRvOy1vLWJhY2tncm91bmQtc2l6ZTphdXRvO2JhY2tncm91bmQtc2l6ZTphdXRvO2NvbnRlbnQ6Jyc7bWFyZ2luLXRvcDotNHB4fSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19tZW51IC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW0+YXtmb250LXNpemU6MTlweH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fbWVudSAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtIC5zdWJtZW51e3Bvc2l0aW9uOnJlbGF0aXZlO2JhY2tncm91bmQ6MCAwO3RleHQtdHJhbnNmb3JtOnVwcGVyY2FzZTtmb250LXdlaWdodDozMDA7cGFkZGluZzowIDM1cHg7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjA7LW1vei1ib3JkZXItcmFkaXVzOjA7Ym9yZGVyLXJhZGl1czowO21hcmdpbi10b3A6MDtvcGFjaXR5OjE7dmlzaWJpbGl0eTp2aXNpYmxlO21heC1oZWlnaHQ6MDtvdmVyZmxvdzpoaWRkZW47LXdlYmtpdC10cmFuc2l0aW9uOi40czstby10cmFuc2l0aW9uOi40czstbW96LXRyYW5zaXRpb246LjRzO3RyYW5zaXRpb246LjRzfSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19tZW51IC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW0gLnN1Ym1lbnU6YmVmb3Jle2Rpc3BsYXk6bm9uZX0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fbWVudSAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtIC5zdWJtZW51IC5saW5rLCNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19tZW51IC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW0gLnN1Ym1lbnUgYXtjb2xvcjojZmZmO2ZvbnQtc2l6ZToxNXB4O3RleHQtdHJhbnNmb3JtOnVwcGVyY2FzZTtsaW5lLWhlaWdodDozMHB4fSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19tZW51IC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW0gLnN1Ym1lbnUgZGl2e21hcmdpbi10b3A6NXB4fSNhcHAgLmhlYWRlciAuaGVhZGVyX19pbmZvX19tZW51IC50b3AtbWVudV9fbGlzdCAudG9wLW1lbnVfX2l0ZW0gLnN1Ym1lbnUgZGl2OmZpcnN0LWNoaWxke21hcmdpbi10b3A6MH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fbWVudSAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtLm9wZW46YmVmb3Jley13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgxODBkZWcpOy1tb3otdHJhbnNmb3JtOnJvdGF0ZSgxODBkZWcpOy1tcy10cmFuc2Zvcm06cm90YXRlKDE4MGRlZyk7LW8tdHJhbnNmb3JtOnJvdGF0ZSgxODBkZWcpO3RyYW5zZm9ybTpyb3RhdGUoMTgwZGVnKX0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fbWVudSAudG9wLW1lbnVfX2xpc3QgLnRvcC1tZW51X19pdGVtLm9wZW4gLnN1Ym1lbnV7bWF4LWhlaWdodDoxMDB2aDtwYWRkaW5nOjE1cHggMzVweH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fc3VwcG9ydHtiYWNrZ3JvdW5kOjAgMH0jYXBwIC5oZWFkZXIgLmhlYWRlcl9faW5mb19fc3VwcG9ydF9fdGV4dHtkaXNwbGF5Om5vbmV9I2FwcCAuaGVhZGVyIC5oZWFkZXJfX2luZm9fX21lbnUtYnV0dG9ue2Rpc3BsYXk6YmxvY2t9I2FwcCAuaGVhZGVyIC5ub3RlLXBvcHVwIC53cmFwe3BhZGRpbmc6MTF2d319Il0sInNvdXJjZVJvb3QiOiIifQ== */</style>
<style type="text/css">
    #app .footer[data-v-62d44d30] {
        background: -webkit-linear-gradient(290deg, #360e69, transparent 75%);
        background: -moz-linear-gradient(290deg, #360e69, transparent 75%);
        background: -o-linear-gradient(290deg, #360e69, transparent 75%);
        background: linear-gradient(160deg, #360e69, transparent 75%);
        -webkit-background-size: 100% 100%;
        -moz-background-size: 100% 100%;
        -o-background-size: 100% 100%;
        background-size: 100% 100%;
        background-repeat: no-repeat
    }

    #app .pretty-links-column[data-v-62d44d30] {
        border-bottom: 1px solid #472774
    }

    #app .pretty-links-column .container[data-v-62d44d30] {
        padding-top: 0;
        padding-bottom: 0
    }

    #app .footer__prettyLink[data-v-62d44d30] {
        display: block;
        padding: 80px 80px 80px 160px;
        background-repeat: no-repeat;
        -webkit-background-size: auto auto;
        -moz-background-size: auto;
        -o-background-size: auto;
        background-size: auto;
        background-position: 45px 80px;
        font-size: 34px;
        line-height: 46px;
        font-weight: 500
    }

    #app .footer__prettyLink.leaf[data-v-62d44d30] {
        background-image: url(/templates/hostfly/img/icons/pretty-link-leaf.svg)
    }

    #app .footer__prettyLink.sea[data-v-62d44d30] {
        background-image: url(/templates/hostfly/img/icons/pretty-link-sea.svg)
    }

    #app .footer__prettyLink[data-v-62d44d30]:nth-child(1) {
        border-right: 1px solid #472774
    }

    #app .footer__prettyLink p[data-v-62d44d30] {
        margin-bottom: 10px
    }

    #app .footer__cards[data-v-62d44d30] {
        padding: 40px 0;
        margin-top: 15px
    }

    #app .footer__cards .footer__card[data-v-62d44d30] {
        margin: 10px
    }

    #app .footer__bottomMenu[data-v-62d44d30] {
        width: 1120px
    }

    #app .footer__bottomMenu__item[data-v-62d44d30] {
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        padding: 0 17px;
        margin-bottom: 17px;
        color: #fff;
        font-weight: 300;
        font-size: 15px;
        line-height: 15px;
        text-transform: uppercase;
        border-left: 1px solid #fff
    }

    #app .footer__bottomMenu__item[data-v-62d44d30]:first-child, #app .footer__bottomMenu__item[data-v-62d44d30]:nth-child(7) {
        border-left: none
    }

    #app .footer__bottomMenu__item a[data-v-62d44d30] {
        color: #fff
    }

    #app .footer__bottomMenu__item a[data-v-62d44d30]:hover {
        border-color: #fff
    }

    #app .footer__socialNetworks[data-v-62d44d30] {
        margin: 25px 0
    }

    #app .footer__socialNetwork[data-v-62d44d30] {
        max-width: 50px;
        max-height: 28px
    }

    #app .footer__socialNetwork .socialNetwork__link[data-v-62d44d30] {
        color: #fff;
        opacity: .5
    }

    #app .footer__socialNetwork .socialNetwork__link[data-v-62d44d30]:hover {
        opacity: 1;
        border-color: transparent
    }

    #app .footer__socialNetwork .socialNetwork__link svg[data-v-62d44d30], #app .footer__socialNetwork .socialNetwork__link svg *[data-v-62d44d30] {
        max-width: 45px;
        max-height: 28px;
        width: 45px;
        height: 45px
    }

    #app .footer__copyright[data-v-62d44d30] {
        text-align: center;
        height: 115px;
        font-size: 14px;
        opacity: .3
    }

    @media screen and (max-width: 959px) {
        #app .pretty-links-column[data-v-62d44d30] {
            border-bottom: none
        }

        #app .footer__prettyLinks[data-v-62d44d30] {
            padding-top: 25px
        }

        #app .footer__prettyLink[data-v-62d44d30] {
            -webkit-box-flex: 0;
            -webkit-flex: 0 0 100%;
            -moz-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            margin-left: 0 !important;
            margin-bottom: 25px;
            border-bottom: 1px solid #472774
        }

        #app .footer__bottomMenu__item[data-v-62d44d30] {
            border-left: none
        }
    }

    @media screen and (max-width: 959px) {
        #app .footer__prettyLink[data-v-62d44d30] {
            font-size: 22px;
            line-height: 1.4;
            padding-left: 80px;
            background-position: 80px 20px;
            padding-top: 100px;
            padding-bottom: 30px
        }
    }

    @media screen and (max-width: 767px) {
        #app .footer__prettyLinks[data-v-62d44d30] {
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap
        }

        #app .footer__prettyLink[data-v-62d44d30] {
            padding-left: 120px;
            padding-top: 10px;
            font-size: 16px;
            background-position: 25px 10px;
            -webkit-background-size: 80px 70px;
            -moz-background-size: 80px 70px;
            -o-background-size: 80px 70px;
            background-size: 80px 70px
        }

        #app .footer__prettyLink a[data-v-62d44d30] {
            color: #fff !important;
            white-space: nowrap
        }

        #app .footer__prettyLink[data-v-62d44d30]:first-child {
            border-right: none
        }
    }

    @media screen and (max-width: 575px) {
        #app .footer__cards[data-v-62d44d30] {
            margin-top: -10px
        }
    }
</style>

{if $templatefile == "viewticket" && !$loggedin}
  <meta name="robots" content="noindex" />
{/if}
