<div class="block-left-nav">
    {*<a href="/cart.php?a=view" class="btn btn-success btn-sm" id="product1-order-button">
        <i class="fas fa-shopping-cart"></i>
        Корзина
    </a>*}
    <div menuitemname="Account" class="panel panel-sidebar">
        <div class="panel-heading">
            <h3 class="panel-title" onmousedown="location.href='/clientarea.php'"><a href="/clientarea.php">Мой аккаунт</a></h3>
        </div>

    </div>
    {foreach $sidebar as $item}
        <div menuItemName="{$item->getName()}"
             class="panel panel-sidebar {if $item->getClass()}{$item->getClass()}{else}panel-sidebar{/if}{if $item->getExtra('mobileSelect') and $item->hasChildren()} hidden-sm hidden-xs{/if}"{if $item->getAttribute('id')} id="{$item->getAttribute('id')}"{/if}>
            <div class="panel-heading">
                <h3 class="panel-title">
                    {if $item->getLabel() == 'Уже зарегистрированы?'}  Зарегистрированы?{else}
                        {if $item->hasIcon()}<i class="{$item->getIcon()}"></i>{/if}
                        {$item->getLabel()} {/if}
                    {if $item->hasBadge()}<span class="badge">{$item->getBadge()}</span>{/if}
                    <i class="fas fa-chevron-up panel-minimise pull-right"></i>
                </h3>
            </div>

            {if $item->hasBodyHtml()}
                <div class="panel-body">
                    {$item->getBodyHtml()}
                </div>
            {/if}
            {if $item->hasChildren()}
                <div class="list-group{if $item->getChildrenAttribute('class')} {$item->getChildrenAttribute('class')}{/if}">
                    {foreach $item->getChildren() as $childItem}
                        {if $childItem->getUri()}
                            <a menuItemName="{$childItem->getName()}" href="{$childItem->getUri()}"
                               class="list-group-item{if $childItem->isDisabled()} disabled{/if}{if $childItem->getClass()} {$childItem->getClass()}{/if}{if $childItem->isCurrent()} active{/if}"{if $childItem->getAttribute('dataToggleTab')} data-toggle="tab"{/if}{if $childItem->getAttribute('target')} target="{$childItem->getAttribute('target')}"{/if}
                               id="{$childItem->getId()}">
                                {if $childItem->hasBadge()}<span class="badge">{$childItem->getBadge()}</span>{/if}
                                {*{if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;{/if}*}
                                {$childItem->getLabel()}
                            </a>
                        {else}
                            <div menuItemName="{$childItem->getName()}"
                                 class="list-group-item{if $childItem->getClass()} {$childItem->getClass()}{/if}"
                                 id="{$childItem->getId()}">
                                {if $childItem->hasBadge()}<span class="badge">{$childItem->getBadge()}</span>{/if}
                                {if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;{/if}
                                {$childItem->getLabel()}
                            </div>
                        {/if}
                    {/foreach}
                </div>
            {/if}
            {if $item->hasFooterHtml()}
                <div class="panel-footer clearfix">
                    {$item->getFooterHtml()}
                </div>
            {/if}
        </div>
        {if $item->getExtra('mobileSelect') and $item->hasChildren()}
            {* Mobile Select only supports dropdown menus *}
            <div class="panel hidden-lg hidden-md {if $item->getClass()}{$item->getClass()}{else}panel-default{/if}"{if $item->getAttribute('id')} id="{$item->getAttribute('id')}"{/if}>
                <div class="panel-heading">
                    <h3 class="panel-title">{$item->getLabel()}
                        {*{if $item->hasIcon()}<i class="{$item->getIcon()}"></i>&nbsp;{/if}*}
                        {if $item->hasBadge()}&nbsp;<span class="badge">{$item->getBadge()}</span>{/if}
                    </h3>
                </div>
                <div class="panel-body">
                    <form role="form">
                        <select class="form-control" onchange="selectChangeNavigate(this)">
                            {foreach $item->getChildren() as $childItem}
                                <option menuItemName="{$childItem->getName()}" value="{$childItem->getUri()}"
                                        class="list-group-item" {if $childItem->isCurrent()}selected="selected"{/if}>
                                    {$childItem->getLabel()}
                                    {if $childItem->hasBadge()}({$childItem->getBadge()}){/if}
                                </option>
                            {/foreach}
                        </select>
                    </form>
                </div>
                {if $item->hasFooterHtml()}
                    <div class="panel-footer">
                        {$item->getFooterHtml()}
                    </div>
                {/if}
            </div>
        {/if}
    {/foreach}
    <div menuitemname="Categories" class="panel panel-sidebar">
        <div class="panel-heading">
            <h3 class="panel-title">
                Услуги
                <i class="fas fa-chevron-up panel-minimise pull-right"></i>
            </h3>
        </div>
        <div class="list-group">
            <a menuitemname="Веб-хостинг" href="/cart.php?gid=2" class="list-group-item"
               id="Secondary_Sidebar-Categories-Веб-хостинг">
                Веб-хостинг
            </a>
            <a menuitemname="Выделенный сервер" href="/cart.php?gid=3" class="list-group-item"
               id="Secondary_Sidebar-Categories-Выделенный_сервер">
                Выделенный сервер
            </a>
            <a menuitemname="Виртуальные сервера" href="/cart.php?gid=4" class="list-group-item"
               id="Secondary_Sidebar-Categories-Виртуальные_сервера">
                Виртуальные сервера
            </a>
        </div>
    </div>
    <div menuitemname="Actions" class="panel panel-sidebar">
        <div class="panel-heading">
            <h3 class="panel-title">
                Действия
                <i class="fas fa-chevron-up panel-minimise pull-right"></i>
            </h3>
        </div>
        <div class="list-group">
            <a menuitemname="Domain Registration" href="/cart.php?a=add&amp;domain=register"
               class="list-group-item" id="Secondary_Sidebar-Actions-Domain_Registration">
                {*<i class="fas fa-globe fa-fw"></i>*}Зарегистрировать домен
            </a>
            <a menuitemname="Domain Renewals" href="/index.php?rp=/cart/domain/renew" class="list-group-item" id="Secondary_Sidebar-Actions-Domain_Renewals">
                Продление домена
            </a>
            <a menuitemname="Domain Transfer" href="/cart.php?a=add&amp;domain=transfer" class="list-group-item"
               id="Secondary_Sidebar-Actions-Domain_Transfer">
                Перенос домена
            </a>
            <a menuitemname="View Cart" href="/cart.php?a=view" class="list-group-item"
               id="Secondary_Sidebar-Actions-View_Cart">
                Просмотр корзины
            </a>
        </div>

    </div>
</div>
