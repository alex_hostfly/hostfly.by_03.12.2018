<ol class="breadcrumb">
    {foreach $breadcrumb as $key => $item}
        {if $key>0 && $item.label!='Регистрация'}
        <li{if $item@last} class="active"{/if}>
            {if !$item@last}<a href="{$item.link}">{/if}
            {$item.label}
            {if !$item@last}</a>{/if}
        </li>
        {/if}
    {/foreach}
</ol>
