{if $captcha}
    <div class="row">
        <div class="col-xs-12">
            {if $filename == 'index'}
            <div class="domainchecker-homepage-captcha">
                {/if}

                {if $captcha == "recaptcha"}
                    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                    <div id="google-recaptcha-domainchecker" class="g-recaptcha"
                         data-sitekey="{$reCaptchaPublicKey}"></div>
                {else}
                    <div class="col-xs-12">
                        <div id="default-captcha-domainchecker"
                             class="{if $filename == 'domainchecker'}input-group input-group-box {/if}">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <input id="inputCaptcha" type="text" name="code" maxlength="5"
                                       class="form-control {if $filename == 'register'}pull-left{/if}"
                                       placeholder="Код с картинки"/>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-3 captchaimage">
                                <div class="inputCaptchaImage-block">
                                    <img id="inputCaptchaImage" src="includes/verifyimage.php" align="middle"/>
                                </div>
                            </div>
                            {*<p>{lang key="captchaverify"}</p>*}
                        </div>
                    </div>
                {/if}

                {if $filename == 'index'}
            </div>
            {/if}
        </div>
    </div>
{/if}
