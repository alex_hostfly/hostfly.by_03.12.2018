<div class="col-xs-12 col-sm-4 col-md-3">
    <div class="block-left-nav">
        <a href="cart.php?a=add&amp;pid=6" class="btn btn-success btn-sm" id="product1-order-button">
            <i class="fas fa-shopping-cart"></i>
            Корзина
        </a>
        <div menuitemname="Categories" class="panel panel-sidebar">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Личный кабинет
                    <i class="fas fa-chevron-up panel-minimise pull-right"></i>
                </h3>
            </div>
            <div class="list-group">
                <a menuitemname="Веб-хостинг" href="/clientarea.php" class="list-group-item active"
                   id="Secondary_Sidebar-Categories-Веб-хостинг">
                    Войти
                </a>
                <a menuitemname="Выделенный сервер" href="/register.php" class="list-group-item"
                   id="Secondary_Sidebar-Categories-Выделенный_сервер">
                    Зарегистрироваться
                </a>
                <a menuitemname="Виртуальные сервера" href="/pwreset.php" class="list-group-item"
                   id="Secondary_Sidebar-Categories-Виртуальные_сервера">
                    Восстановть пароль
                </a>
            </div>
        </div>
        <div menuitemname="Categories" class="panel panel-sidebar">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Услуги
                    <i class="fas fa-chevron-up panel-minimise pull-right"></i>
                </h3>
            </div>
            <div class="list-group">
                <a menuitemname="Веб-хостинг" href="/cart.php?gid=2" class="list-group-item"
                   id="Secondary_Sidebar-Categories-Веб-хостинг">
                    Веб-хостинг
                </a>
                <a menuitemname="Выделенный сервер" href="/cart.php?gid=3" class="list-group-item"
                   id="Secondary_Sidebar-Categories-Выделенный_сервер">
                    Выделенный сервер
                </a>
                <a menuitemname="Виртуальные сервера" href="/cart.php?gid=4" class="list-group-item"
                   id="Secondary_Sidebar-Categories-Виртуальные_сервера">
                    Виртуальные сервера
                </a>
            </div>
        </div>
        <div menuitemname="Actions" class="panel panel-sidebar">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Домен
                    <i class="fas fa-chevron-up panel-minimise pull-right"></i>
                </h3>
            </div>
            <div class="list-group">
                <a menuitemname="Domain Registration" href="/cart.php?a=add&amp;domain=register"
                   class="list-group-item" id="Secondary_Sidebar-Actions-Domain_Registration">
                    <i class="fas fa-globe fa-fw"></i>
                    Зарегистрировать домен
                </a>
                <a menuitemname="Domain Transfer" href="/cart.php?a=add&amp;domain=transfer" class="list-group-item"
                   id="Secondary_Sidebar-Actions-Domain_Transfer">
                    Перенос домена
                </a>
                <a menuitemname="View Cart" href="/cart.php?a=view" class="list-group-item"
                   id="Secondary_Sidebar-Actions-View_Cart">
                    Просмотр корзины
                </a>
            </div>

        </div>
    </div>
</div>

<div class="col-xs-12 col-sm-8 col-md-8 login-block-form">
    {include file="$template/includes/pageheader.tpl" title=$LANG.login desc="{$LANG.restrictedpage}"}

    {if $incorrect}
        {include file="$template/includes/alert.tpl" type="error" msg=$LANG.loginincorrect textcenter=true}
    {elseif $verificationId && empty($transientDataName)}
        {include file="$template/includes/alert.tpl" type="error" msg=$LANG.verificationKeyExpired textcenter=true}
    {elseif $ssoredirect}
        {include file="$template/includes/alert.tpl" type="info" msg=$LANG.sso.redirectafterlogin textcenter=true}
    {/if}

    <h1 class="login-h1">Войти в личный кабинет</h1>
    <div data-v-0e0cbbc2="" class="special-offer login-block">
        <div data-v-0e0cbbc2="" class="layout">
            <div data-v-0e0cbbc2="" class="flex special-offer__box mix-gradient xs12">
                <div data-v-0e0cbbc2="" class="inner just-num-46">
                    <div data-v-0e0cbbc2="" class="text">
                        <div class="logincontainer{if $linkableProviders} with-social{/if}">

                            <div class="providerLinkingFeedback"></div>

                            <div class="row">
                                <div class="col-sm-{if $linkableProviders}7{else}12{/if}">

                                    <form method="post" action="{$systemurl}dologin.php" class="login-form" role="form">
                                        <div class="form-group">
                                            {*<label for="inputEmail">{$LANG.clientareaemail}</label>*}
                                            <label for="username" class="field-icon">
                                                <i class="fas fa-user"></i>
                                            </label>
                                            <input type="email" name="username" class="form-control" id="inputEmail"
                                                   placeholder="{$LANG.enteremail}" autofocus>
                                        </div>

                                        <div class="form-group mb-0">
                                            {*<label for="inputPassword">{$LANG.clientareapassword}</label>*}
                                            <label for="username" class="field-icon">
                                                <i class="fas fa-lock"></i>
                                            </label>
                                            <input type="password" name="password" class="form-control" id="inputPassword"
                                                   placeholder="{$LANG.clientareapassword}" autocomplete="off">
                                        </div>
                                        <a href="pwreset.php" class="fogot-pass">{$LANG.forgotpw}</a>

                                        {*<div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="rememberme"/> {$LANG.loginrememberme}
                                            </label>
                                        </div>*}

                                        <div class="container-btn-login-form">
                                                <button id="login" type="submit" class="btn btn-primary">Войти</button>
                                                <div class="text-remember-registreted">
                                                    Нет аккаунта?
                                                    <a href="/register.php">Зарегистрируйтесь</a>
                                                </div>
                                        </div>
                                    </form>

                                </div>
                                <div class="col-sm-5{if !$linkableProviders} hidden{/if}">
                                    {include file="$template/includes/linkedaccounts.tpl" linkContext="login" customFeedback=true}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>