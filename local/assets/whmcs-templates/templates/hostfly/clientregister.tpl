{if in_array('state', $optionalFields)}
    <script>
      var statesTab = 10
      var stateNotRequired = true
    </script>
{/if}

<script type="text/javascript" src="{$BASE_PATH_JS}/StatesDropdown.js"></script>
<script type="text/javascript" src="{$BASE_PATH_JS}/PasswordStrength.js"></script>
<script>
  window.langPasswordStrength = "{$LANG.pwstrength}"
  window.langPasswordWeak = "{$LANG.pwstrengthweak}"
  window.langPasswordModerate = "{$LANG.pwstrengthmoderate}"
  window.langPasswordStrong = "{$LANG.pwstrengthstrong}"
  jQuery(document).ready(function () {
    jQuery('#inputNewPassword1').keyup(registerFormPasswordStrengthFeedback)
  })
</script>
{if $registrationDisabled}
    {include file="$template/includes/alert.tpl" type="error" msg=$LANG.registerCreateAccount|cat:' <strong><a href="cart.php" class="alert-link">'|cat:$LANG.registerCreateAccountOrder|cat:'</a></strong>'}
{/if}

{if $errormessage}
    {include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
{/if}

{if !$registrationDisabled}
    <div class="row">
        <div class="col-xs-12 mix-gradient" style="padding: 0;padding-left: 6px; height: 38px; margin-bottom: -2px;">
            <div class="tab_face">
                <a href="FIZ" class="active btn btn-success btn-sm">Физ. лицо</a>
                <a href="UR" class="btn btn-success btn-sm">Юр. лицо</a>
                <a href="IP" class="btn btn-success btn-sm">ИП</a>
            </div>
        </div>
    </div>
    <script>
      $(document).ready(function () {
        var type_face = 'FIZ'
        $('input[name=dzen], input[name=residentRB]').change(function () {
          isHidePasportData()
        })

        function isHidePasportData (face) {
          var is = $('input[name=dzen]').is(':checked')
          $('.bank_data').each(function () {
            (is && (type_face == 'UR' || type_face == 'IP')) ? $(this).show() : $(this).hide()
          })
          var isRez = $('input[name=residentRB]').is(':checked')
          $('.pasport_data').each(function () {
            (isRez && (type_face == 'FIZ' || type_face == 'IP')) ? $(this).show() : $(this).hide()
          })
          $('.svidetelstvo').each(function () {
            (isRez && (type_face == 'UR' || type_face == 'IP')) ? $(this).show() : $(this).hide()
          })
        }

        function FilterByAttr (attr) {
          $('.filterInput').each(function () {
            var input = $(this).find('input')
            input.removeAttr('required')
            switch (attr) {
              case 'FIZ' :
                ($(this).data('fiz')) ? $(this).show().find('input:not(.noRequired)').attr('required', true) : $(this).hide()
                break
              case 'UR' :
                ($(this).data('ur')) ? $(this).show().find('input:not(.noRequired)').attr('required', true) : $(this).hide()
                break
              case 'IP' :
                ($(this).data('ip')) ? $(this).show().find('input:not(.noRequired)').attr('required', true) : $(this).hide()
                break
              default:
                $(this).hide()
            }

          })
        }

        $('.tab_face').on('click', 'a', function (e) {
          e.preventDefault()
          $('.tab_face a').removeClass('active')
          $(this).addClass('active')
          type_face = $(this).attr('href').toString()
          FilterByAttr($(this).attr('href').toString())
          localStorage.type_face = type_face
          isHidePasportData()
        })
        if (localStorage.type_face != undefined) {
          $('.tab_face a[href=' + localStorage.type_face.toString() + ']').trigger('click')
        } else {
          FilterByAttr('FIZ')
        }
      })
    </script>
    <div data-v-0e0cbbc2="" class="special-offer registration-block">
        <div data-v-0e0cbbc2="" class="layout">
            <div data-v-0e0cbbc2="" class="flex special-offer__box mix-gradient xs12">
                <div data-v-0e0cbbc2="" class="inner just-num-46">
                    <div data-v-0e0cbbc2="" class="text">
                        <div id="registration">
                            <form method="post" class="using-password-strength" action="{$smarty.server.PHP_SELF}"
                                  role="form" name="orderfrm" id="frmCheckout">
                                <input type="hidden" name="register" value="true"/>

                                <div id="containerNewUserSignup">
                                    <div class="row" style="padding: 0 15px;">
                                        <div class="col-xs-12" style="padding: 0 15px;">
                                            <div>
                                                <span style="color: #333333;font-size: 20px;font-weight: 500;line-height: 28px;">
                                                    Резидент РБ?
                                                </span>
                                                <br>
                                                <div class="tg-list-item">
                                                    <span class="text-tg-check">нет</span>
                                                    <input type="checkbox" name="residentRB" id="cb2"
                                                           class="tgl tgl-ios" checked>
                                                    <label for="cb2" class="tgl-btn"></label>
                                                    <span class="text-tg-check">да</span>
                                                </div>
                                            </div>
                                        </div>
                                        {include file="$template/includes/linkedaccounts.tpl" linkContext="registration"}

                                        <div class="sub-heading ">
                                            <span>{$LANG.orderForm.personalInformation}</span>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 filterInput" data-fiz=true data-ur=false data-ip=true>

                                                <div class="form-group prepend-icon">
                                                    <label for="inputFirstName" class="field-icon">
                                                        <i class="fas fa-user"></i>
                                                    </label>
                                                    <input type="text" name="firstname" id="inputFirstName"
                                                           class="field form-control"
                                                           placeholder="{$LANG.orderForm.firstName}"
                                                           value="{$clientfirstname}"
                                                           {if !in_array('firstname', $optionalFields)}required{/if}
                                                           autofocus>
                                                </div>

                                            </div>
                                            <div class="col-xs-6 filterInput" data-fiz=true data-ur=false data-ip=true>

                                                <div class="form-group prepend-icon">
                                                    <label for="inputLastName" class="field-icon">
                                                        <i class="fas fa-user"></i>
                                                    </label>
                                                    <input type="text" name="lastname" id="inputLastName"
                                                           class="field form-control"
                                                           placeholder="{$LANG.orderForm.lastName}"
                                                           value="{$clientlastname}"
                                                           {if !in_array('lastname', $optionalFields)}required{/if}>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 filterInput" data-fiz=true data-ur=true data-ip=true>
                                                <div class="form-group prepend-icon">
                                                    <label for="inputEmail" class="field-icon">
                                                        <i class="fas fa-envelope"></i>
                                                    </label>
                                                    <input type="email" name="email" id="inputEmail"
                                                           class="field form-control"
                                                           placeholder="{$LANG.orderForm.emailAddress}"
                                                           value="{$clientemail}">
                                                </div>
                                            </div>
                                            <div class="col-xs-6 filterInput" data-fiz=true data-ur=true data-ip=true>
                                                <div class="form-group prepend-icon">
                                                    <label for="inputPhone" class="field-icon">
                                                        <i class="fas fa-phone"></i>
                                                    </label>
                                                    <input type="tel" name="phonenumber" id="inputPhone"
                                                           class="field"
                                                           placeholder="{$LANG.orderForm.phoneNumber}"
                                                           value="{$clientphonenumber}">

                                                </div>
                                            </div>
                                        </div>

                                        <div class="sub-heading hidden">
                                            <span>{$LANG.orderForm.billingAddress}</span>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 filterInput" data-fiz=true data-ur=false
                                                 data-ip=false>

                                                <div class="form-group prepend-icon">
                                                    <label for="inputCompanyName" class="field-icon">
                                                        <i class="fas fa-building"></i>
                                                    </label>
                                                    <input type="text" name="companyname" id="inputCompanyName"
                                                           class="field noRequired"
                                                           placeholder="{$LANG.orderForm.companyName} ({$LANG.orderForm.optional})"
                                                           value="{$clientcompanyname}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 filterInput" data-fiz=true data-ur=false
                                                 data-ip=false>
                                                <div class="form-group prepend-icon">
                                                    <label for="inputAddress1" class="field-icon">
                                                        <i class="far fa-building"></i>
                                                    </label>
                                                    <input type="text" name="address1" id="inputAddress1"
                                                           class="field form-control"
                                                           placeholder="{$LANG.orderForm.streetAddress}"
                                                           value="{$clientaddress1}"
                                                           {if !in_array('address1', $optionalFields)}required{/if}>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 filterInput" data-fiz=true data-ur=false
                                                 data-ip=false>
                                                <div class="form-group prepend-icon">
                                                    <label for="inputAddress2" class="field-icon">
                                                        <i class="fas fa-map-marker-alt"></i>
                                                    </label>
                                                    <input type="text" name="address2" id="inputAddress2"
                                                           class="field"
                                                           placeholder="{$LANG.orderForm.streetAddress2}"
                                                           value="{$clientaddress2}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 filterInput" data-fiz=true data-ur=false
                                                 data-ip=false>
                                                <div class="form-group prepend-icon">
                                                    <label for="inputCity" class="field-icon">
                                                        <i class="far fa-building"></i>
                                                    </label>
                                                    <input type="text" name="city" id="inputCity"
                                                           class="field form-control"
                                                           placeholder="{$LANG.orderForm.city}"
                                                           value="{$clientcity}"
                                                           {if !in_array('city', $optionalFields)}required{/if}>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 filterInput" data-fiz=true data-ur=false
                                                 data-ip=false>
                                                <div class="form-group prepend-icon">
                                                    <label for="state" class="field-icon" id="inputStateIcon">
                                                        <i class="fas fa-map-signs"></i>
                                                    </label>
                                                    <label for="stateinput" class="field-icon" id="inputStateIcon">
                                                        <i class="fas fa-map-signs"></i>
                                                    </label>
                                                    <input type="text" name="state" id="state"
                                                           class="field form-control"
                                                           placeholder="{$LANG.orderForm.state}"
                                                           value="{$clientstate}"
                                                           {if !in_array('state', $optionalFields)}required{/if}>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 filterInput" data-fiz=true data-ur=false
                                                 data-ip=false>
                                                <div class="form-group prepend-icon">
                                                    <label for="inputPostcode" class="field-icon">
                                                        <i class="fas fa-certificate"></i>
                                                    </label>
                                                    <input type="text" name="postcode" id="inputPostcode"
                                                           class="field form-control"
                                                           placeholder="{$LANG.orderForm.postcode}"
                                                           value="{$clientpostcode}"
                                                           {if !in_array('postcode', $optionalFields)}required{/if}>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 filterInput" data-fiz=true data-ur=false
                                                 data-ip=true>

                                                <div class="form-group prepend-icon">
                                                    <label for="inputCountry" class="field-icon"
                                                           id="inputCountryIcon">
                                                        <i class="fas fa-globe"></i>
                                                    </label>
                                                    <select name="country" id="inputCountry"
                                                            class="field form-control">
                                                        {foreach $clientcountries as $countryCode => $countryName}
                                                            <option value="{$countryCode}"{if (!$clientcountry && $countryCode eq $defaultCountry) || ($countryCode eq $clientcountry)} selected="selected"{/if}>
                                                                {$countryName}
                                                            </option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {if $customfields || $currencies}
                                        {*<div class="sub-heading">
                                            <span>{$LANG.orderadditionalrequiredinfo}</span>
                                        </div>*}
                                        <div class="row">
                                            {if $customfields}
                                                <script>
                                                  var data_field = [
                                                      {foreach $customfields as $customfield}
                                                    {
                                                      name: '{$customfield.name}',
                                                      description: '{$customfield.description}'
                                                    },
                                                      {/foreach}
                                                  ]
                                                </script>
                                                <div class="">
                                                    {foreach $customfields as  $key => $customfield}
                                                        {if $customfields[$key-1].name != $customfield.name}
                                                            <div class="row" style="padding: 0 15px;">
                                                        {/if}
                                                        {if $customfields[$key-1].name != $customfield.name && $customfield.name != 'Банковские реквизиты:'}
                                                            <div class="col-xs-12 filterInput {if $customfield.name == 'Банковские реквизиты:'}bank_data{/if}
                                                                 {if $customfield.name == 'Свидетельство о регистрации юр. лица (ЕГР): '} svidetelstvo{/if}"
                                                                 data-fiz={if $customfield.name == 'Дата рождения'
                                                                 || $customfield.name =='Паспортные данные:'}true{else}false{/if}
                                                                 data-ur={if $customfield.name == 'Банковские реквизиты:'
                                                                 || $customfield.name == 'Уполномоченное лицо на заключение договора'
                                                                 || $customfield.name == 'Реквизиты юридического лица'
                                                                 || $customfield.name == 'Юридический адрес:'
                                                                 || $customfield.name == 'Свидетельство о регистрации юр. лица (ЕГР): '
                                                                 || $customfield.name == 'Почтовый адрес'
                                                                 }true{else}false{/if}
                                                                 data-ip={if $customfield.name == 'Банковские реквизиты:'
                                                                 || $customfield.name == 'Юридический адрес:'
                                                                 || $customfield.name == 'Почтовый адрес'
                                                                 || $customfield.name == 'Паспортные данные:'
                                                                 || $customfield.name == 'Свидетельство о регистрации юр. лица (ЕГР): '
                                                                 }true{else}false{/if}>
                                                                <div class="sub-heading ">
                                                                    <span>{$customfield.name}</span>
                                                                </div>
                                                            </div>
                                                        {/if}
                                                        {if $customfields[$key-1].name != $customfield.name && $customfield.name == 'Банковские реквизиты:'}
                                                            <div class="col-xs-12">
                                                                <div class="filterInput" data-fiz=false data-ur=true
                                                                     data-ip=true"
                                                                     style="display: none;">
                                                                          <span style="color: #333333;
    font-size: 20px;
    font-weight: 500;
    line-height: 28px;
    top: 0;
    padding-top: 30px;
    display: block;
    padding-left: 15px;">{$customfield.name}</span>
                                                                    <div class="col-xs-12">
                                                                        <span style="font-size: 16px;">Есть ли у Вас расчетный счет в белорусском банке?</span>
                                                                        <br>
                                                                        <div class="tg-list-item">
                                                                            <span class="text-tg-check">нет</span>
                                                                            <input type="checkbox" name="dzen" id="dzen"
                                                                                   class="tgl tgl-ios" checked>
                                                                            <label for="dzen" class="tgl-btn"></label>
                                                                            <span class="text-tg-check">да</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        {/if}
                                                        <div class="col-xs-6">
                                                            <div class="filterInput
                                                            {if $customfield.description == 'Факс' ||
                                                                $customfield.description == 'Факс'
                                                            } noRequired {/if}
                                                             {if $customfield.name == 'Банковские реквизиты:'}bank_data{/if}
                                                                                    {if $customfield.description == 'Орган, который выдал'
                                                            || $customfield.description == 'Личный номер'
                                                            || $customfield.description == 'Дата выдачи'
                                                            }pasport_data {/if} {if $customfield.name == 'Свидетельство о регистрации юр. лица (ЕГР): '} svidetelstvo{/if}"
                                                                 data-fiz={if $customfield.name == 'Дата рождения'
                                                                 || $customfield.name =='Паспортные данные:'}true{else}false{/if}
                                                                 data-ur={if $customfield.name == 'Банковские реквизиты:'
                                                                 || $customfield.name == 'Уполномоченное лицо на заключение договора'
                                                                 || $customfield.name == 'Реквизиты юридического лица'
                                                                 || $customfield.name == 'Юридический адрес:'
                                                                 || $customfield.name == 'Свидетельство о регистрации юр. лица (ЕГР): '
                                                                 || $customfield.name == 'Почтовый адрес'
                                                                 }true{else}false{/if}
                                                                 data-ip={if $customfield.name == 'Банковские реквизиты:'
                                                                 || $customfield.name == 'Юридический адрес:'
                                                                 || $customfield.name == 'Почтовый адрес'
                                                                 || $customfield.name == 'Паспортные данные:'
                                                                 || $customfield.name == 'Свидетельство о регистрации юр. лица (ЕГР): '
                                                                 }true{else}false{/if}>

                                                                <div class="form-group">
                                                                    {*<label for="customfield{$customfield.id}">{$customfield.name}</label>*}
                                                                    <div class="control placeholderCheck">
                                                                        {$customfield.input}
                                                                        {if $customfield.description}
                                                                            <span class="field-help-text d-none">{$customfield.description}</span>
                                                                        {/if}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {if $customfields[$key+1].name != $customfield.name}
                                                            </div>
                                                        {/if}
                                                    {/foreach}
                                                </div>
                                            {/if}
                                            {if $customfields && count($customfields)%2 > 0 }
                                                <div class="clearfix"></div>
                                            {/if}
                                            {if $currencies}
                                                <div class="col-sm-6">
                                                    <div class="form-group prepend-icon">
                                                        <label for="inputCurrency" class="field-icon">
                                                            <i class="far fa-money-bill-alt"></i>
                                                        </label>
                                                        <select id="inputCurrency" name="currency"
                                                                class="field form-control">
                                                            {foreach from=$currencies item=curr}
                                                                <option value="{$curr.id}"{if !$smarty.post.currency && $curr.default || $smarty.post.currency eq $curr.id } selected{/if}>{$curr.code}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    {/if}
                                    <div id="containerNewUserSecurity" {if $remote_auth_prelinked && !$securityquestions } class="hidden"{/if}>

                                        <div class="sub-heading" style="padding-left: 17px;">
                                            <span class="px-0">{$LANG.orderForm.accountSecurity}</span>
                                        </div>
                                        <div id="containerPassword"
                                             class="row{if $remote_auth_prelinked && $securityquestions} hidden{/if}">
                                            <div id="passwdFeedback" style="display: none;"
                                                 class="alert alert-info text-center col-sm-12"></div>
                                            <div class="col-xs-12 px-0">
                                                <div class="col-sm-6">
                                                    <div class="form-group prepend-icon">
                                                        <label for="inputNewPassword1" class="field-icon">
                                                            <i class="fas fa-lock"></i>
                                                        </label>
                                                        <input type="password" name="password" id="inputNewPassword1"
                                                               data-error-threshold="{$pwStrengthErrorThreshold}"
                                                               data-warning-threshold="{$pwStrengthWarningThreshold}"
                                                               class="field" placeholder="{$LANG.clientareapassword}"
                                                               autocomplete="off"{if $remote_auth_prelinked} value="{$password}"{/if}>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 px-0">
                                                <div class="col-sm-6">
                                                    <div class="form-group prepend-icon">
                                                        <label for="inputNewPassword2" class="field-icon">
                                                            <i class="fas fa-lock"></i>
                                                        </label>
                                                        <input type="password" name="password2" id="inputNewPassword2"
                                                               class="field"
                                                               placeholder="{$LANG.clientareaconfirmpassword}"
                                                               autocomplete="off"{if $remote_auth_prelinked} value="{$password}"{/if}>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 px-0">
                                                <div class="col-sm-6">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success progress-bar-striped"
                                                             role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                                             aria-valuemax="100" id="passwordStrengthMeterBar">
                                                        </div>
                                                    </div>
                                                    <p class="small text-muted"
                                                       id="passwordStrengthTextLabel">{$LANG.pwstrength}
                                                        : {$LANG.pwstrengthenter}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {if $securityquestions}
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <select name="securityqid" id="inputSecurityQId"
                                                            class="field form-control">
                                                        <option value="">{$LANG.clientareasecurityquestion}</option>
                                                        {foreach $securityquestions as $question}
                                                            <option value="{$question.id}"{if $question.id eq $securityqid} selected{/if}>
                                                                {$question.question}
                                                            </option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group prepend-icon">
                                                        <label for="inputSecurityQAns" class="field-icon">
                                                            <i class="fas fa-lock"></i>
                                                        </label>
                                                        <input type="password" name="securityqans"
                                                               id="inputSecurityQAns"
                                                               class="field form-control"
                                                               placeholder="{$LANG.clientareasecurityanswer}"
                                                               autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                    </div>
                                    {include file="$template/includes/captcha.tpl"}
                                    <div class="row">
                                        <div class="col-xs-12">
                                            {if $showMarketingEmailOptIn}
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="marketing-email-optin">
                                                        {*<h4>{lang key='emailMarketing.joinOurMailingList'}</h4>
                                                        <p>{$marketingEmailOptInMessage}</p>*}
                                                        <div class="checkbox-style">
                                                            <input type="checkbox" value="None" id="checkboxEmail"
                                                                   name="marketingoptin"
                                                                   value="1"{if $marketingEmailOptIn} checked{/if}
                                                                   class="checkbox no-icheck" data-size="small"
                                                                   data-on-text="{lang key='yes'}"
                                                                   data-off-text="{lang key='no'}"/>
                                                            <label for="checkboxEmail"></label>
                                                        </div>
                                                        <p>Подписаться на рассылку новостей и специальных
                                                            предложений</p>
                                                    </div>
                                                </div>
                                            {/if}
                                        </div>
                                    </div>


                                    <br/>
                                    {if $accepttos}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel panel-danger tospanel">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title"><span
                                                                    class="fas fa-exclamation-triangle tosicon"></span>
                                                            &nbsp; {$LANG.ordertos}</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="col-md-12">
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="accepttos"
                                                                       class="accepttos">
                                                                {$LANG.ordertosagreement} <a href="{$tosurl}"
                                                                                             target="_blank">{$LANG.ordertos}</a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {/if}
                                    <div class="col-xs-12">
                                        <div id="checkCompany">
                                            <input class="btn-large btn-primary btn-registreted-form" type="submit"
                                                   value="Зарегистрироваться{*{$LANG.clientregistertitle}*}"/>
                                        </div>
                                    </div>
                                    <script>
                                      $(document).ready(function () {
                                        if (!$('#inputCompanyName').val() == ' ') {
                                          $('#inputCompanyName').val('')
                                        }
                                        $('#checkCompany').on('click', function (e) {
                                          if (!$('#inputCompanyName').val()) {
                                            e.preventDefault()
                                            e.stopPropagation()
                                            $('#inputCompanyName').val(' ')
                                            console.log($('#inputCompanyName').val(' '))
                                            $('input[type="submit"].btn-registreted-form').trigger('click')
                                          }
                                        })
                                        $('.placeholderCheck input').each(function () {
                                          $(this).attr('placeholder', $(this).siblings('.field-help-text').text())
                                        })
                                      })
                                    </script>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}
