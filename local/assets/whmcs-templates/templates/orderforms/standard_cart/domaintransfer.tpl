{include file="orderforms/standard_cart/common.tpl"}

<div id="order-standard_cart">

    <div class="row">

        <div class="pull-md-right col-md-9">

            <div class="header-lined">
                <h1>
                    {$LANG.transferdomain}
                </h1>
            </div>

        </div>

        <div class="col-md-3 pull-md-left sidebar hidden-xs hidden-sm">

            {include file="orderforms/standard_cart/sidebar-categories.tpl"}

        </div>

        <div class="col-md-9 pull-md-right">

            {include file="orderforms/standard_cart/sidebar-categories-collapsed.tpl"}

            <div class="">
                <p>{lang key='orderForm.transferExtend'}*</p>
            </div>
            <br />

            <form method="post" action="cart.php" id="frmDomainTransfer">
                <input type="hidden" name="a" value="addDomainTransfer">

                <div class="row">
                    <div class="special-offer">
                        <div class="panel panel-default special-offer__box mix-gradient">
                            <div class="inner">
                                <div class="">
                                    <h2 class="panel-title">{lang key='orderForm.singleTransfer'}</h2>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="inputTransferDomain">{lang key='domainname'}</label>
                                            <input type="text" class="form-control" name="domain" id="inputTransferDomain" value="{$lookupTerm}" placeholder="{lang key='yourdomainplaceholder'}.{lang key='yourtldplaceholder'}" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="{lang key='orderForm.enterDomain'}" />
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="inputAuthCode" style="width:100%;">
                                                {lang key='orderForm.authCode'}
                                                <a href="" data-toggle="tooltip" data-placement="left" title="{lang key='orderForm.authCodeTooltip'}" class="pull-right"><i class="fas fa-question-circle"></i> {lang key='orderForm.help'}</a>
                                            </label>
                                            <input type="text" class="form-control" name="epp" id="inputAuthCode" placeholder="{lang key='orderForm.authCodePlaceholder'}" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="{lang key='orderForm.required'}" />
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <div id="transferUnavailable" class="alert alert-warning slim-alert text-center hidden"></div>
                                    {if $captcha}
                                        <div class="captcha-container" id="captchaContainer">
                                            {if $captcha eq "recaptcha"}
                                                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                                                <div id="google-recaptcha" class="g-recaptcha recaptcha-transfer center-block" data-sitekey="{$reCaptchaPublicKey}" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="{lang key='orderForm.required'}" ></div>
                                            {else}
                                                <div class="">
                                                    <p>{lang key="cartSimpleCaptcha"}</p>
                                                    <div class="d-inline-block">
                                                        <img id="inputCaptchaImage" src="includes/verifyimage.php" />
                                                        <input id="inputCaptcha" type="text" name="code" maxlength="5" class="form-control input-sm" data-toggle="tooltip" data-placement="right" data-trigger="manual" title="{lang key='orderForm.required'}" />
                                                    </div>
                                                </div>
                                            {/if}
                                        </div>
                                    {/if}
                                </div>
                                <div class=" text-right">
                                    <button type="submit" id="btnTransferDomain" class="btn btn-primary btn-transfer">
                                    <span class="loader hidden" id="addTransferLoader">
                                        <i class="fas fa-fw fa-spinner fa-spin"></i>
                                    </span>
                                        <span id="addToCart">{lang key="orderForm.addToCart"}</span>
                                    </button>
                                </div>
                            </div>
                            <!-- /.inner -->

                        </div>
                    </div>
                </div>

            </form>

            <p class=" small">* {lang key='orderForm.extendExclusions'}</p>

        </div>
    </div>
</div>
