(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Benefits"],{

/***/ "./local/assets/source/vue/components/mainPage/Benefits/component.vue":
/*!****************************************************************************!*\
  !*** ./local/assets/source/vue/components/mainPage/Benefits/component.vue ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !babel-loader!./script.js */ "./node_modules/babel-loader/lib/index.js!./local/assets/source/vue/components/mainPage/Benefits/script.js");
/* harmony import */ var _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_template_compiler_index_id_data_v_41e417db_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../../../../../node_modules/vue-loader/lib/template-compiler/index?{"id":"data-v-41e417db","hasScoped":true,"optionsId":"0","buble":{"transforms":{}}}!../../../../../../../node_modules/vue-loader/lib/selector?type=template&index=0!./component.vue */ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-41e417db\",\"hasScoped\":true,\"optionsId\":\"0\",\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/mainPage/Benefits/component.vue");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_component_normalizer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/component-normalizer */ "./node_modules/vue-loader/lib/runtime/component-normalizer.js");
var disposed = false
function injectStyle (context) {
  if (disposed) return
  __webpack_require__(/*! !vue-style-loader!css-loader?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler/index?{"optionsId":"0","vue":true,"id":"data-v-41e417db","scoped":true,"sourceMap":true}!sass-loader!./styles.scss */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-41e417db\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/mainPage/Benefits/styles.scss")
}
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-41e417db"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null

var Component = Object(_node_modules_vue_loader_lib_runtime_component_normalizer__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0___default.a,
  _node_modules_vue_loader_lib_template_compiler_index_id_data_v_41e417db_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__["render"],
  _node_modules_vue_loader_lib_template_compiler_index_id_data_v_41e417db_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__["staticRenderFns"],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "local/assets/source/vue/components/mainPage/Benefits/component.vue"

/* hot reload */
if (false) {}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./local/assets/source/vue/components/mainPage/Benefits/script.js":
/*!********************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./local/assets/source/vue/components/mainPage/Benefits/script.js ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _BaseComponent = __webpack_require__(/*! ../../../BaseComponent */ "./local/assets/source/vue/BaseComponent.js");

var _BaseComponent2 = _interopRequireDefault(_BaseComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'Benefits',
  extends: _BaseComponent2.default,
  data: function data() {
    return {
      title: null,
      benefits: null,
      button: null,
      fitsClass: null
    };
  }
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-41e417db\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/mainPage/Benefits/styles.scss":
/*!***************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader?sourceMap!./node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-41e417db","scoped":true,"sourceMap":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/mainPage/Benefits/styles.scss ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "\n#app .benefis[data-v-41e417db]{padding:80px 0 0;background:rgba(31,18,80,.8);-webkit-border-radius:16px;-moz-border-radius:16px;border-radius:16px;margin-bottom:80px\n}\n#app .benefis h2[data-v-41e417db]{max-width:600px;margin-bottom:35px;text-align:center\n}\n#app .benefis h3[data-v-41e417db]{font-size:20px;margin:15px 0\n}\n#app .benefis .row[data-v-41e417db]{width:100%\n}\n#app .benefis .cards-container[data-v-41e417db]{max-width:100%\n}\n#app .benefis .benefis-card[data-v-41e417db]{text-align:center;padding:25px;-webkit-flex-basis:25%;-ms-flex-preferred-size:25%;flex-basis:25%;max-width:25%;min-width:280px\n}\n#app .benefis .benefis-card .image[data-v-41e417db]{width:100%;height:100px\n}\n#app .benefis .benefis-card .image img[data-v-41e417db]{max-width:100px\n}\n#app .benefis .benefis-card .description[data-v-41e417db]{font-size:14px;line-height:24px\n}\n#app .benefis .btn[data-v-41e417db],#app .benefis .v-btn[data-v-41e417db]{margin-top:30px;background:#eb276d;color:#fff\n}\n#app .site-constructor h2[data-v-41e417db]{max-width:800px\n}\n@media screen and (max-width:767px){\n#app .benefis .benefis-card[data-v-41e417db]{width:100%;max-width:100%;-webkit-box-flex:0;-webkit-flex:0 0 100%;-moz-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%\n}\n}", "", {"version":3,"sources":["/home/hostfly/public_html/local/assets/source/vue/components/mainPage/Benefits/<input css 22>"],"names":[],"mappings":";AAAA,+BAAc,iBAAiB,6BAA6B,2BAA2B,wBAAwB,mBAAmB,kBAAkB;CAAC;AAAA,kCAAiB,gBAAgB,mBAAmB,iBAAiB;CAAC;AAAA,kCAAiB,eAAe,aAAa;CAAC;AAAA,oCAAmB,UAAU;CAAC;AAAA,gDAA+B,cAAc;CAAC;AAAA,6CAA4B,kBAAkB,aAAa,uBAAuB,4BAA4B,eAAe,cAAc,eAAe;CAAC;AAAA,oDAAmC,WAAW,YAAY;CAAC;AAAA,wDAAuC,eAAe;CAAC;AAAA,0DAAyC,eAAe,gBAAgB;CAAC;AAAA,0EAAwC,gBAAgB,mBAAmB,UAAU;CAAC;AAAA,2CAA0B,eAAe;CAAC;AAAA;AAAoC,6CAA4B,WAAW,eAAe,mBAAmB,sBAAsB,gBAAgB,kBAAkB,aAAa;CAAC;CAAC","file":"styles.scss","sourcesContent":["#app .benefis{padding:80px 0 0;background:rgba(31,18,80,.8);-webkit-border-radius:16px;-moz-border-radius:16px;border-radius:16px;margin-bottom:80px}#app .benefis h2{max-width:600px;margin-bottom:35px;text-align:center}#app .benefis h3{font-size:20px;margin:15px 0}#app .benefis .row{width:100%}#app .benefis .cards-container{max-width:100%}#app .benefis .benefis-card{text-align:center;padding:25px;-webkit-flex-basis:25%;-ms-flex-preferred-size:25%;flex-basis:25%;max-width:25%;min-width:280px}#app .benefis .benefis-card .image{width:100%;height:100px}#app .benefis .benefis-card .image img{max-width:100px}#app .benefis .benefis-card .description{font-size:14px;line-height:24px}#app .benefis .btn,#app .benefis .v-btn{margin-top:30px;background:#eb276d;color:#fff}#app .site-constructor h2{max-width:800px}@media screen and (max-width:767px){#app .benefis .benefis-card{width:100%;max-width:100%;-webkit-box-flex:0;-webkit-flex:0 0 100%;-moz-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%}}"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-41e417db\",\"hasScoped\":true,\"optionsId\":\"0\",\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/mainPage/Benefits/component.vue":
/*!********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-41e417db","hasScoped":true,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/mainPage/Benefits/component.vue ***!
  \********************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "benefis page-content just-num-5 column",
      class: _vm.fitsClass
    },
    [
      _c("h2", [_vm._v(_vm._s(_vm.title))]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "just-num-8 just-wrap cards-container" },
        _vm._l(_vm.benefits, function(benefit) {
          return _c("div", { staticClass: "col benefis-card" }, [
            _c("div", { staticClass: "image just-num-5" }, [
              _c("img", { attrs: { src: benefit.imageUrl, alt: "" } })
            ]),
            _vm._v(" "),
            _c("h3", { domProps: { innerHTML: _vm._s(benefit.title) } }),
            _vm._v(" "),
            _c("div", { staticClass: "description" }, [
              _vm._v(_vm._s(benefit.description))
            ])
          ])
        })
      ),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _vm.button && _vm.button.href
        ? _c(
            "v-btn",
            {
              staticClass: "btn--pinky btn--alt",
              attrs: { big: "", href: _vm.button.href }
            },
            [_vm._v("\n        " + _vm._s(_vm.button.text) + "\n    ")]
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true

if (false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-41e417db\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/mainPage/Benefits/styles.scss":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader!./node_modules/css-loader?sourceMap!./node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-41e417db","scoped":true,"sourceMap":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/mainPage/Benefits/styles.scss ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-41e417db","scoped":true,"sourceMap":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!./styles.scss */ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-41e417db\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/mainPage/Benefits/styles.scss");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! ../../../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("4a7a51d6", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);
//# sourceMappingURL=chunk-Benefits.529dd7975c901fa58f8b.js.map