(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["404"],{

/***/ "./local/assets/source/vue/components/common/Banner404/component.vue":
/*!***************************************************************************!*\
  !*** ./local/assets/source/vue/components/common/Banner404/component.vue ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !babel-loader!./script.js */ "./node_modules/babel-loader/lib/index.js!./local/assets/source/vue/components/common/Banner404/script.js");
/* harmony import */ var _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_template_compiler_index_id_data_v_10c8cd9c_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../../../../../node_modules/vue-loader/lib/template-compiler/index?{"id":"data-v-10c8cd9c","hasScoped":true,"optionsId":"0","buble":{"transforms":{}}}!../../../../../../../node_modules/vue-loader/lib/selector?type=template&index=0!./component.vue */ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-10c8cd9c\",\"hasScoped\":true,\"optionsId\":\"0\",\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/common/Banner404/component.vue");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_component_normalizer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/component-normalizer */ "./node_modules/vue-loader/lib/runtime/component-normalizer.js");
var disposed = false
function injectStyle (context) {
  if (disposed) return
  __webpack_require__(/*! !vue-style-loader!css-loader?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler/index?{"optionsId":"0","vue":true,"id":"data-v-10c8cd9c","scoped":true,"sourceMap":true}!sass-loader!./styles.scss */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-10c8cd9c\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/common/Banner404/styles.scss")
}
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-10c8cd9c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null

var Component = Object(_node_modules_vue_loader_lib_runtime_component_normalizer__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0___default.a,
  _node_modules_vue_loader_lib_template_compiler_index_id_data_v_10c8cd9c_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__["render"],
  _node_modules_vue_loader_lib_template_compiler_index_id_data_v_10c8cd9c_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__["staticRenderFns"],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "local/assets/source/vue/components/common/Banner404/component.vue"

/* hot reload */
if (false) {}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./local/assets/source/vue/components/common/Banner404/script.js":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./local/assets/source/vue/components/common/Banner404/script.js ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _BaseComponent = __webpack_require__(/*! ../../../BaseComponent */ "./local/assets/source/vue/BaseComponent.js");

var _BaseComponent2 = _interopRequireDefault(_BaseComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'Banner404',
  extends: _BaseComponent2.default,
  data: function data() {
    return {
      img: null,
      title: null,
      description: null
    };
  }
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-10c8cd9c\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/common/Banner404/styles.scss":
/*!**************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader?sourceMap!./node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-10c8cd9c","scoped":true,"sourceMap":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/common/Banner404/styles.scss ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "\n#app .banner[data-v-10c8cd9c]{min-height:-webkit-calc(100vh - 60px);min-height:-moz-calc(100vh - 60px);min-height:calc(100vh - 60px)\n}\n#app h1[data-v-10c8cd9c]{width:100%;max-width:468px;font-size:150px;line-height:1.22222;margin:80px 0 0\n}\n#app .description[data-v-10c8cd9c]{width:100%;max-width:600px;margin:45px 0\n}\n#app .banner__img-bg[data-v-10c8cd9c]{position:absolute;z-index:1;top:0;left:-webkit-calc(50% - 960px);left:-moz-calc(50% - 960px);left:calc(50% - 960px)\n}\n@media screen and (max-width:575px){\n#app .banner[data-v-10c8cd9c]{position:relative;height:-webkit-calc(100vh - 60px);height:-moz-calc(100vh - 60px);height:calc(100vh - 60px);overflow:hidden\n}\n#app h1[data-v-10c8cd9c]{margin:20px 0 0;font-size:55px\n}\n#app .description[data-v-10c8cd9c]{width:100%;max-width:600px;margin:15px 0\n}\n#app .banner__img-bg[data-v-10c8cd9c]{opacity:.5\n}\n}", "", {"version":3,"sources":["/var/www/project/local/assets/source/vue/components/common/Banner404/<input css 17>"],"names":[],"mappings":";AAAA,8BAAa,sCAAsC,mCAAmC,6BAA6B;CAAC;AAAA,yBAAQ,WAAW,gBAAgB,gBAAgB,oBAAoB,eAAe;CAAC;AAAA,mCAAkB,WAAW,gBAAgB,aAAa;CAAC;AAAA,sCAAqB,kBAAkB,UAAU,MAAM,+BAA+B,4BAA4B,sBAAsB;CAAC;AAAA;AAAoC,8BAAa,kBAAkB,kCAAkC,+BAA+B,0BAA0B,eAAe;CAAC;AAAA,yBAAQ,gBAAgB,cAAc;CAAC;AAAA,mCAAkB,WAAW,gBAAgB,aAAa;CAAC;AAAA,sCAAqB,UAAU;CAAC;CAAC","file":"styles.scss","sourcesContent":["#app .banner{min-height:-webkit-calc(100vh - 60px);min-height:-moz-calc(100vh - 60px);min-height:calc(100vh - 60px)}#app h1{width:100%;max-width:468px;font-size:150px;line-height:1.22222;margin:80px 0 0}#app .description{width:100%;max-width:600px;margin:45px 0}#app .banner__img-bg{position:absolute;z-index:1;top:0;left:-webkit-calc(50% - 960px);left:-moz-calc(50% - 960px);left:calc(50% - 960px)}@media screen and (max-width:575px){#app .banner{position:relative;height:-webkit-calc(100vh - 60px);height:-moz-calc(100vh - 60px);height:calc(100vh - 60px);overflow:hidden}#app h1{margin:20px 0 0;font-size:55px}#app .description{width:100%;max-width:600px;margin:15px 0}#app .banner__img-bg{opacity:.5}}"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-10c8cd9c\",\"hasScoped\":true,\"optionsId\":\"0\",\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/common/Banner404/component.vue":
/*!*******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-10c8cd9c","hasScoped":true,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/common/Banner404/component.vue ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "banner" },
    [
      _c("img", { staticClass: "banner__img-bg", attrs: { src: _vm.img } }),
      _vm._v(" "),
      _c(
        "v-container",
        [
          _c(
            "v-layout",
            [
              _c("v-flex", { attrs: { xs12: "" } }, [
                _c("h1", [_vm._v(_vm._s(_vm.title))]),
                _vm._v(" "),
                _c("div", {
                  staticClass: "description",
                  domProps: { innerHTML: _vm._s(_vm.description) }
                })
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true

if (false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-10c8cd9c\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/common/Banner404/styles.scss":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader!./node_modules/css-loader?sourceMap!./node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-10c8cd9c","scoped":true,"sourceMap":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/common/Banner404/styles.scss ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-10c8cd9c","scoped":true,"sourceMap":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!./styles.scss */ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-10c8cd9c\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/common/Banner404/styles.scss");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! ../../../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("ee987ac4", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);
//# sourceMappingURL=chunk-404.d729dc9fce204bd9a575.js.map