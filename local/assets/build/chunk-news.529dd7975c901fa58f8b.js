(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["News"],{

/***/ "./local/assets/source/vue/components/news/News/component.vue":
/*!********************************************************************!*\
  !*** ./local/assets/source/vue/components/news/News/component.vue ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !babel-loader!./script.js */ "./node_modules/babel-loader/lib/index.js!./local/assets/source/vue/components/news/News/script.js");
/* harmony import */ var _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_template_compiler_index_id_data_v_ae1eb732_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../../../../../node_modules/vue-loader/lib/template-compiler/index?{"id":"data-v-ae1eb732","hasScoped":true,"optionsId":"0","buble":{"transforms":{}}}!../../../../../../../node_modules/vue-loader/lib/selector?type=template&index=0!./component.vue */ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-ae1eb732\",\"hasScoped\":true,\"optionsId\":\"0\",\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/news/News/component.vue");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_component_normalizer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/component-normalizer */ "./node_modules/vue-loader/lib/runtime/component-normalizer.js");
var disposed = false
function injectStyle (context) {
  if (disposed) return
  __webpack_require__(/*! !vue-style-loader!css-loader?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler/index?{"optionsId":"0","vue":true,"id":"data-v-ae1eb732","scoped":true,"sourceMap":true}!sass-loader!./styles.scss */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-ae1eb732\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/news/News/styles.scss")
}
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-ae1eb732"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null

var Component = Object(_node_modules_vue_loader_lib_runtime_component_normalizer__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0___default.a,
  _node_modules_vue_loader_lib_template_compiler_index_id_data_v_ae1eb732_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__["render"],
  _node_modules_vue_loader_lib_template_compiler_index_id_data_v_ae1eb732_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__["staticRenderFns"],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "local/assets/source/vue/components/news/News/component.vue"

/* hot reload */
if (false) {}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./local/assets/source/vue/components/news/News/script.js":
/*!************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./local/assets/source/vue/components/news/News/script.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _toConsumableArray2 = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _BaseComponent = __webpack_require__(/*! ../../../BaseComponent */ "./local/assets/source/vue/BaseComponent.js");

var _BaseComponent2 = _interopRequireDefault(_BaseComponent);

__webpack_require__(/*! url-polyfill */ "./node_modules/url-polyfill/url-polyfill.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'News',
  extends: _BaseComponent2.default,
  data: function data() {
    return {
      tabs: null,
      currentTab: false,
      elements: null,
      currentElements: [],
      currentPage: false,
      chunksElements: [],
      chunkVolume: 10,
      minimalPage: 1,
      navigationLength: false,
      url: new URL(window.location.href),
      categories: ['all', 'news', 'shares']
    };
  },

  methods: {
    setDataInUrl: function setDataInUrl(params) {
      var newUrl = this.url.origin + this.url.pathname + '?' + this.encodeData(params);
      window.history.pushState(null, null, newUrl);
    },
    encodeData: function encodeData(data) {
      return (0, _keys2.default)(data).map(function (key) {
        return [key, data[key]].map(encodeURIComponent).join('=');
      }).join('&');
    },
    openPage: function openPage(page) {
      this.currentElements = this.chunksElements[page];
      this.refreshUrlObj();
      this.setDataInUrl(this.getObjParams(this.url.searchParams.get('category'), page));
    },
    refreshUrlObj: function refreshUrlObj() {
      this.url = new URL(window.location.href);
    },
    getObjParams: function getObjParams(category, page) {
      return {
        'category': category,
        'page': page
      };
    },
    getElementsByCategory: function getElementsByCategory(category) {
      if (category === 'all') return [].concat((0, _toConsumableArray3.default)(this.elements));

      var elements = [];
      this.elements.forEach(function (element) {
        if (element.category === category) {
          elements.push(element);
        }
      });
      return elements;
    },
    openCategory: function openCategory(category, page) {
      this.chunksElements = this.separateItemsOnChunks(this.getElementsByCategory(category));

      this.changeActiveTab(category);

      this.currentPage = page;

      this.refreshNavigationLength();

      this.currentElements = this.chunksElements[page];

      this.setDataInUrl(this.getObjParams(category, page));
    },
    changeActiveTab: function changeActiveTab(category) {
      var _this = this;

      (0, _keys2.default)(this.tabs).forEach(function (tabKey) {
        if (_this.tabs[tabKey].class.length === 0 && tabKey === category) {
          _this.tabs[tabKey].class = 'active';
        } else {
          _this.tabs[tabKey].class = '';
        }
      });
    },
    refreshNavigationLength: function refreshNavigationLength() {
      this.navigationLength = this.chunksElements.length - 1;
    },
    separateItemsOnChunks: function separateItemsOnChunks(elements) {
      var count = elements.length;
      if (count === 0) return true;

      var result = [];
      for (var i = 1; i <= count; i++) {
        var chunks = [];

        for (var p = 0; p < this.chunkVolume; p++) {
          var element = elements.shift();

          if (!element && chunks.length === 0) return result;

          if (element) {
            chunks.push(element);
          } else {
            result[i] = chunks;
            return result;
          }
        }

        result[i] = chunks;
      }

      return result;
    },
    validateCategory: function validateCategory(categoryFromUrl) {
      if (categoryFromUrl) {
        var foundCategory = this.categories.filter(function (categoryFromArray) {
          return categoryFromArray === categoryFromUrl;
        });
        if (foundCategory.length > 0) return categoryFromUrl;
      }
      return 'all';
    },
    validatePage: function validatePage(page, category) {
      if (page) {
        if (page > 0 && page <= this.getMaxPageOfCategory(category)) {
          return page;
        }
      }
      return this.minimalPage;
    },
    getMaxPageOfCategory: function getMaxPageOfCategory(categoryFromUrl) {
      var categoryElements = categoryFromUrl === 'all' ? [].concat((0, _toConsumableArray3.default)(this.elements)) : this.elements.filter(function (element) {
        return element.category === categoryFromUrl;
      });

      if (categoryElements.length === 0) return this.minimalPage;

      return Math.ceil(categoryElements.length / this.chunkVolume);
    }
  },
  mounted: function mounted() {
    var category = this.validateCategory(this.url.searchParams.get('category'));
    var page = this.validatePage(+this.url.searchParams.get('page'), category);

    this.openCategory(category, +page);
  }
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-ae1eb732\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/news/News/styles.scss":
/*!*******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader?sourceMap!./node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-ae1eb732","scoped":true,"sourceMap":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/news/News/styles.scss ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "\n#app .white-section[data-v-ae1eb732]{position:relative;z-index:1;margin-top:100px\n}\n#app .white-section .page-content[data-v-ae1eb732]{padding-top:50px\n}\n#app .filter[data-v-ae1eb732]{padding:30px 15px\n}\n#app .navigation-tabs[data-v-ae1eb732]{width:440px;max-width:100%;height:70px;position:relative;z-index:11;margin:auto;margin-top:20px;padding:5px;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px\n}\n#app .navigation-tabs .tab[data-v-ae1eb732]{width:33.3333%;height:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;color:#fff;text-align:center;padding:0 25px;cursor:pointer;background:0 0;-webkit-transition:.8s;-o-transition:.8s;-moz-transition:.8s;transition:.8s\n}\n#app .navigation-tabs .tab[data-v-ae1eb732]:hover{text-decoration:underline\n}\n#app .navigation-tabs .tab.active[data-v-ae1eb732]{color:#333;background:#fff;cursor:default\n}\n#app h5[data-v-ae1eb732]{font-size:15px;font-weight:300;text-transform:uppercase;color:#919191;margin-top:10px\n}\n#app h5.action[data-v-ae1eb732]{color:#eb276d\n}\n#app h3[data-v-ae1eb732]{margin-top:10px;font-size:18px\n}\n#app h3 a[data-v-ae1eb732]{color:#333\n}\n#app h3 a[data-v-ae1eb732]:hover{color:#eb276d\n}\n#app .description[data-v-ae1eb732]{margin-top:10px;font-size:18px\n}\n@media screen and (max-width:959px){\n#app .news-container h5[data-v-ae1eb732]{margin-top:-15px\n}\n#app .news-container .layout[data-v-ae1eb732]{padding-bottom:15px;margin-bottom:30px;border-bottom:1px solid rgba(0,0,0,.2)\n}\n}\n@media screen and (max-width:575px){\n#app .navigation-tabs[data-v-ae1eb732]{display:block;height:auto\n}\n#app .navigation-tabs .tab[data-v-ae1eb732]{width:100%;padding:5px 0\n}\n}", "", {"version":3,"sources":["/var/www/project/local/assets/source/vue/components/news/News/<input css 32>"],"names":[],"mappings":";AAAA,qCAAoB,kBAAkB,UAAU,gBAAgB;CAAC;AAAA,mDAAkC,gBAAgB;CAAC;AAAA,8BAAa,iBAAiB;CAAC;AAAA,uCAAsB,YAAY,eAAe,YAAY,kBAAkB,WAAW,YAAY,gBAAgB,YAAY,0BAA0B,uBAAuB,iBAAiB;CAAC;AAAA,4CAA2B,eAAe,YAAY,0BAA0B,uBAAuB,kBAAkB,WAAW,kBAAkB,eAAe,eAAe,eAAe,uBAAuB,kBAAkB,oBAAoB,cAAc;CAAC;AAAA,kDAAiC,yBAAyB;CAAC;AAAA,mDAAkC,WAAW,gBAAgB,cAAc;CAAC;AAAA,yBAAQ,eAAe,gBAAgB,yBAAyB,cAAc,eAAe;CAAC;AAAA,gCAAe,aAAa;CAAC;AAAA,yBAAQ,gBAAgB,cAAc;CAAC;AAAA,2BAAU,UAAU;CAAC;AAAA,iCAAgB,aAAa;CAAC;AAAA,mCAAkB,gBAAgB,cAAc;CAAC;AAAA;AAAoC,yCAAwB,gBAAgB;CAAC;AAAA,8CAA6B,oBAAoB,mBAAmB,sCAAsC;CAAC;CAAC;AAAA;AAAoC,uCAAsB,cAAc,WAAW;CAAC;AAAA,4CAA2B,WAAW,aAAa;CAAC;CAAC","file":"styles.scss","sourcesContent":["#app .white-section{position:relative;z-index:1;margin-top:100px}#app .white-section .page-content{padding-top:50px}#app .filter{padding:30px 15px}#app .navigation-tabs{width:440px;max-width:100%;height:70px;position:relative;z-index:11;margin:auto;margin-top:20px;padding:5px;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px}#app .navigation-tabs .tab{width:33.3333%;height:100%;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;color:#fff;text-align:center;padding:0 25px;cursor:pointer;background:0 0;-webkit-transition:.8s;-o-transition:.8s;-moz-transition:.8s;transition:.8s}#app .navigation-tabs .tab:hover{text-decoration:underline}#app .navigation-tabs .tab.active{color:#333;background:#fff;cursor:default}#app h5{font-size:15px;font-weight:300;text-transform:uppercase;color:#919191;margin-top:10px}#app h5.action{color:#eb276d}#app h3{margin-top:10px;font-size:18px}#app h3 a{color:#333}#app h3 a:hover{color:#eb276d}#app .description{margin-top:10px;font-size:18px}@media screen and (max-width:959px){#app .news-container h5{margin-top:-15px}#app .news-container .layout{padding-bottom:15px;margin-bottom:30px;border-bottom:1px solid rgba(0,0,0,.2)}}@media screen and (max-width:575px){#app .navigation-tabs{display:block;height:auto}#app .navigation-tabs .tab{width:100%;padding:5px 0}}"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/url-polyfill/url-polyfill.js":
/*!***************************************************!*\
  !*** ./node_modules/url-polyfill/url-polyfill.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {(function(global) {
  /**
   * Polyfill URLSearchParams
   *
   * Inspired from : https://github.com/WebReflection/url-search-params/blob/master/src/url-search-params.js
   */

  var checkIfIteratorIsSupported = function() {
    try {
      return !!Symbol.iterator;
    } catch(error) {
      return false;
    }
  };


  var iteratorSupported = checkIfIteratorIsSupported();

  var createIterator = function(items) {
    var iterator = {
      next: function() {
        var value = items.shift();
        return { done: value === void 0, value: value };
      }
    };

    if(iteratorSupported) {
      iterator[Symbol.iterator] = function() {
        return iterator;
      };
    }

    return iterator;
  };

  /**
   * Search param name and values should be encoded according to https://url.spec.whatwg.org/#urlencoded-serializing
   * encodeURIComponent() produces the same result except encoding spaces as `%20` instead of `+`.
   */
  var serializeParam = function(value) {
    return encodeURIComponent(value).replace(/%20/g, '+');
  };

  var deserializeParam = function(value) {
    return decodeURIComponent(value).replace(/\+/g, ' ');
  };

  var polyfillURLSearchParams= function() {

    var URLSearchParams = function(searchString) {
      Object.defineProperty(this, '_entries', { value: {} });

      if(typeof searchString === 'string') {
        if(searchString !== '') {
          searchString = searchString.replace(/^\?/, '');
          var attributes = searchString.split('&');
          var attribute;
          for(var i = 0; i < attributes.length; i++) {
            attribute = attributes[i].split('=');
            this.append(
              deserializeParam(attribute[0]),
              (attribute.length > 1) ? deserializeParam(attribute[1]) : ''
            );
          }
        }
      } else if(searchString instanceof URLSearchParams) {
        var _this = this;
        searchString.forEach(function(value, name) {
          _this.append(value, name);
        });
      }
    };

    var proto = URLSearchParams.prototype;

    proto.append = function(name, value) {
      if(name in this._entries) {
        this._entries[name].push(value.toString());
      } else {
        this._entries[name] = [value.toString()];
      }
    };

    proto.delete = function(name) {
      delete this._entries[name];
    };

    proto.get = function(name) {
      return (name in this._entries) ? this._entries[name][0] : null;
    };

    proto.getAll = function(name) {
      return (name in this._entries) ? this._entries[name].slice(0) : [];
    };

    proto.has = function(name) {
      return (name in this._entries);
    };

    proto.set = function(name, value) {
      this._entries[name] = [value.toString()];
    };

    proto.forEach = function(callback, thisArg) {
      var entries;
      for(var name in this._entries) {
        if(this._entries.hasOwnProperty(name)) {
          entries = this._entries[name];
          for(var i = 0; i < entries.length; i++) {
            callback.call(thisArg, entries[i], name, this);
          }
        }
      }
    };

    proto.keys = function() {
      var items = [];
      this.forEach(function(value, name) { items.push(name); });
      return createIterator(items);
    };

    proto.values = function() {
      var items = [];
      this.forEach(function(value) { items.push(value); });
      return createIterator(items);
    };

    proto.entries = function() {
      var items = [];
      this.forEach(function(value, name) { items.push([name, value]); });
      return createIterator(items);
    };

    if(iteratorSupported) {
      proto[Symbol.iterator] = proto.entries;
    }

    proto.toString = function() {
      var searchString = '';
      this.forEach(function(value, name) {
        if(searchString.length > 0) searchString+= '&';
        searchString += serializeParam(name) + '=' + serializeParam(value);
      });
      return searchString;
    };

    global.URLSearchParams = URLSearchParams;
  };

  if(!('URLSearchParams' in global) || (new URLSearchParams('?a=1').toString() !== 'a=1')) {
    polyfillURLSearchParams();
  }

  // HTMLAnchorElement

})(
  (typeof global !== 'undefined') ? global
    : ((typeof window !== 'undefined') ? window
    : ((typeof self !== 'undefined') ? self : this))
);

(function(global) {
  /**
   * Polyfill URL
   *
   * Inspired from : https://github.com/arv/DOM-URL-Polyfill/blob/master/src/url.js
   */

  var checkIfURLIsSupported = function() {
    try {
      var u = new URL('b', 'http://a');
      u.pathname = 'c%20d';
      return (u.href === 'http://a/c%20d') && u.searchParams;
    } catch(e) {
      return false;
    }
  };


  var polyfillURL = function() {
    var _URL = global.URL;

    var URL = function(url, base) {
      if(typeof url !== 'string') url = String(url);

      var doc = document.implementation.createHTMLDocument('');
      window.doc = doc;
      if(base) {
        var baseElement = doc.createElement('base');
        baseElement.href = base;
        doc.head.appendChild(baseElement);
      }

      var anchorElement = doc.createElement('a');
      anchorElement.href = url;
      doc.body.appendChild(anchorElement);
      anchorElement.href = anchorElement.href; // force href to refresh

      if(anchorElement.protocol === ':' || !/:/.test(anchorElement.href)) {
        throw new TypeError('Invalid URL');
      }

      Object.defineProperty(this, '_anchorElement', {
        value: anchorElement
      });
    };

    var proto = URL.prototype;

    var linkURLWithAnchorAttribute = function(attributeName) {
      Object.defineProperty(proto, attributeName, {
        get: function() {
          return this._anchorElement[attributeName];
        },
        set: function(value) {
          this._anchorElement[attributeName] = value;
        },
        enumerable: true
      });
    };

    ['hash', 'host', 'hostname', 'port', 'protocol', 'search']
    .forEach(function(attributeName) {
      linkURLWithAnchorAttribute(attributeName);
    });

    Object.defineProperties(proto, {

      'toString': {
        get: function() {
          var _this = this;
          return function() {
            return _this.href;
          };
        }
      },

      'href' : {
        get: function() {
          return this._anchorElement.href.replace(/\?$/,'');
        },
        set: function(value) {
          this._anchorElement.href = value;
        },
        enumerable: true
      },

      'pathname' : {
        get: function() {
          return this._anchorElement.pathname.replace(/(^\/?)/,'/');
        },
        set: function(value) {
          this._anchorElement.pathname = value;
        },
        enumerable: true
      },

      'origin': {
        get: function() {
          // get expected port from protocol
          var expectedPort = {'http:': 80, 'https:': 443, 'ftp:': 21}[this._anchorElement.protocol];
          // add port to origin if, expected port is different than actual port
          // and it is not empty f.e http://foo:8080
          // 8080 != 80 && 8080 != ''
          var addPortToOrigin = this._anchorElement.port != expectedPort &&
            this._anchorElement.port !== ''

          return this._anchorElement.protocol +
            '//' +
            this._anchorElement.hostname +
            (addPortToOrigin ? (':' + this._anchorElement.port) : '');
        },
        enumerable: true
      },

      'password': { // TODO
        get: function() {
          return '';
        },
        set: function(value) {
        },
        enumerable: true
      },

      'username': { // TODO
        get: function() {
          return '';
        },
        set: function(value) {
        },
        enumerable: true
      },

      'searchParams': {
        get: function() {
          var searchParams = new URLSearchParams(this.search);
          var _this = this;
          ['append', 'delete', 'set'].forEach(function(methodName) {
            var method = searchParams[methodName];
            searchParams[methodName] = function() {
              method.apply(searchParams, arguments);
              _this.search = searchParams.toString();
            };
          });
          return searchParams;
        },
        enumerable: true
      }
    });

    URL.createObjectURL = function(blob) {
      return _URL.createObjectURL.apply(_URL, arguments);
    };

    URL.revokeObjectURL = function(url) {
      return _URL.revokeObjectURL.apply(_URL, arguments);
    };

    global.URL = URL;

  };

  if(!checkIfURLIsSupported()) {
    polyfillURL();
  }

  if((global.location !== void 0) && !('origin' in global.location)) {
    var getOrigin = function() {
      return global.location.protocol + '//' + global.location.hostname + (global.location.port ? (':' + global.location.port) : '');
    };

    try {
      Object.defineProperty(global.location, 'origin', {
        get: getOrigin,
        enumerable: true
      });
    } catch(e) {
      setInterval(function() {
        global.location.origin = getOrigin();
      }, 100);
    }
  }

})(
  (typeof global !== 'undefined') ? global
    : ((typeof window !== 'undefined') ? window
    : ((typeof self !== 'undefined') ? self : this))
);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-ae1eb732\",\"hasScoped\":true,\"optionsId\":\"0\",\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/news/News/component.vue":
/*!************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-ae1eb732","hasScoped":true,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/news/News/component.vue ***!
  \************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "white-section section-arrow-top" },
    [
      _c("div", { staticClass: "filter" }, [
        _c(
          "div",
          { staticClass: "just-num-46 navigation-tabs mix-gradient--revert" },
          _vm._l(_vm.tabs, function(tab, category) {
            return _c(
              "div",
              {
                class: "tab just-num-5 " + tab.class,
                on: {
                  click: function($event) {
                    _vm.openCategory(category, _vm.minimalPage)
                  }
                }
              },
              [_vm._v(_vm._s(tab.name))]
            )
          })
        )
      ]),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "news-container", attrs: { "grid-list-md": "" } },
        [
          _vm._l(_vm.currentElements, function(element) {
            return _c(
              "v-layout",
              { key: element.sort, staticClass: "just-wrap" },
              [
                _c(
                  "v-flex",
                  { staticClass: "just-num-7", attrs: { xs12: "", md5: "" } },
                  [
                    element.imageUrl
                      ? _c("div", { staticClass: "img" }, [
                          _c("img", { attrs: { src: element.imageUrl } })
                        ])
                      : _c("div", { staticClass: "img nophoto" }, [
                          _c("img", {
                            attrs: {
                              src: "/local/assets/images/bg/news.png",
                              alt: ""
                            }
                          })
                        ])
                  ]
                ),
                _vm._v(" "),
                _c("v-flex", { attrs: { xs12: "", md7: "" } }, [
                  element.finishedShare
                    ? _c("h5", [_vm._v("Акция завершена")])
                    : element.newsDate.from
                      ? _c("h5", { staticClass: "action" }, [
                          _vm._v(
                            "Акция с " +
                              _vm._s(element.newsDate.from) +
                              " по " +
                              _vm._s(element.newsDate.to)
                          )
                        ])
                      : _c("h5", [_vm._v(_vm._s(element.newsDate))]),
                  _vm._v(" "),
                  _c("h3", [
                    _c("a", { attrs: { href: element.detailUrl } }, [
                      _vm._v(_vm._s(element.title))
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", {
                    staticClass: "description",
                    domProps: { innerHTML: _vm._s(element.description) }
                  })
                ])
              ],
              1
            )
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "pagination-box just-num-5" },
            [
              _vm.navigationLength
                ? _c("v-pagination", {
                    attrs: { length: _vm.navigationLength, "total-visible": 7 },
                    on: {
                      input: function($event) {
                        _vm.openPage($event)
                      }
                    },
                    model: {
                      value: _vm.currentPage,
                      callback: function($$v) {
                        _vm.currentPage = $$v
                      },
                      expression: "currentPage"
                    }
                  })
                : _vm._e()
            ],
            1
          )
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true

if (false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-ae1eb732\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/news/News/styles.scss":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader!./node_modules/css-loader?sourceMap!./node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-ae1eb732","scoped":true,"sourceMap":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/news/News/styles.scss ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-ae1eb732","scoped":true,"sourceMap":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!./styles.scss */ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-ae1eb732\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/news/News/styles.scss");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! ../../../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("fb4a13f4", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);
//# sourceMappingURL=chunk-News.529dd7975c901fa58f8b.js.map