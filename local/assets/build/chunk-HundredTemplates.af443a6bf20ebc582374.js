(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["HundredTemplates"],{

/***/ "./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/component.vue":
/*!***********************************************************************************************!*\
  !*** ./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/component.vue ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !babel-loader!./script.js */ "./node_modules/babel-loader/lib/index.js!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/script.js");
/* harmony import */ var _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_template_compiler_index_id_data_v_6cb1c1a8_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../../../../../node_modules/vue-loader/lib/template-compiler/index?{"id":"data-v-6cb1c1a8","hasScoped":true,"optionsId":"0","buble":{"transforms":{}}}!../../../../../../../node_modules/vue-loader/lib/selector?type=template&index=0!./component.vue */ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6cb1c1a8\",\"hasScoped\":true,\"optionsId\":\"0\",\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/component.vue");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_component_normalizer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/component-normalizer */ "./node_modules/vue-loader/lib/runtime/component-normalizer.js");
var disposed = false
function injectStyle (context) {
  if (disposed) return
  __webpack_require__(/*! !vue-style-loader!css-loader?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler/index?{"optionsId":"0","vue":true,"id":"data-v-6cb1c1a8","scoped":true,"sourceMap":true}!sass-loader!./styles.scss */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-6cb1c1a8\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/styles.scss")
}
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6cb1c1a8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null

var Component = Object(_node_modules_vue_loader_lib_runtime_component_normalizer__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _babel_loader_script_js__WEBPACK_IMPORTED_MODULE_0___default.a,
  _node_modules_vue_loader_lib_template_compiler_index_id_data_v_6cb1c1a8_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__["render"],
  _node_modules_vue_loader_lib_template_compiler_index_id_data_v_6cb1c1a8_hasScoped_true_optionsId_0_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_component_vue__WEBPACK_IMPORTED_MODULE_1__["staticRenderFns"],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "local/assets/source/vue/components/siteConstructorPage/HundredTemplates/component.vue"

/* hot reload */
if (false) {}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/script.js":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/script.js ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _BaseComponent = __webpack_require__(/*! ../../../BaseComponent */ "./local/assets/source/vue/BaseComponent.js");

var _BaseComponent2 = _interopRequireDefault(_BaseComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'HundredTemplates',
  extends: _BaseComponent2.default,
  data: function data() {
    return {
      title: null,
      imageUrl: null
    };
  }
};

/***/ }),

/***/ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-6cb1c1a8\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/styles.scss":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader?sourceMap!./node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-6cb1c1a8","scoped":true,"sourceMap":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/styles.scss ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "\n#app .section--templates[data-v-6cb1c1a8]{position:relative;height:675px;overflow:hidden\n}\n#app .section--templates h2[data-v-6cb1c1a8]{font-size:150px;color:#fff;max-width:800px;text-align:center;margin:0 auto;text-shadow:0 0 81px #000;font-weight:700;margin-top:160px;line-height:135px\n}\n#app .section--templates .banner__img-bg[data-v-6cb1c1a8]{position:absolute;z-index:1;top:-10px;left:-webkit-calc(50% - 1316px);left:-moz-calc(50% - 1316px);left:calc(50% - 1316px)\n}\n#app .section--templates[data-v-6cb1c1a8]:before{content:'';display:block;height:100px;position:absolute;left:-webkit-calc(50% - 1070px);left:-moz-calc(50% - 1070px);left:calc(50% - 1070px);border:59em solid transparent;border-top:100px solid #1e1250;z-index:2\n}\n@media screen and (max-width:768px){\n.section--templates h2[data-v-6cb1c1a8]{font-size:110px!important\n}\n}\n@media screen and (max-width:575px){\n.section--templates h2[data-v-6cb1c1a8]{font-size:55px!important;line-height:105px!important\n}\n}", "", {"version":3,"sources":["/var/www/project/local/assets/source/vue/components/siteConstructorPage/HundredTemplates/<input css 38>"],"names":[],"mappings":";AAAA,0CAAyB,kBAAkB,aAAa,eAAe;CAAC;AAAA,6CAA4B,gBAAgB,WAAW,gBAAgB,kBAAkB,cAAc,0BAA0B,gBAAgB,iBAAiB,iBAAiB;CAAC;AAAA,0DAAyC,kBAAkB,UAAU,UAAU,gCAAgC,6BAA6B,uBAAuB;CAAC;AAAA,iDAAgC,WAAW,cAAc,aAAa,kBAAkB,gCAAgC,6BAA6B,wBAAwB,8BAA8B,+BAA+B,SAAS;CAAC;AAAA;AAAoC,wCAAuB,yBAAyB;CAAC;CAAC;AAAA;AAAoC,wCAAuB,yBAAyB,2BAA2B;CAAC;CAAC","file":"styles.scss","sourcesContent":["#app .section--templates{position:relative;height:675px;overflow:hidden}#app .section--templates h2{font-size:150px;color:#fff;max-width:800px;text-align:center;margin:0 auto;text-shadow:0 0 81px #000;font-weight:700;margin-top:160px;line-height:135px}#app .section--templates .banner__img-bg{position:absolute;z-index:1;top:-10px;left:-webkit-calc(50% - 1316px);left:-moz-calc(50% - 1316px);left:calc(50% - 1316px)}#app .section--templates:before{content:'';display:block;height:100px;position:absolute;left:-webkit-calc(50% - 1070px);left:-moz-calc(50% - 1070px);left:calc(50% - 1070px);border:59em solid transparent;border-top:100px solid #1e1250;z-index:2}@media screen and (max-width:768px){.section--templates h2{font-size:110px!important}}@media screen and (max-width:575px){.section--templates h2{font-size:55px!important;line-height:105px!important}}"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-6cb1c1a8\",\"hasScoped\":true,\"optionsId\":\"0\",\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/component.vue":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-6cb1c1a8","hasScoped":true,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/component.vue ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "white-section section--templates" },
    [
      _c("img", {
        staticClass: "banner__img-bg",
        attrs: { src: _vm.imageUrl }
      }),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "special-offer" },
        [
          _c(
            "v-layout",
            [
              _c(
                "v-flex",
                {
                  staticClass: "special-offer__box",
                  attrs: { xs12: "", center: "" }
                },
                [
                  _c("h2", [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.title) +
                        "\n                "
                    )
                  ])
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true

if (false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-6cb1c1a8\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/styles.scss":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader!./node_modules/css-loader?sourceMap!./node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-6cb1c1a8","scoped":true,"sourceMap":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/styles.scss ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader?sourceMap!../../../../../../../node_modules/vue-loader/lib/style-compiler?{"optionsId":"0","vue":true,"id":"data-v-6cb1c1a8","scoped":true,"sourceMap":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!./styles.scss */ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"optionsId\":\"0\",\"vue\":true,\"id\":\"data-v-6cb1c1a8\",\"scoped\":true,\"sourceMap\":true}!./node_modules/sass-loader/lib/loader.js!./local/assets/source/vue/components/siteConstructorPage/HundredTemplates/styles.scss");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! ../../../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("09bb5ee0", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);
//# sourceMappingURL=chunk-HundredTemplates.af443a6bf20ebc582374.js.map