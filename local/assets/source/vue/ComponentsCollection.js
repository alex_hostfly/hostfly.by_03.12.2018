import Header from './components/template/Header/component.vue'
import Footer from './components/template/Footer/component.vue'

export const ComponentsCollection = {
  /* app components */
  Header: () => Promise.resolve(Header),
  Footer: () => Promise.resolve(Footer),

  Banner: () => import('./components/common/Banner/component.vue' /* webpackChunkName: "Common" */),
  SpecialOffer: () => import('./components/common/SpecialOffer/component.vue' /* webpackChunkName: "Common" */),
  TypesRates: () => import('./components/common/TypesRates/component.vue' /* webpackChunkName: "Common" */),
  FeaturesList: () => import('./components/common/FeaturesList/component.vue' /* webpackChunkName: "Common" */),
  FeaturesListTabs: () => import('./components/common/FeaturesListTabs/component.vue' /* webpackChunkName: "Common" */),
  SearchAndTransferDomain: () => import('./components/common/SearchAndTransferDomain/component.vue' /* webpackChunkName: "Common" */),

  ImageText: () => import('./components/common/ImageText/component.vue' /* webpackChunkName: "Common" */),
  SliderBanner: () => import('./components/common/SliderBanner/component.vue' /* webpackChunkName: "Common" */),
  ButtonBanner: () => import('./components/common/ButtonBanner/component.vue' /* webpackChunkName: "Common" */),
  SubscribeBanner: () => import('./components/common/SubscribeBanner/component.vue' /* webpackChunkName: "Common" */),
  OnlineChatBanner: () => import('./components/common/OnlineChatBanner/component.vue' /* webpackChunkName: "Common" */),
  RatesShortList: () => import('./components/mainPage/RatesShortList/component.vue' /* webpackChunkName: "Main" */),
  Slider: () => import('./components/mainPage/Slider/component.vue' /* webpackChunkName: "Main" */),
  NewsList: () => import('./components/mainPage/NewsList/component.vue' /* webpackChunkName: "Main" */),
  Benefits: () => import('./components/mainPage/Benefits/component.vue' /* webpackChunkName: "Benefits" */),

  RatesFullList: () => import('./components/Rates/RatesFullList/component.vue' /* webpackChunkName: "Rates" */),
  RatesRecommendations: () => import('./components/Rates/RatesRecommendations/component.vue' /* webpackChunkName: "Rates" */),
  RatesFaq: () => import('./components/Rates/RatesFaq/component.vue' /* webpackChunkName: "Rates" */),

  FaqList: () => import('./components/faqPage/FaqList/component.vue' /* webpackChunkName: "Faq" */),

  AddressBlock: () => import('./components/contactsPage/AddressBlock/component.vue' /* webpackChunkName: "Contacts" */),
  PhonesBlock: () => import('./components/contactsPage/PhonesBlock/component.vue' /* webpackChunkName: "Contacts" */),
  FormAndMapBlock: () => import('./components/contactsPage/FormAndMapBlock/component.vue' /* webpackChunkName: "Contacts" */),
  FeedbackForm: () => import('./components/contactsPage/FeedbackForm.vue' /* webpackChunkName: "Contacts" */),
  Map: () => import('./components/contactsPage/Map.vue' /* webpackChunkName: "Contacts" */),

  NewsDetailBanner: () => import('./components/news/NewsDetailBanner/component.vue' /* webpackChunkName: "news" */),
  NewsDetailContainer: () => import('./components/news/NewsDetailContainer/component.vue' /* webpackChunkName: "news" */),
  NewsDetailCard: () => import('./components/news/NewsDetailCard/component.vue' /* webpackChunkName: "news" */),
  NewsDetailLastWidget: () => import('./components/news/NewsDetailLastWidget/component.vue' /* webpackChunkName: "news" */),
  ShareSocialNetworks: () => import('./components/news/ShareSocialNetworks/component.vue' /* webpackChunkName: "news" */),

  Clients: () => import('./components/howAreWePage/Clients/component.vue' /* webpackChunkName: "HowAreWePage" */),
  BlockAboutUs: () => import('./components/howAreWePage/BlocksAboutUs/component.vue' /* webpackChunkName: "HowAreWePage" */),
  Achievements: () => import('./components/howAreWePage/Achievements/component.vue' /* webpackChunkName: "HowAreWePage" */),
  AchievementsPopup: () => import('./components/popups/AchievementsPopup/component.vue' /* webpackChunkName: "HowAreWePage" */),

  News: () => import('./components/news/News/component.vue' /* webpackChunkName: "News" */),
  RatesUserGuide: () => import('./components/siteConstructorPage/RatesUserGuide/component.vue' /* webpackChunkName: "RatesUserGuide" */),
  SpecialOfferConstructor: () => import('./components/siteConstructorPage/SpecialOfferConstructor/component.vue' /* webpackChunkName: "SpecialOfferConstructor" */),
  HundredTemplates: () => import('./components/siteConstructorPage/HundredTemplates/component.vue' /* webpackChunkName: "HundredTemplates" */),

  Banner404: () => import('./components/common/Banner404/component.vue' /* webpackChunkName: "404" */)

}
