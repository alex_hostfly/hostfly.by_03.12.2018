import BaseComponent from '../../../BaseComponent'

export default {
  name: 'NewsDetailLastWidget',
  extends: BaseComponent,
  props: ['lastNews']
}