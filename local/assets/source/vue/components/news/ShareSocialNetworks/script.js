import BaseComponent from '../../../BaseComponent'

export default {
  name: 'ShareSocialNetworks',
  extends: BaseComponent,
  data () {
    return {
      news: null
    }
  },
  methods: {
    shareVk () {
      let url = this.createFullUrl('http://vkontakte.ru/share.php?', {
        'url': window.location.href,
        'title': this.news.title,
        'description': this.news.previewText,
        'image': this.news.previewImage,
        'noparse': true,
      })
      this.showPopup(url)
    },
    shareFacebook () {
      let url = this.createFullUrl('http://www.facebook.com/sharer.php?', {
        's': 100,
        'p[title]': this.news.title,
        'p[summary]': this.news.previewText,
        'p[url]': window.location.href,
        'p[images][0]': this.news.previewImage
      })
      this.showPopup(url)
    },
    shareOk () {
      let url = this.createFullUrl('http://www.odnoklassniki.ru/dk?', {
        'st.cmd': 'addShare',
        'st.s': 1,
        'st.comments': this.news.previewText,
        'st._surl': window.location.href,
      })
      this.showPopup(url)
    },
    shareTwitter () {
      let url = this.createFullUrl('http://twitter.com/share?', {
        'text': this.news.title,
        'url': window.location.href,
        'twitter_counter': true,
      })
      this.showPopup(url)
    },
    shareViber () {
      let url = this.createFullUrl('viber://forward?', {
        'text': this.news.title + ' ' + window.location.href
      })
      this.showPopup(url)
    },
    shareTelegram () {
      let url = this.createFullUrl('https://t.me/share/url', {
        'url': window.location.href,
        'mobile': true,
        '_utl_t': 'tm',
        'text': this.news.previewText
      })
      this.showPopup(url)
    },
    showPopup (url) {
      window.open(url, '', `toolbar=0,status=yes,width=626,height=436,top=${((window.screen.height - 436) / 2)},left=${((window.screen.width - 626) / 2)}`)
    }
  }
}
