import BaseComponent from '../../../BaseComponent'
import 'url-polyfill'

export default {
  name: 'News',
  extends: BaseComponent,
  data () {
    return {
      tabs: null,
      currentTab: false,
      elements: null,
      currentElements: [],
      currentPage: false,
      chunksElements: [],
      chunkVolume: 10,
      minimalPage: 1,
      navigationLength: false,
      url: new URL(window.location.href),
      categories: ['all', 'news', 'shares']
    }
  },
  methods: {
    setDataInUrl (params) {
      let newUrl = this.url.origin + this.url.pathname + '?' + this.encodeData(params)
      window.history.pushState(null, null, newUrl)
    },
    encodeData (data) {
      return Object.keys(data).map((key) => {
        return [key, data[key]].map(encodeURIComponent).join('=')
      }).join('&')
    },
    openPage (page) {
      this.currentElements = this.chunksElements[page]
      this.refreshUrlObj()
      this.setDataInUrl(this.getObjParams(this.url.searchParams.get('category'), page))
    },
    refreshUrlObj () {
      this.url = new URL(window.location.href)
    },
    getObjParams (category, page) {
      return {
        'category': category,
        'page': page
      }
    },
    getElementsByCategory (category) {
      if (category === 'all') return [...this.elements]

      let elements = []
      this.elements.forEach((element) => {
        if (element.category === category) {
          elements.push(element)
        }
      })
      return elements
    },
    openCategory (category, page) {
      this.chunksElements = this.separateItemsOnChunks(this.getElementsByCategory(category))

      this.changeActiveTab(category)

      this.currentPage = page

      this.refreshNavigationLength()

      this.currentElements = this.chunksElements[page]

      this.setDataInUrl(this.getObjParams(category, page))
    },
    changeActiveTab (category) {
      Object.keys(this.tabs).forEach((tabKey) => {
        if (this.tabs[tabKey].class.length === 0 && tabKey === category) {
          this.tabs[tabKey].class = 'active'
        } else {
          this.tabs[tabKey].class = ''
        }
      })
    },
    refreshNavigationLength () {
      this.navigationLength = this.chunksElements.length - 1
    },
    separateItemsOnChunks (elements) {
      let count = elements.length
      if (count === 0) return true

      let result = []
      for (let i = 1; i <= count; i++) {
        let chunks = []

        for (let p = 0; p < this.chunkVolume; p++) {
          let element = elements.shift()

          if (!element && chunks.length === 0) return result

          if (element) {
            chunks.push(element)
          } else {
            result[i] = chunks
            return result
          }
        }

        result[i] = chunks
      }

      return result
    },
    validateCategory (categoryFromUrl) {
      if (categoryFromUrl) {
        let foundCategory = this.categories.filter(categoryFromArray => categoryFromArray === categoryFromUrl)
        if (foundCategory.length > 0) return categoryFromUrl
      }
      return 'all'
    },
    validatePage (page, category) {
      if (page) {
        if (page > 0 && page <= this.getMaxPageOfCategory(category)) {
          return page
        }
      }
      return this.minimalPage
    },
    getMaxPageOfCategory (categoryFromUrl) {
      let categoryElements = categoryFromUrl === 'all'
        ? [...this.elements]
        : this.elements.filter(element => element.category === categoryFromUrl)

      if (categoryElements.length === 0) return this.minimalPage

      return Math.ceil(categoryElements.length / this.chunkVolume)
    }
  },
  mounted () {
    let category = this.validateCategory(this.url.searchParams.get('category'))
    let page = this.validatePage(+this.url.searchParams.get('page'), category)

    this.openCategory(category, +page)
  }
}