import BaseComponent from '../../../BaseComponent'
import Banner from '../../common/Banner/component.vue'

export default {
  name: 'NewsDetailBanner',
  extends: BaseComponent,
  components: {
    Banner
  },
  data () {
    return {
      title: null,
      description: null,
      goToListButton: null,
      img: null
    }
  }
}