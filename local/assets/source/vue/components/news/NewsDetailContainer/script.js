import BaseComponent from '../../../BaseComponent'
import NewsDetailLastWidget from '../NewsDetailLastWidget/component.vue'
import NewsDetailCard from '../NewsDetailCard/component.vue'

export default {
  name: 'NewsDetailContainer',
  extends: BaseComponent,
  components: {
    NewsDetailCard,
    NewsDetailLastWidget
  },
  data () {
    return {
      lastNews: null,
      currentNews: null
    }
  }
}