import BaseComponent from '../../../BaseComponent'

export default {
  name: 'NewsDetailCard',
  extends: BaseComponent,
  props: ['currentNews']
}