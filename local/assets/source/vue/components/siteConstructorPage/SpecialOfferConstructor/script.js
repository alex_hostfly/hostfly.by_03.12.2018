import BaseComponent from '../../../BaseComponent'

export default {
  name: 'SpecialOfferConstructor',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      button: null
    }
  }
}
