import BaseComponent from '../../../BaseComponent'

export default {
  name: 'HundredTemplates',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      imageUrl: null
    }
  }
}