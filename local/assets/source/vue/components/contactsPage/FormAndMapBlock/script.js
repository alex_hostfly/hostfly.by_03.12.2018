import BaseComponent from '../../../BaseComponent'
import Map from '../../../components/contactsPage/Map.vue'
import FeedbackForm from '../../../components/contactsPage/FeedbackForm.vue'

export default {
  name: 'FormAndMapBlock',
  extends: BaseComponent,
  components: {
    Map, FeedbackForm
  },
  data () {
    return {
      title: null,
      form: null,
      api: null,
      coordinationCenterY: null,
      coordinationCenterX: null
    }
  }
}