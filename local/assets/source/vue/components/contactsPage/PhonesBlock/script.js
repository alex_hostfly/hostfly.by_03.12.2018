import BaseComponent from '../../../BaseComponent'

export default {
  name: 'PhonesBlock',
  extends: BaseComponent,
  data () {
    return {
      saleDepartment: null,
      support: null,
    }
  },
}