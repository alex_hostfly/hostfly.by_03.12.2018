import BaseComponent from '../../../BaseComponent'

export default {
  name: 'AddressBlock',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      text: null,
      email: null,
    }
  },
}