import BaseComponent from '../../../BaseComponent'

export default {
  name: 'Header',
  extends: BaseComponent,
  data () {
    return {
      phone: null,
      menu: null,
      authHref: null,
      showPopup: false,
      contacts: null,
      phoneObj: null,
      phonesPopup: false,
      mobileMenu: false,
    }
  },
  methods: {
    formatAllPhonesInContacts () {
      let refreshedContacts = [...this.contacts]
      refreshedContacts.forEach((blockData, key) => {
        if (blockData.additionalContacts.length > 0) {
          blockData.additionalContacts.forEach((contactData, keyContact) => {
            refreshedContacts[key].additionalContacts[keyContact].phone = this.getFormattedPhone(contactData.phone, false)
          })
        }

        if (blockData.mainPhone) {
          refreshedContacts[key].mainPhone = this.getFormattedPhone(blockData.mainPhone, false)
        }
      })

      return refreshedContacts
    },
    switchPhones(){
      if(!this.phonesPopup){
        this.phonesPopup=true;
      }
      else{
        this.phonesPopup=false;
      }
    },
    showMenu(){
      if(!this.mobileMenu){
        this.mobileMenu=true;
      }
      else{
        this.mobileMenu=false;
      }
    }
  },
  mounted () {
    this.phoneObj = this.getFormattedPhone(this.phone)
    this.contacts = this.formatAllPhonesInContacts()
  }
}
