import BaseComponent from '../../../BaseComponent'

export default {
  name: 'Footer',
  extends: BaseComponent,
  data () {
    return {
      prettyLinks: null,
      cards: null,
      bottomMenu: null,
      socialNetworks: null,
      copyright: null
    }
  }
}