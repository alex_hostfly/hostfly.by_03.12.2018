import BaseComponent from '../../../BaseComponent'
import Banner from '../Banner/component.vue'

export default {
  name: 'SubscribeBanner',
  extends: BaseComponent,
  components: {
    Banner
  },
  data () {
    return {
      img: null,
      title: null,
      description: null,
      subscribe: null,
      userMail: ''
    }
  }
}