import Banner from '../Banner/component.vue'
import BaseComponent from '../../../BaseComponent'

export default {
  name: 'OnlineChatBanner',
  extends: BaseComponent,
  components: {
    Banner
  },
  data () {
    return {
      title: null,
      description: null,
      onlineChat: null,
      textBeforeButton: null,
      img:null
    }
  }
}