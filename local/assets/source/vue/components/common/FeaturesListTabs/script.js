import BaseComponent from '../../../BaseComponent'
import FeaturesList from '../FeaturesList/component.vue'

export default {
  name: 'FeaturesListTabs',
  extends: BaseComponent,
  components: {
    FeaturesList
  },
  data () {
    return {
      start: 0,
      items: null
    }
  },
  methods: {
    changeTab (index) {
      this.start = index
    }
  }
}
