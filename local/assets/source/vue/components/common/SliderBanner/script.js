import BaseComponent from '../../../BaseComponent'
import $ from 'jquery'

export default {
  name: 'SliderBanner',
  extends: BaseComponent,
  components: {},
  data () {
    return {
      img: null,
      slides: null,
      bannerClass: null
    }
  },
  mounted () {

    $('#banner-slider').slick({
      arrows: false,
      infinite: false,
      dots: true
    })

  }
}