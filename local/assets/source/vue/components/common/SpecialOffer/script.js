import BaseComponent from '../../../BaseComponent'

export default {
  name: 'SpecialOffer',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      button: null,
      rootClass: null
    }
  }
}