import BaseComponent from '../../../BaseComponent'

export default {
  name: 'ImageText',
  extends: BaseComponent,
  props: [],
  data () {
    return {
      title: null,
      description: null,
      link: null,
      link_text: null,
      image: null,
      root_class: null
    }
  },
  methods: {
    showButton () {
      return this.link !== null && this.link_text !== null
    }
  }
}