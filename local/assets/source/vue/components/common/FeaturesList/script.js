import BaseComponent from '../../../BaseComponent'

export default {
  name: 'FeaturesList',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      rates: null,
      type: null
    }
  },
  computed: {
    init () {
      if (this.type == 'dedicated-server') {
        document.querySelectorAll('table.decorated-table tr').forEach((elem) => {
          let arrayTD = elem.querySelectorAll('td')
          arrayTD[1].style.padding = '20px 0'
          arrayTD[3].style.padding = '20px 0'
          arrayTD[6].style.padding = '20px 0'
          arrayTD[7].style.padding = '20px 20px'
        })
      }
    }
  }
}
