import BaseComponent from '../../../BaseComponent'

export default {
  name: 'Banner404',
  extends: BaseComponent,
  data () {
    return {
      img: null,
      title: null,
      description: null
    }
  }
}