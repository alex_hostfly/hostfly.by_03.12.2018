import Banner from '../Banner/component.vue'
import BaseComponent from '../../../BaseComponent'

export default {
  name: 'ButtonBanner',
  extends: BaseComponent,
  components: {
    Banner
  },
  data () {
    return {
      title: null,
      description: null,
      button: null,
      bannerClass: null,
      img: null
    }
  }
}