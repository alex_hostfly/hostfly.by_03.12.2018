import BaseComponent from '../../../BaseComponent'

export default {
  name: 'Banner',
  extends: BaseComponent,
  props: ['title', 'description'],
  data () {
    return {}
  }
}