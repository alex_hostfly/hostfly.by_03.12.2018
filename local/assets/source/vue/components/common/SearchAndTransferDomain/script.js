import SearchAndTransferDomainResult from '../SearchAndTransferDomainResult/component.vue'
import BaseComponent from '../../../BaseComponent'

export default {
  name: 'SearchAndTransferDomain',
  extends: BaseComponent,
  props: [],
  components: {
    SearchAndTransferDomainResult
  },
  data () {
    return {
      open: false,
      rootClass: null,
      domains: null
    }
  },
  methods: {
    getIdDomain (index) {
      return `domain-${index}`
    },

    toggleDomains () {
      this.open = !this.open
    }
  }
}