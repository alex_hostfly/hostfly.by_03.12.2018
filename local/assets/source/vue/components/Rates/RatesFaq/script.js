import BaseComponent from '../../../BaseComponent'

export default {
  name: 'RatesFaq',
  extends: BaseComponent,
  data () {
    return {
      whiteSectionLast: null,
      showTopBackground: null,
      questions: null
    }
  }
}