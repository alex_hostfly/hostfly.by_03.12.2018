import BaseComponent from '../../../BaseComponent'

export default {
  name: 'RatesRecommendations',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      list: null,
      imageSrc: null,
      button: null
    }
  }
}
