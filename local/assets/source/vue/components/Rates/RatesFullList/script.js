import BaseComponent from '../../../BaseComponent'

export default {
  name: 'RatesFullList',
  extends: BaseComponent,
  data () {
    return {
      rates: null,
      priceDictionary: null,
      currentPeriodMode: null,
      currentRates: [],
      periods: [],
      activePeriods: [],
    }
  },
  methods: {
    updateTabs() {
      Object.keys(this.priceDictionary).forEach((key) => {
        if(this.activePeriods[key]) {
          this.periods.push(key)
        }
      })
    },
    setPeriodMode(periodMode = 'monthly') {
      this.currentPeriodMode = periodMode
      this.updateItems()
    },
    updateItems() {

      let rateIds = []

      Object.keys(this.activePeriods[this.currentPeriodMode]).forEach((key) => {
        rateIds.push(key)
      })

      this.currentRates = this.getRatesByIds(rateIds)
    },
    calculatePrices() {
      Object.keys(this.rates).forEach((key) => {

        let rate = this.rates[key]
        let price = rate['price']['value']

        Object.keys(price).forEach((key) => {

          let priceValue = parseFloat(price[key])

          if(priceValue > 0) {
            if(!this.activePeriods[key]) {
              this.activePeriods[key] = {}
            }
            this.activePeriods[key][ rate['id'] ] = true
          }
        })

      })
    },
    getRatesByIds(rateIds) {

      let rates = [];

      Object.keys(this.rates).forEach((key) => {
        let rate = this.rates[key]
        if(rateIds.includes(rate['id'].toString())) {
          rates.push(rate)
        }
      })

      return rates
    }
  },
  mounted () {
    this.calculatePrices()
    this.updateTabs()
    this.updateItems()
  }
}