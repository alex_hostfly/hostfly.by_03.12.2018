import BaseComponent from '../../../BaseComponent'

export default {
  name: 'Slider',
  extends: BaseComponent,
  data () {
    return {
      slide: null,
      slides: null,
      current: 1,
      sliderHeight: 500
    }
  },
  computed: {
    previous() {

      if(this.current === 1) {
        return this.slides.length;
      } else {
        return (this.current - 1);
      }
    },
    next() {
      if(this.current === this.slides.length) {
        return 1;
      } else {
        return (this.current + 1);
      }
    }
  },
  methods: {
    sliderJump(slideNum) {
      this.current = slideNum;
    },
    correctSliderHeight(height){
      if(this.sliderHeight<(height+260)){
        this.sliderHeight=height+260
      }
    }
  },
  mounted() {
    let slides= this.$refs.mainSlider.children;
    for (let i = 0; i < slides.length; i++) {
      this.correctSliderHeight(slides[i].children[1].clientHeight);
    }
  }

}