import BaseComponent from '../../../BaseComponent'

export default {
  name: 'NewsList',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      elements: null,
      button: null
    }
  }
}