import BaseComponent from '../../../BaseComponent'

export default {
  name: 'RatesShortList',
  extends: BaseComponent,
  data () {
    return {
      rates: null,
      priceDictionary: null,
    }
  },
  mounted() {
  }
}
