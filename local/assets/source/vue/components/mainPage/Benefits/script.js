import BaseComponent from '../../../BaseComponent'

export default {
  name: 'Benefits',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      benefits: null,
      button: null,
      fitsClass: null
    }
  }
}