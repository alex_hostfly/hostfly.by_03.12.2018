import BaseComponent from '../../../BaseComponent'
import AchievementsPopup from '../../popups/AchievementsPopup/component.vue'

export default {
  name: 'BlocksAboutUs',
  extends: BaseComponent,
  components: {
    AchievementsPopup
  },
  data () {
    return {
      title: null,
      elements: null,
      button: null,
      currentElements: [],
      currentCount: 8,
      chunkVolume: 8,
      showButton: true,
      popup: false,
      popupsData: false
    }
  },
  methods: {
    showMore () {
      this.currentCount += this.chunkVolume
      if (this.currentCount > this.elements.length) this.showButton = false
      this.display()
    },
    display () {
      this.currentElements = []
      for (let i = 0; i < this.currentCount; i++) {
        let element = this.elements[i]
        if (element) {
          this.currentElements.push(this.elements[i])
        }
      }
    },
    openPopup (element) {
      this.popup = !this.popup
      this.popupsData = element.popup || false
    }
  },
  mounted () {
    this.display()
  }
}