import BaseComponent from '../../../BaseComponent'
import $ from 'jquery'

export default {
  name: 'Clients',
  extends: BaseComponent,
  data () {
    return {
      title: null,
      slides: null
    }
  },
  mounted(){
    $('.slider-logos').slick({
      slidesToShow: 5,
      slidesToScroll:4,
      infinite: false,
      responsive: [
        {
          breakpoint: 1359,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    })
  }
}