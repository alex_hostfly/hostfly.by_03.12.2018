import BaseComponent from '../../../BaseComponent'

export default {
  name: 'BlocksAboutUs',
  extends: BaseComponent,
  data () {
    return {
      blocks: null
    }
  }
}