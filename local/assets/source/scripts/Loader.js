import $ from 'jquery'

class Loader {
  turnOnLoader () {
    $('.footer').css('opacity', 0)
  }

  turnOffLoader () {
    setTimeout(function () {
      $('#app > div').css('opacity', 1)
    }, 700)
  }
}

export default new Loader()
