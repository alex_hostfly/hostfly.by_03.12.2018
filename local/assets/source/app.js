import Vue from 'vue'
import VueResource from 'vue-resource'
import { ComponentsCollection } from './vue/ComponentsCollection'
import { vueInvoker } from './vue/VueInvoker'
import { main } from './scripts/Main'
import Vuetify from 'vuetify'
import SvgSprite from 'vue-svg-sprite'

Vue.use(SvgSprite, {
  url: '/local/assets/images/icons/_sprite.svg',
  class: 'svg_sprite'
})
Vue.use(Vuetify)
Vue.use(VueResource)

main.initModules()

vueInvoker.prepareComponents(ComponentsCollection).then((result) => {})
