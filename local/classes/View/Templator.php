<?php

namespace Local\View;

class Templator {
	
	const TEMPLATES_PATH = '/local/views';
	
	/**
	 * @param $templateName
	 * @return bool|string
	 * @throws TemplatorException
	 */
	protected function getTemplate($templateName) {
		
		$folder = self::TEMPLATES_PATH;
		$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "$folder/$templateName.twig");
		
		if(!$template) {
			throw new TemplatorException('Не удалось получить шаблон');
		}
		
		return $template;
	}
	
	/**
	 * @param $templateName
	 * @param $context
	 * @return string|bool
	 * @throws TemplatorException
	 */
	public function render($templateName, $context) {
		
		try {
			$twig = new \Twig_Environment(new \Twig_Loader_String());
			$template = $twig->render($this->getTemplate($templateName), $context);
		} catch (\Twig_Error_Loader $exception) {
			throw new TemplatorException($exception->getMessage());
		} catch (\Twig_Error_Runtime $exception) {
			throw new TemplatorException($exception->getMessage());
		} catch (\Twig_Error_Syntax $exception) {
			throw new TemplatorException($exception->getMessage());
		}
		
		return $template;
	}
}