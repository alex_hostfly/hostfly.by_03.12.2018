<?php
namespace Local\HostFly;

use Local\HostFly\Products\ProductFactory;
use Local\HostFly\Products\ServerProduct;

class TariffBaseComponent extends HostFlyComponent {
	
	const SECTION_ID = 1;
	
	/**
	 * @param $sectionCode
	 * @return string
	 */
	public function fetchTariffPageByCode($sectionCode) {
		
		$iblockData = ServerProduct::getIblockData();
		$detailPageUrlTemplate = $iblockData['DETAIL_PAGE_URL'];
		
		if(empty($detailPageUrlTemplate) || empty($sectionCode)) {
			return null;
		}
		
		return str_replace('#CODE#', $sectionCode, $detailPageUrlTemplate);
	}
	
	/**
	 * @return $this
	 */
	protected function prepareData() {
		
		$this->arResult['data'] = [
			'rates' => $this->getRates(),
            'priceDictionary' => ServerProduct::PRICE_MODES,
            'currentPeriodMode' => 'monthly'
		];
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	protected function getMetaDataForCurrentSection() {
		
		$sections = $this->fetchSectionsIb([
			'filter' => [
				'IBLOCK_ID' => TARIFFS_IBLOCK_ID,
				'ID' => static::SECTION_ID
			],
			'select' => [
				'ID', 'IBLOCK_ID', 'NAME',
				'PROPS' => [
					'CLASSNAME',
                    'HIT'
				]
			]
		]);
		
		$section = array_shift($sections);
		
		return [
			'TITLE' => $section['NAME'],
			'CLASSNAME' => $section['PROPS']['CLASSNAME'],
		];
	}
	
	/**
	 * @return array
	 */
	protected function getRates($section_id = false) {
	    if ($section_id === false){
	        $section_id = self::SECTION_ID;
        }

        $items = [];

		$factory = new ProductFactory();
		
		$products = $factory->getProductsFromIb([
			'IBLOCK_ID' => TARIFFS_IBLOCK_ID,
			'SECTION_ID' => $section_id
		]);
		
		foreach($products as $product) {
			/* @var ServerProduct $product */
			$items[] = $product->getPreparedDataForUI();
		}

        $data = $items;

		return $data;
	}
	
	/**
	 * @return array|bool
	 */
	protected function fetchAllTariffGroups() {
		
		return $this->fetchSectionsIb([
			'select' => [
				'ID', 'NAME', 'CODE',
				'PROPS' => [
					'CLASSNAME',
					'NAME_ALT',
					'FEATURES',
				]
			],
			'filter' => [
				'ACTIVE' => 'Y',
				'IBLOCK_ID' => TARIFFS_IBLOCK_ID
			],
			'order' => [
				'SORT' => 'ASC'
			]
		]);
	}
	
	/**
	 * @param $sectionId
	 * @return array
	 */
	protected function fetchNeighborTariffGroups($sectionId) {
		
		return $this->fetchSectionsIb([
			'select' => [
				'ID', 'NAME', 'CODE', 'DESCRIPTION', 'PICTURE',
				'PROPS' => [
					'CLASSNAME',
					'NAME_ALT',
					'FEATURES'
				]
			],
			'filter' => [
				'ACTIVE' => 'Y',
				'IBLOCK_ID' => TARIFFS_IBLOCK_ID,
				'!ID' => $sectionId
			],
			'order' => [
				'SORT' => 'ASC'
			]
		]);
	}
	
	/**
	 * @return array
	 */
	protected function fetchTariffs() {
		
		$productFactory = new ProductFactory();
		
		return $productFactory->getProductsFromIb([
			'ACTIVE' => 'Y'
		]);
	}
	
	/**
	 * @return $this
	 */
	protected function fetchData() {
		
		$this->arResult['META'] = $this->getMetaDataForCurrentSection();
		
		return $this;
	}
}