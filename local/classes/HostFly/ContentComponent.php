<?php

namespace Local\HostFly;

class ContentComponent extends HostFlyComponent {
	
	/**
	 * @return $this
	 */
	protected function processRegular() {

		$this->prepareData();
		$this->prepareDataJson();

		if(!empty($this->arResult['data'])) {
			$this->renderComponent();
		}
		
		return $this;
	}
}