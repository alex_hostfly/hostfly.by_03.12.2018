<?php

namespace Local\HostFly\Products;

use Local\HostFly\HostFlyComponent;

class ProductFactory {
	
	/**
	 * @param $productsData
	 * @return array
	 */
	public function getProductsByApiData(array $productsData): array {
		
		$instances = [];
		
		foreach ($productsData as $product) {
			
			$instance = $this->getProductByApiData($product);
			
			if ($instance === null)
				continue;
			
			$instances[$instance->getPid()] = $instance;
		}
		
		return $instances;
	}
	
	/**
	 * @param $filter
	 * @return array
	 */
	public function getProductsFromIb($filter) {
		
		$componentObj = new HostFlyComponent();
		
		$filter['IBLOCK_ID'] = ServerProduct::getIblockId();
		
		$elementsArray = $componentObj->getElementsIB([
			'filter' => $filter,
			'select' => ServerProduct::getFieldModel()
		]);
		
		$productsArray = [];
		
		foreach ($elementsArray as $id => $ibData) {
			$productsArray[$id] = $this->getProductByIbData($ibData);
		}
		
		return $productsArray;
	}
	
	/**
	 * @param $ibData
	 * @return ServerProduct
	 */
	public function getProductByIbData($ibData) {
		return new ServerProduct([
			'ibData' => $ibData
		]);
	}
	
	/**
	 * @param $apiData
	 * @return ServerProduct
	 */
	public function getProductByApiData(array $apiData) {
		return new ServerProduct([
			'apiData' => $apiData
		]);
	}
	
}