<?php

namespace Local\HostFly\Products;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Local\HostFly\Exchange\Api;
use Local\HostFly\Exchange\ApiInternalException;

class ServerProduct
{

    const IB_CODE = TARIFFS_IBLOCK_CODE;

    const PRICE_MODES = [
        'monthly' => [
            'sort' => 1,
            'title' => '1 месяц',
            'period' => 'месяц',
        ],
        'quarterly' => [
            'sort' => 2,
            'title' => '1 квартал',
            'period' => 'квартал'
        ],
        'semiannually' => [
            'sort' => 3,
            'title' => '1 раз в полгода',
            'period' => 'полгода'
        ],
        'annually' => [
            'sort' => 4,
            'title' => '1 год',
            'period' => 'год'
        ],
        'biennially' => [
            'sort' => 5,
            'title' => '2 года',
            'period' => 'два года'
        ],
        'triennially' => [
            'sort' => 6,
            'title' => '3 года',
            'period' => 'три года'
        ],
    ];

    const FEES_PRICE_MODES = [
        'msetupfee' => [
            'sort' => 1,
            'title' => '1 месяц',
            'period' => 'месяц'
        ],
        'qsetupfee' => [
            'sort' => 2,
            'title' => '1 квартал',
            'period' => 'квартал'
        ],
        'ssetupfee' => [
            'sort' => 3,
            'title' => '1 раз в полгода',
            'period' => 'полгода'
        ],
        'asetupfee' => [
            'sort' => 4,
            'title' => '1 год',
            'period' => 'год'
        ],
        'bsetupfee' => [
            'sort' => 5,
            'title' => '2 года',
            'period' => 'два года'
        ],
        'tsetupfee' => [
            'sort' => 6,
            'title' => '3 года',
            'period' => 'три года'
        ],
    ];
    const API_TO_IB = [
        'pid' => 'PID',
        'name' => 'NAME',
    ];
    protected static $fieldModel = [
        'ID',
        'NAME',
        'DETAIL_TEXT',
        'IBLOCK_SECTION_ID',
        'CODE',
        'PROPS' => [
            // this properties will be added on the fly
        ]
    ];
    protected static $iblockData;
    protected static $propertyData;
    protected static $buyLinkTemplate = '/cart.php?a=add&pid=#PID#';
    public $exists;
    protected $fields;

    /**
     * BaseServer constructor.
     * @param array $params
     */
    public function __construct($params = [])
    {

        if (!empty($params['apiData'])) {
            $this->setFieldsByApi($params['apiData']);
        } elseif (!empty($params['ibData'])) {
            $this->setFieldsByIb($params['ibData']);
        }
    }

    /**
     * @param $apiData
     * @return $this
     */
    public function setFieldsByApi($apiData)
    {

        foreach (self::API_TO_IB as $apiCode => $ibCode) {

            if ($apiCode === 'pricing') {
                continue;
            }

            $value = $apiData[$apiCode];
            $this->setField($ibCode, $value);
        }

        if (!empty($apiData['pricing']['BYN'])) {
            $this->setField('PRICE', json_encode($apiData['pricing']['BYN']));
        }

        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    protected function setField($name, $value)
    {

        if (in_array($name, self::getFieldModel())) {
            $this->fields[$name] = $value;
        } elseif (in_array($name, self::getFieldModel()['PROPS'])) {
            $this->fields['PROPS'][$name] = $value;
        }

        return $this;
    }

    /**
     * @return array
     */
    public static function getFieldModel()
    {

        if (!self::$fieldModel['PROPS']) {
            $fieldData = self::getFieldDataStatic();
            self::$fieldModel['PROPS'] = array_keys($fieldData);
        }

        return self::$fieldModel;
    }

    public static function getFieldDataStatic(): array
    {
        $instance = new self();
        return $instance->getFieldData();
    }

    /**
     * @return array
     */
    public function getFieldData(): array
    {

        if (self::$propertyData === null) {

            $fields = [];

            $filter = [
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => $this->getIblockId()
            ];

            $iblockFieldsDb = \CIBlockProperty::GetList([
                'SORT' => 'ASC'
            ], $filter);

            while ($field = $iblockFieldsDb->GetNext()) {
                $fields[$field['CODE']] = $field;
            }

            self::$propertyData = $fields;
        }

        return self::$propertyData;
    }

    /**
     * @return int
     */
    public static function getIblockId(): int
    {
        return TARIFFS_IBLOCK_ID;
    }

    /**
     * @param $ibData
     * @return $this
     */
    public function setFieldsByIb($ibData)
    {

        $this->fields = $ibData;

        $this->exists = true;

        return $this;
    }

    /**
     * @return array
     */
    public static function getIblockData(): array
    {

        if (self::$iblockData === null) {

            $filter = [
                '=CODE' => self::IB_CODE
            ];

            $select = [
                'ID',
                'NAME',
                'CODE',
                'SECTION_PAGE_URL',
                'DETAIL_PAGE_URL'
            ];

            try {

                self::$iblockData = \Bitrix\Iblock\IblockTable::getList([
                    'select' => $select,
                    'filter' => $filter
                ])->fetch();

            } catch (ObjectPropertyException $e) {
                return null;
            } catch (ArgumentException $e) {
                return null;
            } catch (SystemException $e) {
                return null;
            }
        }

        return self::$iblockData;
    }



    /**
     * @return bool
     * @throws ServerSaveException
     */
    public function save()
    {

        $iblockElement = new \CIBlockElement();

        $defaultProps = $this->getDefaultFields();
        $customProps = $this->getCustomFields();

        if ($this->isExists()) {

            if (!empty($customProps)) {
                \CIBlockElement::SetPropertyValuesEx($this->getId(), $this->getIblockId(), $customProps);
            }

            if (!$iblockElement->Update($this->getId(), $defaultProps)) {
                throw new ServerSaveException($iblockElement->LAST_ERROR);
            }

        } else {

            $addProps = $defaultProps;
            $addProps['PROPERTY_VALUES'] = $customProps;
            $addProps['IBLOCK_ID'] = $this->getIblockId();

            $id = $iblockElement->Add($addProps);

            if (!$id) {
                throw new ServerSaveException($iblockElement->LAST_ERROR);
            }

            $this->setId($id);
        }

        $this->update();

        return $this->getId();
    }

    /**
     * @return array
     */
    protected function getDefaultFields()
    {

        $fields = $this->fields;

        unset($fields['PROPS']);

        return $fields;
    }

    /**
     * @return array
     */
    protected function getCustomFields()
    {
        return $this->fields['PROPS'];
    }

    /**
     * @return bool
     */
    protected function isExists(): bool
    {

        if ($this->exists === null) {

            $filter = [
                '=IBLOCK_CODE' => self::IB_CODE,
                '=PROPERTY_PID' => $this->getPid()
            ];

            $select = [
                'ID'
            ];

            $id = \CIBlockElement::GetList([], $filter, false, false, $select)->Fetch()['ID'];

            $this->exists = (bool)$id;

            if (intval($id) > 0) {
                $this->setId($id);
            }
        }

        return $this->exists;
    }

    /**
     * @return int
     */
    public function getPid(): int
    {
        return (int)$this->fields['PROPS']['PID'];
    }

    /**
     * @param $id
     * @return $this
     */
    protected function setId($id)
    {

        $this->fields['ID'] = (int)$id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->fields['ID'];
    }

    /**
     * @return $this
     */
    public function update()
    {

        $ibData = reset(\Local\Base\IblockComponent::getElements([
            'filter' => [
                '=ID' => $this->getId(),
            ],
            'select' => self::getFieldModel()
        ]));

        $this->exists = true;

        $this->setFieldsByIb($ibData);

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->fields['NAME'];
    }

    public function getParentSectionId()
    {
        return $this->fields['IBLOCK_SECTION_ID'];
    }

    /**
     * @return array
     */
    public function getPreparedDataForUI()
    {

        $buyLink = $this->getBuyLink();
        $priceData = $this->getPriceData();

        $discountValue = $this->getField('DISCOUNT');
        $discountData = null;

        if ($discountValue) {
            $discountData = $priceData;
            $discountData['value'] = $discountValue;
        }

        $element = [
            'id' => $this->getId(),
            'period' => $priceData['periodTitle'],
            'name' => $this->getField('NAME'),
            'price' => $priceData,
            'discountPrice' => $discountData,
            'description' => $this->getField('DETAIL_TEXT'),
            'hit' => $this->getField('HIT') === 'Y',
            'buttonToOrder' => [
                'text' => 'Заказать',
                'href' => $buyLink
            ],
            'buttonToTry' => [
                'text' => 'Попробовать бесплатно',
                'href' => $buyLink
            ],
            'color' => $this->getColorClassName()
        ];

        $fieldsData = $this->getFieldData();
        $features = $this->getFeaturesFieldNames();

        foreach ($features as $fieldName) {

            $value = $this->getField($fieldName);

            if ($value) {
                $element['features'][$fieldName] = [
                    'name' => $fieldsData[$fieldName]['NAME'],
                    'value' => $value
                ];
            }
        }

        return $element;
    }

    /**
     * @return string
     */
    public function getBuyLink()
    {

        try {
            $api = Api::getInstance();
        } catch (ApiInternalException $exception) {
            return null;
        }

        $gateway = $api->getGateway();
        $pid = $this->getField('PID');

        return $gateway . str_replace('#PID#', $pid, self::$buyLinkTemplate);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getField($name)
    {

        if (in_array($name, self::getFieldModel())) {
            return $this->fields[$name];
        } elseif (in_array($name, self::getFieldModel()['PROPS'])) {
            return $this->fields['PROPS'][$name];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getPriceData(): array
    {
        $price = json_decode($this->getField('PRICE'), true);

        if (!$price) {
            return [];
        }

        return [
            'value' => $price,
            'currency' => 'рублей',
        ];
    }

    /**
     * @return array
     */
    public function getFeaturesFieldNames()
    {
        $props = array_merge((array)$this->getDefaultFieldModel(), (array)$this->getCustomFieldModel());

        return array_filter($props, function ($element) {
            return strpos($element, 'SPEC') !== false;
        });
    }

    /**
     * @return array
     */
    public function getDefaultFieldModel()
    {
        $fields = self::getFieldModel();
        unset($fields['PROPS']);
        return $fields;
    }

    /**
     * @return mixed
     */
    public function getCustomFieldModel()
    {
        return self::getFieldModel()['PROPS'];
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getField('DESCRIPTION');
    }

    /**
     * @param $name
     * @return $this
     */
    protected function setName($name)
    {
        $this->fields['NAME'] = (string)$name;

        return $this;
    }

    /**
     * @return array|null
     */
    protected function getSectionData()
    {
        $sectionId = $this->getField('IBLOCK_SECTION_ID');

        if(!$sectionId) {
            return null;
        }

        $section = \CIBlockSection::GetList(['SORT' => 'ASC'], [
            'ID' => $sectionId,
            'IBLOCK_ID' => TARIFFS_IBLOCK_ID
        ], false, [
            'UF_*'
        ])->Fetch();

        return $section;
    }

    /**
     * @return string|null
     */
    public function getColorClassName()
    {
        $section = $this->getSectionData();

        return $section['UF_CLASSNAME'];
    }
}