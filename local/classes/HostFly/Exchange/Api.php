<?php

namespace Local\HostFly\Exchange;

final class Api {
	
	private static $instance;
	private $gateway;
	private $path;
	private $secret;
	private $identifier;
	
	/**
	 * Api constructor.
	 * @throws ApiInternalException
	 */
	private function __construct() {
		
		try {
			$config = $this->getConfig();
			$this->setSettings($config);
		} catch (ApiConfigException $exception) {
			throw new ApiInternalException($exception->getMessage());
		}
	}
	
	/**
	 * @throws ApiConfigException
	 */
	private function getConfig() {
		
		$json = file_get_contents(PROJECT_ROOT . '/configs/api.json');
		
		if (!$json) {
			throw new ApiConfigException("Missing config JSON for API");
		}
		
		$config = json_decode($json, true);
		
		if (!$config) {
			throw new ApiConfigException("Can't read config JSON for API");
		}
		
		return $config;
	}
	
	/**
	 * @param array $config
	 * @return Api
	 * @throws ApiConfigException
	 */
	private function setSettings($config = []) {
		
		if (empty($config)) {
			throw new ApiConfigException('Unable to set API connection settings from config');
		}
		
		$this->gateway = $config['gateway'];
		$this->identifier = $config['identifier'];
		$this->path = $config['path'];
		$this->secret = $config['secret'];
		
		return $this;
	}
	
	/**
	 * @return Api
	 * @throws ApiInternalException
	 */
	public static function getInstance() {
		
		if (self::$instance === null) {
			self::$instance = new Api();
		}
		
		return self::$instance;
	}
	
	/**
	 * @param $data
	 * @return array
	 * @throws ApiExternalException
	 */
	public function GetProducts($data = []) {
		
		$result = $this->doAction('GetProducts', $data);
		
		if (empty($result['products'])) {
			throw new ApiExternalException('Missing "products" key');
		}
		
		if (empty($result['products']['product'])) {
			throw new ApiExternalException('Missing "products" key');
		}
		
		return $result['products']['product'];
	}
	
	/**
	 * @param $actionName
	 * @param $data
	 * @return array
	 * @throws ApiExternalException
	 */
	private function doAction($actionName = null, $data = []) {
		
		$data['action'] = $actionName;
		$data['responsetype'] = 'json';
		$data['identifier'] = $this->identifier;
		$data['secret'] = $this->secret;
		
		try {
			$data = $this->request($this->getApiEntrypoint(), $data);
		} catch (ApiConnectionException $exception) {
			throw new ApiExternalException($exception->getMessage());
		}
		
		if ($data['result'] === 'error') {
			throw new ApiExternalException($data['message']);
		}
		
		return $data;
	}
	
	/**
	 * @param $url
	 * @param $data
	 * @return array
	 * @throws ApiConnectionException
	 */
	private function request($url, $data) {
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		
		$response = curl_exec($ch);
		
		curl_close($ch);
		
		if (curl_error($ch)) {
			throw new ApiConnectionException('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
		}
		
		$data = json_decode($response, true);
		
		if (!$data) {
			throw new ApiConnectionException("Can't read response JSON");
		}
		
		return $data;
	}
	
	/**
	 * @return string
	 */
	private function getApiEntrypoint() {
		return $this->gateway . $this->path;
	}
	
	/**
	 * @return string
	 */
	public function getGateway() {
		return $this->gateway;
	}
}