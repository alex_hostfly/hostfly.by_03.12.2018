<?php
namespace Local\HostFly;

class TariffMainComponent extends TariffBaseComponent {
	
	/**
	 * @return $this
	 */
	protected function processRegular() {
		
		global $APPLICATION;
		
		parent::processRegular();
		
		$APPLICATION->SetPageProperty('APP_CLASS', $this->arResult['META']['CLASSNAME']);
		$APPLICATION->SetTitle($this->arResult['META']['TITLE']);
		
		return $this;
	}
	
	/**
	 * @return $this
	 */
	protected function fetchData() {
		$this->arResult['META'] = $this->getMetaDataForCurrentSection();
		
		return $this;
	}
}