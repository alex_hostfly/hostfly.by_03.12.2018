<?
declare(strict_types=1);

namespace Local\Base;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

class IblockComponent extends BaseComponent {
	
	/**
	 * CIblockComponent constructor.
	 * @param \CBitrixComponent|null $component
	 */
	public function __construct(\CBitrixComponent $component = null) {
		
		try {
			Loader::includeModule('iblock');
		} catch(\Exception $exception) {
		
		}
		
		parent::__construct($component);
	}
	
	/**
	 * @param $params
	 * @return array|bool
	 */
	public static function getElements($params) {
		$object = new self();
		return $object->getElementsIB($params);
	}
	
	/**
	 * @param $params
	 * @return array|bool
	 */
	public static function getSections($params) {
		$object = new self();
		return $object->getElementsIB($params);
	}
	
	/**
	 * @param array $params
	 * @return array|bool
	 */
	public function getElementsIB($params = []) {
		
		if(empty($params)) {
			return false;
		}
		
		$data = [];
		
		$filter = $params['filter'] ?? [];
		$order  = $params['order'] ?? ['SORT' => 'ASC'];
		$select = $params['select'] ?? ['ID'];
		$nav = $params['nav'] ?? false;
		$groupBy = $params['group'] ?? false;

		if (!in_array('CODE', $select)) {
            $select[] = 'CODE';
        }

        if (!in_array('ID', $select)) {
            $select[] = 'ID';
        }

		$originalSelect = $select;
		
		$customParams = $params['params'] ?? [];
		$customKey = isset($customParams['key']) && !empty($customParams['key']) ? $customParams['key'] : false;
		
		if ($filter['PROPS']) {
            $propsFilter = $filter['PROPS'];
            unset($filter['PROPS']);

            foreach ($propsFilter as $propName => $propValue) {
                $filter["PROPERTY_$propName"] = $propValue;
            }
        }
		
		$propsSelect = false;
		
		if($select['PROPS']) {
			
			$propsSelect = $select['PROPS'];
			unset($select['PROPS']);
			
			foreach($propsSelect as $propName) {
				$select[] = "PROPERTY_$propName";
			}
		}

        $filterForIb = null;
		if (isset($filter['IBLOCK_CODE'])) {
            $filterForIb = ['CODE' => $filter['IBLOCK_CODE']];
        } elseif (isset($filter['IBLOCK_ID'])) {
            $filterForIb = ['ID' => $filter['IBLOCK_ID']];
        }

        if($filterForIb) {
            $iblockData = $this->getIbData($filterForIb);
        }

		$result = \CIBlockElement::GetList($order, $filter, $groupBy, $nav, $select);
		
		while ($element = $result->Fetch()) {
			
			$resultElement = [
				'ID' => $element['ID']
			];
			
			foreach($originalSelect as $index => $field) {
				
				if($index === 'PROPS') {
					continue;
				}
				
				$resultElement[$field] = $element[$field];
			}
			
			if($propsSelect) {
				
				$resultElement['PROPS'] = [];
				
				foreach($propsSelect as $propName) {
					$valueKey = "PROPERTY_{$propName}_VALUE";
					$resultElement['PROPS'][$propName] = $element[$valueKey];
				}
			}
			
			if($resultElement['PREVIEW_PICTURE']) {
				$this->addFile($resultElement['PREVIEW_PICTURE']);
			}
			
			if($resultElement['DETAIL_PICTURE']) {
				$this->addFile($resultElement['DETAIL_PICTURE']);
			}
			
			$key = $element['ID'];
			
			if($customKey && isset($resultElement['PROPS'][$customKey])) {
				$key = $customKey;
			}

			if ($iblockData) {
			    if ($iblockData['DETAIL_PAGE_URL']) {
                    $resultElement['DETAIL_PAGE_URL'] = str_replace('#SECTION_CODE#',
                        $resultElement['CODE'],
                        $iblockData['DETAIL_PAGE_URL']);
                }
            }

			$data[$key] = $resultElement;
		}

		return $data;
	}

    /**
     * @param $filter
     * @return mixed
     */
    protected function getIbData($filter)
    {
        try {
            return \Bitrix\Iblock\IblockTable::getList([
                'filter' => $filter
            ])->fetch();
        } catch (ArgumentException $e) {
            return false;
        } catch (SystemException $e) {
	        return false;
        }
    }

    /**
     * @param $elementsParams
     * @param $sectionsParams
     * @return array|bool
     */
	public function fetchDataIb($elementsParams, $sectionsParams)
    {
        if (empty($elementsParams) || empty($sectionsParams)) {
            return false;
        }

        $dataIb = [];

        $sections = $this->fetchSectionsIb($sectionsParams);

        if (!isset($elementsParams['filter']['SECTION_ID'])) {
            $elementsParams['filter']['SECTION_ID'] = array_keys($sections);
        }

        if (!in_array('IBLOCK_SECTION_ID', $elementsParams['select'])) {
            $elementsParams['select'][] = 'IBLOCK_SECTION_ID';
        }

        $elements = $this->getElementsIB($elementsParams);

        foreach ($sections as $sectionKey => $sectionData) {

            $dataIb[$sectionKey] = $sectionData;

            foreach ($elements as $element) {

                if ($element['IBLOCK_SECTION_ID'] == $sectionKey) {
                    $dataIb[$sectionKey]['ELEMENTS'][] = $element;
                }
            }
        }

        return $dataIb;
    }

    /**
     * @param array $dataWithoutPictures
     * @return array
     */
    protected function getDataWithPictures(array $dataWithoutPictures):array
    {
        foreach ($dataWithoutPictures as $sectionKey => $sectionData) {

            foreach ($sectionData['ELEMENTS'] as $elementKey => $elementData) {

                if ($elementData['PREVIEW_PICTURE']) {
                    $dataWithoutPictures[$sectionKey]['ELEMENTS'][$elementKey]['PREVIEW_PICTURE'] = $this->arResult['FILES'][$elementData['PREVIEW_PICTURE']]
                        ?? null;
                }

                if ($elementData['DETAIL_PICTURE']) {
                    $dataWithoutPictures[$sectionKey]['ELEMENTS'][$elementKey]['DETAIL_PICTURE'] = $this->arResult['FILES'][$elementData['DETAIL_PICTURE']]
                        ?? null;
                }
            }
        }

        $dataWithPictures = $dataWithoutPictures;

        return $dataWithPictures;
    }

	/**
	 * @param $params
	 * @return array|bool
	 */
	public function fetchSectionsIb($params) {
		
		if(empty($params)) {
			return false;
		}
		
		$filter = $params['filter'] ?? [];
		$order = $params['order'] ?? ['SORT' => 'ASC'];
		$select = $params['select'] ?? [];
		$originalSelect = $select;

		if (!in_array('ID', $select)) {
		    $select[] = 'ID';
        }

		$customParams = $params['params'] ?? [];
		$customKey = isset($customParams['key']) && !empty($customParams['key']) ? $customParams['key'] : false;
		
		if($filter['PROPS']) {
			$propsFilter = $filter['PROPS'];
			unset($filter['PROPS']);
			
			foreach($propsFilter as $propName => $propValue) {
				$filter["UF_$propName"] = $propValue;
			}
		}
		
		$propsSelect = false;
		
		if($select['PROPS']) {
			
			$propsSelect = $select['PROPS'];
			unset($select['PROPS']);
			
			foreach($propsSelect as $propName) {
				$select[] = "UF_$propName";
			}
		}
		
		$result = \CIBlockSection::GetList($order, $filter, false, $select);
		
		$data = [];
		
		while($element = $result->Fetch()) {
			
			$resultElement = [
				'ID' => $element['ID']
			];
			
			foreach($originalSelect as $index => $field) {

				if($index === 'PROPS') {
					continue;
				}
				
				$resultElement[$field] = $element[$field];
			}
			
			if($propsSelect) {
				
				$resultElement['PROPS'] = [];
				
				foreach($propsSelect as $propName) {
					$valueKey = "UF_{$propName}";
					$resultElement['PROPS'][$propName] = $element[$valueKey];
				}
			}
			
			$key = $element['ID'];
			
			if($customKey && isset($resultElement['PROPS'][$customKey])) {
				$key = $customKey;
			}
			
			if(!empty($element['PICTURE'])) {
				$this->addFile($element['PICTURE']);
			}
			
			$data[$key] = $resultElement;
		}
		
		return $data;
	}

    /**
     * @param array $neededSeo
     * @param array $seoData
     * @return array
     */
    protected function getSeoDataForClient(array $neededSeo, array $seoData):array
    {
        $result = [];
        foreach ($seoData as $metaKey => $metaValue) {
            if (in_array($metaKey, $neededSeo)) {
                $newKey = $this->convertBitrixKeyStringToCamelCase($metaKey);
                $result[$newKey] = $metaValue;
            }
        }
        return $result;
    }

    /**
     * @param string $key
     * @return string
     */
    protected function convertBitrixKeyStringToCamelCase(string $key):string
    {
        if (strpos($key, '_') !== false) {

            $newKey = '';
            $keyParts = explode('_', $key);

            foreach ($keyParts as $index => $part) {
                if ($index > 0) {
                    $newKey .= ucfirst(strtolower($part));
                } else {
                    $newKey .= strtolower($part);
                }
            }

            return $newKey;
        } else {
            return $key;
        }
    }

    /**
     * @param $iBlockId
     * @param $elementId
     * @return array
     */
    protected function getSeoData($iBlockId, $elementId):array
    {
        if (empty($iBlockId) || empty($elementId)) return [];

        $seoObject = new \Bitrix\Iblock\InheritedProperty\ElementValues($iBlockId, $elementId);

        $metasData =  $seoObject->getValues();

        $result = [];

        foreach ($metasData as $metaKey => $metaValue) {
            $neededString = 'ELEMENT_';
            if (strpos($metaKey, $neededString) !== false) {
                $newKey = str_replace($neededString, '', $metaKey);
            } else {
                $newKey = $metaKey;
            }
            $result[$newKey] = $metaValue;
        }

        return $result;
    }
}