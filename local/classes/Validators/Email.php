<?php declare(strict_types=1);

namespace Local\Validators;

use Local\Base\Validator;

class Email extends Validator {
	
	public static function validate($value) {
		
		if(filter_var($value, FILTER_VALIDATE_EMAIL)) {
			return true;
		} else {
			return [
				'error' => 'введите корретный e-mail'
			];
		}
	}
}