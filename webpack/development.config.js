const path = require('path')
const merge = require('webpack-merge')
const baseConfig = require('./base.config')

const config = {
  devtool: 'source-map',
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'eslint-loader',
      },
    ]
  },
  watchOptions: {
    poll: 1000,
    ignored: [
      '/bitrix/',
      '/upload/',
      '/sites/',
      '/data/',
      '/node_modules/',
      '/vendor/'
    ]
  }
}

module.exports = merge(baseConfig, config)