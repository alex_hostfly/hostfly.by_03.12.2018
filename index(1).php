<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Hostfly.by – белорусский хостинг-провайдер, виртуальный хостинг, VPS, аренда серверов, регистрация доменов.");
$APPLICATION->SetTitle("Главная");
?><?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"BANNER_CLASS" => "banner-top",
		"BUTTON_LINK" => "#",
		"BUTTON_TEXT" => "Посмотреть тарифы",
		"BUTTON_TYPE" => "default",
		"COMPONENT_TEMPLATE" => ".default",
		"DESCRIPTION" => "Мы знаем, что требуется от качественного хостинга, поэтому гарантируем вам высокую скорость работы размещаемых сайтов, сервисов и приложений, низкие цены, отказоустойчивость инфраструктуры, безопасность и круглосуточную поддержку наших клиентов.",
		"IMG" => "/local/assets/images/bg/bg_main.png",
		"TEXT_BEFORE_BUTTON" => "",
		"TITLE" => "Такой, каким и должен быть хостинг: облачный, быстрый, надежный"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"tariff:PromoList",
	".default",
Array()
);?>
<?$APPLICATION->IncludeComponent(
	"content:Benefits", 
	".default", 
	array(
		"BENEFITS_1_DESCRIPTION" => "Встроенный конструктор позволяет создать профессиональный сайт за 5 минут без навыков в дизайне и программировании, просто перетаскивая готовые блоки.",
		"BENEFITS_1_IMG" => "/local/assets/images/icons/customise_site.svg",
		"BENEFITS_1_TITLE" => "Конструктор сайтов",
		"BENEFITS_2_DESCRIPTION" => "Мы доступны в любое время суток, включая выходные и праздничные дни. Телефон, онлайн-чат, электронная почта. Всегда на связи при первой необходимости.",
		"BENEFITS_2_IMG" => "/local/assets/images/icons/pretty-link-leaf.svg",
		"BENEFITS_2_TITLE" => "Поддержка 24/7",
		"BENEFITS_3_DESCRIPTION" => "Используем зарекомендовавшие себя во всем мире сервисы cPanel и WHM. Вы легко сможете управлять услугами, счетами, доменами, сайтами  или сервером без посторонней помощи.",
		"BENEFITS_3_IMG" => "/local/assets/images/icons/windows.svg",
		"BENEFITS_3_TITLE" => "Простое управление",
		"BENEFITS_4_DESCRIPTION" => "Перенесем ваши сайты на наш хостинг так, что вы даже этого не заметите. Все расходы берем на себя. Сохранность конфиденциальных данных гарантируем.",
		"BENEFITS_4_IMG" => "/local/assets/images/icons/home.svg",
		"BENEFITS_4_TITLE" => "Бесплатный переезд",
		"BENEFITS_COUNT" => "4",
		"BUTTON_LINK" => "/about/who-are-we/",
		"BUTTON_TEXT" => "Подробнее о нас",
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Почему нас выбирают"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"content:Slider", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"SLIDES_1_BUTTON_LINK" => "#hosting1",
		"SLIDES_1_BUTTON_TEXT" => "Перейти к тарифным планам",
		"SLIDES_1_DESCRIPTION" => "Проверьте качество нашего хостинга в течение бесплатного тестового периода — 14 дней. Зарегистрируйтесь сейчас и оцените скорость, простоту управления и надежность веб-хостинга или виртуального сервера в нашем облаке. Без оплаты и привязки банковской карты.",
		"SLIDES_1_IMG" => "/local/assets/images/markup_image/14days.png",
		"SLIDES_1_TITLE" => "Тестовый период",
		"SLIDES_2_BUTTON_LINK" => "#hosting1",
		"SLIDES_2_BUTTON_TEXT" => "Перейти к тарифным планам",
		"SLIDES_2_DESCRIPTION" => "Ваши данные всегда сохранены. Мы проводим ежедневное резервное копирование, используем надежную отказоустойчивую систему зеркального многопотокового хранения данных и автоматически переключаем ваш сайт на  другой сервер в случае аппаратных проблем.",
		"SLIDES_2_IMG" => "/local/assets/images/markup_image/slide2.png",
		"SLIDES_2_TITLE" => "Резервное копирование",
		"SLIDES_3_BUTTON_LINK" => "#hosting1",
		"SLIDES_3_BUTTON_TEXT" => "Перейти к тарифным планам",
		"SLIDES_3_DESCRIPTION" => "Каждый тарифный план включает в себя гарантированный доступный объем оперативной памяти и процессорного времени, в разы превосходящий потребности сайтов малого и среднего бизнеса, а также простую миграцию между тарифными планами в облаке. Что это значит для нашего клиента? Заказывайте рекламу в Интернете, продвигайте свой бренд в социальных сетях и поисковых системах, добавляйте медийный контент, одним словом, развивайте свой бизнес, не беспокоясь о доступности вашего сайта в периоды пиковых нагрузок. Сайт всегда будет онлайн.",
		"SLIDES_3_IMG" => "/local/assets/images/markup_image/slide1.png",
		"SLIDES_3_TITLE" => "Увеличение мощности",
		"SLIDES_COUNT" => "4",
		"SLIDES_4_TITLE" => "Бесплатный SSL-сертификат",
		"SLIDES_4_DESCRIPTION" => "При выборе любого тарифа, вы получаете бесплатный SSL-сертификат Let’s Encrypt , что гарантирует  вашему сайту дополнительную защиту от взлома при передаче данных, а также положительно влияет на продвижение сайта в поисковых системах Google, Yandex, Tut.by",
		"SLIDES_4_IMG" => "/local/assets/images/markup_image/slide2.png",
		"SLIDES_4_BUTTON_TEXT" => "Бесплатный SSL-сертификат",
		"SLIDES_4_BUTTON_LINK" => "#"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"news:NewsShortList", 
	".default", 
	array(
		"BUTTON_HREF" => "/about/news/",
		"BUTTON_TEXT" => "Больше новостей",
		"COMPONENT_TEMPLATE" => ".default",
		"NEWS_TITLE" => "Новости и акции HOSTFLY"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>