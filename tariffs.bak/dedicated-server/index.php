<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Hostfly.by предоставляет выделенные сервера для малого и среднего бизнеса по доступной цене.");
$APPLICATION->SetPageProperty("title", "Выделенный сервер с полными правами администрирования | Hostfly.by");
$APPLICATION->SetTitle("Страница");
?>

<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BANNER_CLASS" => "banner-top",
		"IMG" => "/local/assets/images/bg/bg_server.png",
		"TITLE" => "Выделенный сервер  — полные права администрирования, независимость и безопасность.",
		"DESCRIPTION" => "Выделенный сервер, полностью управляемый со стороны клиента, идеально подходит в том случае, если вашим системам требуется особая настройка и постоянный контроль. Наши сервера с одним и двумя процессорами Intel Xeon E3/E5 работают с высокой производительностью не опустошая при этом ваши счета.",
		"TEXT_BEFORE_BUTTON" => "",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => "",
		"BUTTON_TYPE" => ""
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"tariff:DedicatedServerList",
	".default",
	array(),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"content:SpecialOffer", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Летние цены на виртуальный сервер. Предложение действительно 14 дней",
		"BUTTON_TEXT" => "Подробнее",
		"BUTTON_LINK" => "#"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"faq:commonList",
	".default",
	array(
		'SECTION_ID' => '4'
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"content:Recommendations", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Тарифные планы имеют следующие особенности:",
		"BUTTON_TEXT" => "Список тарифов",
		"BUTTON_LINK" => "#",
		"BENEFITS_LIST" => array(
			0 => "постоянная работа команды поддержки 24/7",
			1 => "веб-интерфейс CentOS Web Panel",
			2 => "до 3GB резервной памяти для DDoS Protection",
			3 => "ежедневное резервное копирование",
			4 => "5 выделенных IP-адресов",
			5 => "полные права на администрирование сервера",
			6 => "интерфейс IPMI, KVM консоль и возможность перезагрузки сервера",
			7 => "неограниченное количество доменов",
			8 => "доступ SSH",
			9 => "установка любой операционной системы",
			10 => "интеграция Google apps и GSuite",
			11 => "оптимально сконфигурированная серверная среда",
			12 => "",
		),
		"IMG" => "/local/assets/images/bg/man-and-tetris.png"
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"tariff:SectionBlocks",
	".default",
	array(
		'SECTION_ID' => '3'
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>