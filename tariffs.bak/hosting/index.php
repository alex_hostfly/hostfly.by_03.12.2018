<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Облачный веб-хостинг для предприятий малого и среднего бизнеса, ИП, стартапов, физических лиц. Размещение корпоративных сайтов, форумов, блогов, личных страниц, сайтов-визиток.");
$APPLICATION->SetPageProperty("title", "Облачный веб-хостинг | Hostfly.by");
$APPLICATION->SetTitle("Страница");
?>

<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BANNER_CLASS" => "banner-top",
		"IMG" => "/local/assets/images/bg/bg_host.png",
		"TITLE" => "Облачный веб-хостинг — доступный, защищенный, производительный.",
		"DESCRIPTION" => "Качество и скорость работы нашей облачной платформы обеспечивается широкой сетью физических серверов. Их число позволяет обеспечить бесперебойную работу хостинга даже при сбое части оборудования и эффективно распределять нагрузку для максимальной производительности вашего сайта. Независимо от того, являетесь ли вы обладателем небольшого стартапа или владеете бурно развивающимся предприятием среднего бизнеса, Hostfly.by будет вашим партнером на каждом этапе. Вы сами сможете управлять сайтом используя панель управления в личном кабинете.",
		"TEXT_BEFORE_BUTTON" => "",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => "",
		"BUTTON_TYPE" => ""
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"tariff:HostingList",
	".default",
	array(),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"content:SpecialOffer", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Тестовый период пользования веб-хостингом на первые 14 дней",
		"BUTTON_TEXT" => "Подробнее",
		"BUTTON_LINK" => "#"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"faq:commonList",
	".default",
	array(
		'SECTION_ID' => '4'
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"content:Recommendations", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Вам подходит веб-хостинг, если",
		"BUTTON_TEXT" => "Список тарифов",
		"BUTTON_LINK" => "#",
		"BENEFITS_LIST" => array(
			0 => "вы хотите разместить сайт с невысоким потреблением ресурсов",
			1 => "вы владелец стартапа, малого и среднего бизнеса",
			2 => "вы обычный человек, у которого есть блог, личная страница или сайт-визитка",
			3 => "вы планируете посещаемость на уровне 300-400 человек в день",
			4 => "вам достаточно стандартных мер по обеспечению конфиденциальности вашей информации",
			5 => "вы хотите платить только за предоставленные возможности и ни копейкой более",
		),
		"IMG" => "/local/assets/images/bg/man-and-tetris.png"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"tariff:SectionBlocks",
	".default",
	array(
		'SECTION_ID' => '1'
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>