<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Кто мы");
?>
<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	".default", 
	array(
		"IMG" => "/local/assets/images/bg/bg_main.png",
		"TITLE" => "HOSTFLY — облачный хостинг-провайдер и регистратор доменных имен",
		"DESCRIPTION" => "Регистрируем доменные имена. Оказываем услуги размещения сайтов в Интернете. Настраиваем и администрируем серверы. Выстраиваем индивидуальные облачные решения под нужды заказчика. Одним словом, помогаем физическим лицам и малому бизнесу реализовывать идеи, создавать и развивать свои проекты онлайн.",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"BANNER_CLASS" => "",
		"TEXT_BEFORE_BUTTON" => "",
		"BUTTON_TYPE" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"content:Benefits", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Быстрый и надежный хостинг в соответствии с мировыми стандартами  — у вас под рукой",
		"BUTTON_TEXT" => "",
		"BUTTON_LINK" => "",
		"BENEFITS_COUNT" => "8",
		"BENEFITS_1_TITLE" => "Высокие технологии во всём",
		"BENEFITS_1_DESCRIPTION" => "Благодаря современному оборудованию, технологичной инфраструктуре и ЦОД уровня Tier 3 отказоустойчивость наших серверов составляет 99,982%",
		"BENEFITS_1_IMG" => "/local/assets/images/icons/download_cloud.svg",
		"BENEFITS_2_TITLE" => "Персональные SSL сертификаты",
		"BENEFITS_2_DESCRIPTION" => "Все тарифные планы хостинга сайтов в облаке включают в себя поддержку защищенного протокола https:// от сертифицированного поставщика мирового уровня Let’s Encrypt.",
		"BENEFITS_2_IMG" => "/local/assets/images/icons/protect_page.svg",
		"BENEFITS_3_TITLE" => "Поддержка популярных CMS",
		"BENEFITS_3_DESCRIPTION" => "Автоматическая инсталляция и оптимизированное размещение CMS Wordpress, Joomla, OpenCart, MODx, Drupal и 150+ других.",
		"BENEFITS_3_IMG" => "/local/assets/images/icons/connect-world.svg",
		"BENEFITS_4_TITLE" => "Конструктор сайтов",
		"BENEFITS_4_DESCRIPTION" => "Создание профессионального сайта за 5 минут без навыков в программировании, простым перетаскиванием готовых блоков и добавлением своего контента на страницы.",
		"BENEFITS_4_IMG" => "/local/assets/images/icons/customise_site.svg",
		"BENEFITS_5_TITLE" => "Оперативная поддержка клиентов 24/7",
		"BENEFITS_5_DESCRIPTION" => "Мы доступны в любое время суток, включая выходные и праздничные дни. Телефон, онлайн-чат, электронная почта. Всегда на связи при первой необходимости.",
		"BENEFITS_5_IMG" => "/local/assets/images/icons/pretty-link-leaf.svg",
		"BENEFITS_6_TITLE" => "Простое управление",
		"BENEFITS_6_DESCRIPTION" => "Используем зарекомендовавшие себя во всем мире сервисы cPanel и WHM. Вы легко сможете управлять услугами, счетами, доменами, сайтами или сервером без посторонней помощи.",
		"BENEFITS_6_IMG" => "/local/assets/images/icons/windows.svg",
		"BENEFITS_7_TITLE" => "Система уведомлений",
		"BENEFITS_7_DESCRIPTION" => "Сообщим по email, SMS или телефону о предстоящем продлении услуг или о других важнейших событиях, связанных с управлением сервисами.",
		"BENEFITS_7_IMG" => "/local/assets/images/icons/mail-alert.svg",
		"BENEFITS_8_TITLE" => "Максимум лояльности",
		"BENEFITS_8_DESCRIPTION" => "Не заблокируем ваш сайт, даже если резкий рост количества посетителей вызовет превышение лимитов ресурсопотребления, прописанных в договоре.",
		"BENEFITS_8_IMG" => "/local/assets/images/icons/friends.svg"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("whoAreWe:Clients", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"CLIENTS_COUNT" => "27",
		"TITLE" => "Более 1000 довольных клиентов",
		"CLIENTS_1_SLIDE" => "/local/assets/images/partners/apple.png",
		"CLIENTS_2_SLIDE" => "/local/assets/images/partners/atlant.png",
		"CLIENTS_3_SLIDE" => "/local/assets/images/partners/bmw.png",
		"CLIENTS_4_SLIDE" => "/local/assets/images/partners/cola.png",
		"CLIENTS_5_SLIDE" => "/local/assets/images/partners/komunarka.png",
		"CLIENTS_6_SLIDE" => "/local/assets/images/partners/lego.png",
		"CLIENTS_7_SLIDE" => "/local/assets/images/partners/lidskae.png",
		"CLIENTS_8_SLIDE" => "/local/assets/images/partners/maz.png",
		"CLIENTS_9_SLIDE" => "/local/assets/images/partners/nokia.png",
		"CLIENTS_10_SLIDE" => "/local/assets/images/partners/vitex.png",
		"CLIENTS_11_SLIDE" => "/local/assets/images/partners/apple.png",
		"CLIENTS_12_SLIDE" => "/local/assets/images/partners/atlant.png",
		"CLIENTS_13_SLIDE" => "/local/assets/images/partners/apple.png",
		"CLIENTS_14_SLIDE" => "/local/assets/images/partners/cola.png",
		"CLIENTS_15_SLIDE" => "/local/assets/images/partners/komunarka.png",
		"CLIENTS_16_SLIDE" => "/local/assets/images/partners/apple.png",
		"CLIENTS_17_SLIDE" => "/local/assets/images/partners/lidskae.png",
		"CLIENTS_18_SLIDE" => "/local/assets/images/partners/maz.png",
		"CLIENTS_19_SLIDE" => "/local/assets/images/partners/apple.png",
		"CLIENTS_20_SLIDE" => "/local/assets/images/partners/vitex.png",
		"CLIENTS_21_SLIDE" => "/local/assets/images/partners/apple.png",
		"CLIENTS_22_SLIDE" => "/local/assets/images/partners/atlant.png",
		"CLIENTS_23_SLIDE" => "/local/assets/images/partners/bmw.png",
		"CLIENTS_24_SLIDE" => "/local/assets/images/partners/cola.png",
		"CLIENTS_25_SLIDE" => "/local/assets/images/partners/apple.png",
		"CLIENTS_26_SLIDE" => "/local/assets/images/partners/apple.png",
		"CLIENTS_27_SLIDE" => "/local/assets/images/partners/lidskae.png"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>

<?$APPLICATION->IncludeComponent(
	"whoAreWe:BlockAboutUs", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BLOCK_ABOUT_US_COUNT" => "3",
		"BLOCK_ABOUT_US_1_TITLE" => "Круглосуточный мониторинг состояния нашего виртуального облака",
		"BLOCK_ABOUT_US_1_DESCRIPTION" => "В соответствии с высокими стандартами качества в сфере оказания услуг облачного хостинга мы в режиме реального времени отслеживаем состояние программно-аппаратного комплекса, на котором размещаются наши клиенты.",
		"BLOCK_ABOUT_US_1_IMG" => "/local/assets/images/bg/care.png",
		"BLOCK_ABOUT_US_1_ADVANTAGE_TITLE" => "Оперативная техподдержка 24/7",
		"BLOCK_ABOUT_US_1_ADVANTAGE_TEXT" => "Время гарантированного ответа через систему тикетов 15-25 минут в зависимости от уровня сложности поступившего запроса. Мы доступны в любое время суток, включая выходные и праздничные дни.",
		"BLOCK_ABOUT_US_2_TITLE" => "Кто мы",
		"BLOCK_ABOUT_US_2_DESCRIPTION" => "С 2005 года наша команда предоставляет услуги удаленного системного администрирования и сервиса поддержки клиентов по модели HelpDesk более чем десяти хостинг-провайдерам, работающим как на внутреннем рынке США, так и на международном рынке. Среди наших клиентов - бренды с мировым именем, обслуживающие сотни тысяч вебсайтов в Северной и Латинской Америке, Европе и Азии.",
		"BLOCK_ABOUT_US_2_IMG" => "/local/assets/images/bg/team.png",
		"BLOCK_ABOUT_US_2_ADVANTAGE_TITLE" => "Почему мы",
		"BLOCK_ABOUT_US_2_ADVANTAGE_TEXT" => "За годы работы наши специалисты накопили уникальный опыт обслуживания и технического администрирования веб-сайтов разного типа сложности, от одностраничных лендингов до высоконагруженных проектов, от простых решений, реализованных на CMS Wordpress, Joomla и phpBB до по-настоящему крупных проектов, использующих Drupal, Magento и Opencart.",
		"BLOCK_ABOUT_US_3_TITLE" => "Мы за качество во всём",
		"BLOCK_ABOUT_US_3_DESCRIPTION" => "Наша цель - внедрить на рынке хостинга в Беларуси стандарты качества в обслуживании клиентов, используемые во всем мире, при этом предлагая пользователям инновационные решения по разумным ценам.",
		"BLOCK_ABOUT_US_3_IMG" => "/local/assets/images/bg/innovation.png",
		"BLOCK_ABOUT_US_3_ADVANTAGE_TITLE" => "Современное оборудование",
		"BLOCK_ABOUT_US_3_ADVANTAGE_TEXT" => "В построении нашей облачной инфраструктуры мы используем оборудование и программное обеспечение от ведущих мировых вендоров: серверы HP и Dell на базе процессоров Intel Dual Xeon E5, системы виртуализации VMware vCloud и Virtuozzo. Размещается оборудование в одном из лучших дата-центров  Беларуси, сертифицированному по международному стандарту Tier 3 (отказоустойчивость - 99,982%)."
	),
	false
);?>

<a name="achievements"></a>
<?$APPLICATION->IncludeComponent(
	"whoAreWe:Achievements", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Наши награды и сертификаты",
		"BUTTON_TEXT" => "Показать больше сертификатов"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>