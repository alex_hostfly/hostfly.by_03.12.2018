<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?>

<?$APPLICATION->IncludeComponent(
    "content:Banner",
    "subscribe",
    [
        'BANNER_CLASS' => 'banner-top',
        'IMG' => '/local/assets/images/bg/bg_main.png',
        'TITLE' => 'Новости и акции HOSTFLY',
        'DESCRIPTION' => 'Будьте в курсе всех событий и выгодных предложений. Никакого спама, только самое свежее и интересное.',
        'SUBSCRIBE' => [
            'text' => 'Подписаться на рассылку',
            'href' => '#',
        ]
    ],
    false
);?>

<?$APPLICATION->IncludeComponent(
    "news:NewsList",
    ".default",
    [],
    false
);?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");?>