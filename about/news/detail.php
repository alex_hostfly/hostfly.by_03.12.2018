<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>

<?$APPLICATION->IncludeComponent(
	"news:NewsDetailBanner", 
	".default", 
	[
		"BANNER_CLASS" => "banner-top",
		"IMG" => "/local/assets/images/bg/bg_main.png",
		"TITLE" => "Новости и акции HOSTFLY",
		"DESCRIPTION" => "Будьте в курсе всех событий и выгодных предложений. Никакого спама, только самое свежее и интересное.",
		"BUTTON_TEXT" => "К списку",
		"BUTTON_HREF" => "/about/news/",
		"COMPONENT_TEMPLATE" => ".default"
	],
	false
);?>

<?$APPLICATION->IncludeComponent(
	"news:NewsDetailContainer", 
	".default", 
	array(
		"LAST_NEWS_TITLE" => "Последние новости",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
    "news:ShareSocialNetworks",
    ".default",
    array(),
    false
);?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");?>