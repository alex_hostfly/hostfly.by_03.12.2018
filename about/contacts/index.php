<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

<?
$APPLICATION->IncludeComponent(
	"contacts:AddressBlock", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Контакты",
		"TEXT" => "ООО «Суппорт чейн», проспект Победителей, дом 106, офис 14<br>г. Минск, 220062, Республика Беларусь",
		"EMAIL" => "info@hostfly.by"
	),
	false
); ?>

<?
$APPLICATION->IncludeComponent(
	"contacts:PhonesBlock", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE_SALE_DEPARTMENT" => "Отдел продаж",
		"MAIN_PHONE_SALE_DEPARTMENT" => "+375 17 343-55-32",
		"EMAIL_SALE_DEPARTMENT" => "sales@hostfly.by",
		"CONTACTS_SALE_DEPARTMENT" => array(
			0 => "+375 29 123-21-21|Velcom|Пн-Пт: с 9:00 до 20:00",
			1 => "+375 29 123-12-12|MTS|Сб-Вс: с 9:00 до 21:00",
			2 => "+375 29 123-12-12|Life",
			3 => "+375 17 343-55-32|Факс",
			4 => "",
		),
		"TITLE_SUPPORT" => "Техническая поддержка",
		"TEXT_AFTER_IMAGE" => "Круглосуточно <br><a class=phone-white href=tel:+375173435532>+375 17 343-55-32</a>",
		"BUTTON_TEXT" => "online-чат",
		"BUTTON_HREF" => "#",
		"EMAIL_RIGHT_BLOCK" => "support@hostfly.by"
	),
	false
);
?>

<?
$APPLICATION->IncludeComponent(
	"contacts:FormAndMapBlock", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TITLE" => "Написать нам",
		"BUTTON_TEXT" => "Отправить",
		"COORDINATION_Y" => "53.939482",
		"COORDINATION_X" => "27.478855"
	),
	false
);
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>