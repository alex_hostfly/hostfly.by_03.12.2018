<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Часто задаваемые вопросы");
?>

<?$APPLICATION->IncludeComponent(
	"content:Banner", 
	"online-chat", 
	array(
		"BANNER_CLASS" => "banner-top",
		"IMG" => "/local/assets/images/bg/bg_main.png",
		"TITLE" => "Часто задаваемые вопросы",
		"DESCRIPTION" => "Специально для вас мы собрали список самых популярных вопросов. Но если вы н нашли ответа — у нас работает круглосуточная техподдержка",
		"BUTTON_TEXT" => "Online-чат",
		"BUTTON_LINK" => "#",
		"TEXT_BEFORE_BUTTON" => "Круглосуточно <br> +375 17 123-45-67",
		"COMPONENT_TEMPLATE" => "online-chat",
		"BUTTON_TYPE" => ""
	),
	false
);?>

<?
$APPLICATION->IncludeComponent(
    'faq:FaqList',
    '.default',
    array(
    	'IBLOCK_CODE' => 'faq',
		'SECTION_ID' => 7
	),
    false
)
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>